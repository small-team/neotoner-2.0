<?php

return [
    'dir_upload' => 'upload',
    'image_engine' => 'IMagick',
    'imagick_path' => '/usr/local/bin/',
];