<?php
namespace SmartModel;

use Carbon\Carbon;
use InvalidArgumentException;

use SmartModel\Tools\FilesHelper;

use SmartModel\Interfaces\ValidationModelInterface;
use SmartModel\Traits\ValidationModelTrait;
use SmartModel\Exceptions\ValidationException;

use Illuminate\Support\Facades\DB;

abstract class SmartModel extends \Eloquent implements ValidationModelInterface {

    use ValidationModelTrait;

    public $_is_new = false;
    public $_changed_data = [];
    public $customMessages = [];

    public static $files = array();

    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        static::saved(function ($object) {
            $object->afterSave();
        });
    }

    public function afterSave() {
        if(isset(static::$files) && is_array(static::$files) && !empty(static::$files)) {
            FilesHelper::create($this)->saveFiles()->loadFiles();
        }
        if ($this->_is_new && isset($this->_sortability) && $this->_sortability) {
            $_position = DB::table($this->getTable())->max('_position')+1;
            DB::table($this->getTable())->where($this->getKeyName(),'=',$this->getKey())->update(array('_position'=>$_position));
        }
        $this->_changed_data = array();
        return true;
    }

    /**
     * Save the model to the database.
     *
     * @param array   $rules
     * @param array   $customMessages
     * @param array   $options
     * @param Closure $beforeSave
     * @param Closure $afterSave
     *
     * @return bool
     */
    public function save(array $rules = array(),
                         array $customMessages = array()
    ) {

        $rules = $this->getValidationRules();

        try
        {
            $this->validate($this->attributes,$rules, $this->customMessages);
        } catch (ValidationException $e)
        {
            return false;
        }

        if (!$this->getKey()) {
            $this->_is_new = true;
        } else {
            $this->_is_new = false;
        }

        if(isset(static::$files) && is_array(static::$files) && !empty(static::$files)) {
            foreach (static::$files as $alias => $file_info) {
                unset($this->$alias);
            }
        }
        parent::save();

        return true;
    }

    /**
     * Get files folder
     *
     * @return null|string
     * */
    public function getFilesFolder() {
        if(isset(static::$files) && is_array(static::$files) && !empty(static::$files)) {
            return FilesHelper::create($this)->getFilesFolder();
        }

        return null;
    }

    /**
     * Remove model files
     *
     * @param string $alias File alias
     * @param string $name File name
     * @return SmartModel
     * */
    public function removeFile($alias, $name) {
        if(isset(static::$files) && is_array(static::$files) && !empty(static::$files)) {
            FilesHelper::create($this)->removeFile($alias, $name);
        }
        return $this;
    }

    /**
     * Load model files
     *
     * @return SmartModel
     * */
    public function loadFiles() {
        if(isset(static::$files) && is_array(static::$files) && !empty(static::$files)) {
            FilesHelper::create($this)->loadFiles();
        }
        return $this;
    }

    /**
     * Save model files
     *
     * @return SmartModel
     * */
    public function saveFiles() {
        if(isset(static::$files) && is_array(static::$files) && !empty(static::$files)) {
            FilesHelper::create($this)->saveFiles()->loadFiles();
        }
        return $this;
    }

    public function mergeData(array $data) {
        if(!is_array($data) || empty($data)) {
            return false;
        }

        $structure = $this->getStructure();

        foreach ($data as $key => $item) {
            if(!isset($structure['columns']) || !is_array($structure['columns']) || !array_key_exists($key, $structure['columns'])) {
                continue;
            }
            $this->$key = $item;
        }

        return $this;
    }

    public function getClassnameWithoutNamespace() {
        $full = get_class($this);
        $path = explode('\\', $full);
        return array_pop($path);
    }

    /**
     * Build model structure by DB table
     *
     * @return array
     * */
    public function getStructure() {
        $table = $this->getTable();
        $structure = DB::select('DESCRIBE '.  $table .'');
        $result = array();

        if(is_array($structure) && !empty($structure)) {
            $columns = array();
            foreach ($structure as $item) {
                $columns[$item->Field] = array(
                    'type' => $item->Type,
                    'default' => $item->Default,
                    'not_null' => $item->Null == 'NO' ? true : false,
                );
            }

            $result = array(
                'table' => $table,
                'primary_key' => $this->getKeyName(),
                'columns' => $columns,
            );
        }
        return $result;
    }
}