<div class="editable_field_block  fleft mr10">
    <input class="form-control filter_numrange input_text editable_field" type="text" name="{{$field_input_name}}[from]" value="{{$field_value['from']}}"/>
    <div class="clear"></div>
</div>
<div class="editable_field_block">
    <input class="form-control filter_numrange input_text editable_field" type="text" name="{{$field_input_name}}[to]" value="{{$field_value['to']}}"/>
    <div class="clear"></div>
</div>
<div class="clear"></div>
