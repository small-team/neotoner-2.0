@extends('admin::default')

@section('content')

<input id="model" value="{{$module['model']}}" type="hidden">

<div class="row">

    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @include ("admin::list._list_widget")
    </article>

</div>

@stop