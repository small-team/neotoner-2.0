@if (isset($module['filters']))
    @include ("admin::list._filter_widget")
@endif
@if (!isset($just_content) || !$just_content)
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false">
<header>
    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
    <h2>{{$module['title']}} @if(!empty($count_orders))( {{$count_orders}} шт. ) @endif </h2>
</header>
<div role="content">
@endif

    <form id="table-data-form" action="/{{$app_name}}/{{$module['name']}}/" method="post" enctype="multipart/form-data">
        <div>
            <div class="jarviswidget-editbox">
            </div>

            <div class="widget-body no-padding">
                {{ Admin\Helpers\AdminMessagesHelper::process() }}
                <div class="widget-body-toolbar">
                    <div class="btn-group">
                        @if (isset($module['parent']) && $module['parent'] == 'Inline')
                            <input id="inline_module_type" value="1" type="hidden">
                        @endif
                        <a class="list_tab_item list_item_filter list_item_active btn btn-sm btn-warning" href="/{{$app_name}}/"><i class="fa fa-reply"></i></a>
                        @if (isset($module['actions']['add']))
                            @if (isset($module['parent']) && $module['parent'] == 'Inline')
                                <span title="Добавить {{{ isset($module['single']) ? $module['single'] : '' }}}" id="add-empty-row" class="list_tab_item list_item_add cp btn-primary btn-sm btn" href="/{{$app_name}}/{{$module['name']}}/add/{{{ isset($id_category) ? 'c/'.$id_category : '' }}}">Добавить <i class="fa fa-plus"></i></span>
                            @else
                                <a title="Добавить {{{ isset($module['single']) ? $module['single'] : '' }}}" class="list_tab_item list_item_add btn-primary btn-sm btn" href="/{{$app_name}}/{{$module['name']}}/add/{{{ isset($id_category) ? 'c/'.$id_category : '' }}}">Добавить <i class="fa fa-plus"></i></a>
                            @endif
                        @endif
                        @if (isset($module['custom_default_top']) && count($module['custom_default_top']))
                            @foreach ($module['custom_default_top'] as $tpl)
                                @include ('admin::custom.'.$tpl)
                            @endforeach
                        @endif

                        @if (isset($module['top_actions']) && count($module['top_actions']))
                            @foreach ($module['top_actions'] as $item)
                                <a class="list_tab_item list_item_{{$item['icon']}}" href="/{{$app_name}}/{{{isset($module['module_action_name']) ? $module['module_action_name'] : $module['name']}}}/{{{ isset($item['action']) ? $item['action'] : '' }}}">{{$item['title']}}</a>
                            @endforeach
                        @endif

                        @if (isset($module['custom_top_actions']) && count($module['custom_top_actions']))
                            @foreach( $module['custom_top_actions'] as $item)
                                <a class="list_tab_item list_item_{{$item['icon']}}" href="/{{$app_name}}/{{$item['module_name']}}/{{{ isset($item['action']) ? $item['action'] : '' }}}">{{$item['module_title']}}</a>
                            @endforeach
                        @endif
                    </div>
                </div>



                <div class="widget-footer ">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="btn-group  pull-left">
                                @if (isset($module['actions']['delete']))
                                    <span class="group_delete_handler btn btn-danger btn-sm">удалить</span>
                                @endif
                                @if (isset($module['group_actions']) && count($module['group_actions']))
                                    @foreach ($module['group_actions'] as $item)
                                        <span class="group_action_handler  btn btn-default btn-sm" id="action{{{ isset($item['confirm']) ? '-confirm' : '' }}}-{{$item['action']}}">{{$item['title']}}</span>
                                    @endforeach
                                @endif
                                @if (isset($module['parent']) && $module['parent'] == 'Inline' && (isset($module['actions']['add']) || isset($module['actions']['edit'])))
                                    <span onclick="showLoader();$('#table-data-form').submit()" class="button ml20 cp inline_handler button btn btn-success btn-sm">сохранить</span>
                                    <a href="/{{$app_name}}/{{{isset($module['module_action_name']) ? $module['module_action_name'] : $module['name']}}}/" class="button inline_handler cp ml20 btn-warning btn btn-sm">отмена</a>
                                @endif
                            </div>
                        </div>
                        @if (isset($module['per_page']) && isset($module['parent']) && $module['parent'] != 'Single')
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">@include ("admin::list._paging")</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@if (!isset($just_content) || !$just_content)
</div>
</div>
@endif

@stop