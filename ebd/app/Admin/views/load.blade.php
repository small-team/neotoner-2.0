@extends('admin::default')
@section('content')

<div>
    <div class="jarviswidget-editbox">
    </div>

    <div class="widget-body no-padding">


            <div class="row">
                <form  id="price-load-form" method=post enctype=multipart/form-data >
                     {!! csrf_field() !!}
                    <input class="uploadprice" type=file name=uploadfile>
                     <h5><label><input type="checkbox" checked name="first_string"/>Пропустить первую строку</label></h5>
                    <input type=submit value=Загрузить>
                </form>

            </div>
            @if(isset($result))
            <div class="row">

                <div class="col-lg-12 report_load">
                    <div class="row">
                            <span class="col-lg-4">
                                Всего строк в файле
                            </span>
                            <span class="col-lg-4">{{$result['all']}}
                            </span>
                    </div>
                    <div class="row">
                            <span class="col-lg-4">
                                Всего импортировано строк
                            </span>
                            <span class="col-lg-4">
                                {{$result['suc']}}
                            </span>
                    </div>
                    <div class="row">
                            <span class="col-lg-4">
                                Всего обновлено
                            </span>
                            <span class="col-lg-4">
                                {{$result['update']}}
                            </span>
                    </div>
                    <div class="row">
                            <span class="col-lg-4">
                                Всего добавлено
                            </span>
                            <span class="col-lg-4">
                                {{$result['add']}}
                            </span>
                    </div>
                    <div class="row">
                            <span class="col-lg-4">
                                Всего удалено
                            </span>
                            <span class="col-lg-4">{{$result['del']}}
                            </span>
                    </div>
                </div>

            </div>
            @else
             <div class="col-lg-12 load-csv-description">
                        Выберите для загрузки файл с расширением .xlsx.<br>
                        <h6><strong>Прайс должен содержать только числовые значения!</strong></h6>
                        <h6><strong>Не должен содержать формул!</strong></h6>
                        Указать бренд нужно в колонке "А", тип картриджа в колонке "B".<br>
                         Ниже произвести запись прайса картриджей этого типа
                         с указанием Артикула в колонке "А", Модели принтера в колонке "B", Цены в доллара в колонке "C", Цены в рублях в колонке "D" <br />
                        <h4>Например:</h4>
                        <div class="expample-price">
                            Бренд |Тип картриджа<br />
                            HP|Лазерные монохромные картриджи<br />
                            Артикул|Модель|Цена в долларах|Цена в рублях<br />
                            92274A|LJ 4+/5|22|1100<br />
                            92284A|LJ 4+/6|20|100<br />
                        </div>
                        <h4><a href="/price_example.xlsx">Скачать пример</a></h4>
                           <p>&nbsp;</p>
                        <form action="{{URL::action('LoadController@downloadPriceExel')}}">
                        <button type="submit" title="Выгрузить текущий прайс" >Выгрузить текущий прайс</button><br>
                        </form>
                        {{--<h4><a href="{{URL::action('LoadController@downloadPriceExel')}}" >Выгрузить текущий прайс</a></h4>--}}

            </div>

@endif
    </div>
</div>
@stop