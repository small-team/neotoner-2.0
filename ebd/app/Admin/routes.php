<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if(isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/'.config('admin.prefix'))===0) {
    Route::group(array('prefix' => config('admin.prefix'),'middleware' => 'login'), function () {
        Route::pattern('id', '[0-9]+');
        Route::pattern('page_number', '[0-9]+');
        Route::any('/save_order_elements', 'Admin\Controllers\AdminBaseController@anySaveOrderElements');
        Route::any('/upload_file', 'Admin\Controllers\AdminBaseController@anyUploadFile');
        Route::any('/upload_files', 'Admin\Controllers\AdminBaseController@anyUploadFiles');
        Route::any('/save_toggle_elements', 'Admin\Controllers\AdminBaseController@anySaveToggleElements');
        //    Route::any('/{module}/delete_file.json', 'AdminBaseController@anyDeleteFile');

        if(isset($_SERVER['REQUEST_URI'])) {
            $uri = str_replace(array('/'.config('admin.prefix'), '?'.$_SERVER['QUERY_STRING']), '', $_SERVER['REQUEST_URI']);
            $uri = str_replace('//', '/', '/'.$uri.'/');
            $patterns = array(
                '#^/(?P<controller>[-_a-z0-9]+)/page/(?P<current_page>[0-9]+)/?$#isU',
                '#^/(?P<controller>[-_a-z0-9]+)/(?P<action>[-_a-z]+)/(?P<id>[0-9]+)/?$#isU',
                '#^/(?P<controller>[-_a-z0-9]+)/(?P<action>[-_a-z]+)/?$#isU',
                '#^/(?P<controller>[-_a-z0-9]+)/?$#isU',
            );

            $result = null;
            foreach ($patterns as $pattern) {
                preg_match($pattern, $uri, $result);
                if(is_array($result) && !empty($result)) {
                    break;
                }
            }
            $controller = isset($result['controller']) && !empty($result['controller']) ? ucfirst(Admin\Tools\StringTools::functionalize($result['controller'])) : false;
            $action = isset($result['action']) && !empty($result['action']) ? ucfirst(Admin\Tools\StringTools::functionalize($result['action'])) : false;
            $current_page = isset($result['current_page']) && intval($result['current_page']) ? intval($result['current_page']) : false;
//            dd($result, $controller, $action, $current_page,strtolower($result['controller']), $controller.'Controller@anyIndex');
            if($controller && $action) {
                Route::any(strtolower($result['controller']).'/'.strtolower($result['action']), $controller.'Controller' . '@any' . $action);
                Route::any(strtolower($result['controller']).'/'.strtolower($result['action']).'/{id}', $controller.'Controller' . '@any' . $action);
            } elseif($controller && class_exists($controller.'Controller') && !$current_page) {
                Route::any(strtolower($result['controller']), $controller.'Controller@anyIndex');
                Route::controller(strtolower($result['controller']), $controller.'Controller');
            } elseif($controller && class_exists($controller.'Controller') && $current_page) {
                Route::controller(strtolower($result['controller']).'/page/{page_number}', $controller.'Controller');
            }
        }

        Route::controller('/', 'Admin\Controllers\AdminIndexController');
    });


}
