<?php
return [
    'title' => 'Загрузка прайса',
    'model' => 'Satellite',
    'fields' => [
        'code' => [
            'title' => 'Ключ проекта',
            'type' => 'text',
        ],


    ],
    'sort' => 'id DESC',
    'tabs' => [ ],
    'actions' => [
        'list' => [
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;