<?php

/**
 * @date: 09.08.2014 15:15:13
 */
class PricesLoadController extends \Admin\Controllers\ListController
{

    public static $id_brand;
    public static $_position_brand = 1;
    public static $str_update ;
    public static $row ;

    public function anyIndex($page_number = 1)
    {

        $this->setupLayout();
        $this->view->setTemplate('admin::load');
        $upload_file = \Input::file('uploadfile');

        if (!empty($upload_file)) {

            $reader = Excel::selectSheetsByIndex(0)->load($upload_file);
            $reader->ignoreEmpty()->noHeading()->sheet(0);

            if (!empty($_POST['first_string'])) {
                $reader->skip(1);
            }

            $rows = $reader->all()->toArray();

            $articule_cartridge = \App\Models\CartridgePrice::lists('articule')->toArray();
            \App\Models\CartridgePrice::where('active', 1)->update(['active' => 0]);

            $data = [];

            foreach ($rows as $k => $row) {

                if (!empty($row[1]) && empty($row[2]) && empty($row[3]) && empty($row[4])) {
                    $brand_title = trim($row[1]);
                    $brand_slug = Slug::make(mb_strtolower($brand_title));
                    $brand = \App\Models\Brand::firstOrCreate(['_slug' => $brand_slug]);
                    $brand->update(['title' => $brand_title, '_position' => self::$_position_brand]);

                    self::$id_brand = $brand->id;

                    self::$_position_brand++;

                    continue;
                }

                $article = trim($row[1]);

                if (!in_array($article, $articule_cartridge)) {
                    $data[] = [
                        'id_brand' => self::$id_brand,
                        'articule' => $article,
                        'active' => 1,
                        'cartridge_model' => trim($row[2]),
                        'price_USD' => trim($row[3]),
                        'price_RUB' => trim($row[4])
                    ];
                    continue;
                }

                \App\Models\CartridgePrice::where('articule', $article)
                    ->update(['id_brand' => self::$id_brand,
                        'articule' => $article,
                        'active' => 1,
                        'cartridge_model' => trim($row[2]),
                        'price_USD' => trim($row[3]),
                        'price_RUB' => trim($row[4])]);

                self::$str_update++;

            }
            $str_add    =   count($data);

            if(!empty($data)){
                \DB::table('cartridge_prices')->insert($data);
            }

            $str_del    =   \App\Models\CartridgePrice::where('active',0)->count('active');
            
            $this->view->result = array(
                'all' => count($rows)-1,
                'add' => $str_add,
                'update' => self::$str_update,
                'del' =>  $str_del,
                'suc' => $str_add+self::$str_update,
                'not_load' =>  count($rows) - ($str_add+self::$str_update),
            );

        }
        return $this->view->make($this);
    }


}