<?php
return [
    'title' => 'Типи картриджей',
    'model' => 'CartridgeType',
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],

    ],
    'sort' => 'id ASC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','title']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;