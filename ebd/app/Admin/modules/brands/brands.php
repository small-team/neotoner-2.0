<?php
return [
    'title' => 'Производители',
    'model' => 'Brand',
    'sortability' => true,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],
        '_slug' => [
            'title' => 'Транслит',
            'type' => 'text',
        ],
//        'meta_title' => [
//            'title' => 'Meta_title',
//            'type' => 'text',
//        ],
//        'meta_description' => [
//            'title' => 'Meta_description',
//            'type' => 'text',
//        ],
//        'meta_keywords' => [
//            'title' => 'Meta_keywords',
//            'type' => 'text',
//        ],
//        'og_title' => [
//            'title' => 'Social title',
//            'type' => 'text',
//        ],
//        'og_description' => [
//            'title' => 'Social description',
//            'type' => 'text',
//        ],


    ],
    'sort' => '_position ASC',
    'tabs' => [
//        'metas'=>['title'=> 'Метаданные','fields'=> ['meta_title', 'meta_description', 'meta_keywords','og_title','og_description']],
    ],
    'actions' => [
        'list' => [
            'show' => ['id','title','_slug','_position']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;