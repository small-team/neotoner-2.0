<?php
return [
    'title' => 'Картриджи',
    'model' => 'CartridgePrice',
    'per_page' => 50,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'id_brand' => [
            'title' => 'Производитель',
            'type' => 'select',
        ],
        'articule' => [
            'title' => 'Артикул',
            'type' => 'text',
        ],
//        'id_type' => [
//            'title' => 'Тип картриджа',
//            'type' => 'select',
//        ],
        'cartridge_model' => [
            'title' => 'Модель картриджа',
            'type' => 'text',
        ],
        'price_USD' => [
            'title' => 'Цена(USD)',
            'type' => 'text',
        ],
        'price_RUB' => [
            'title' => 'Цена(RUB)',
            'type' => 'text',
        ],


    ],
    'group' => [
        'id_brand',
    ],
    'sort' => 'id ASC, articule ASC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','id_brand','articule','cartridge_model','price_RUB','price_USD']
        ],
        'edit' => [
            'hide' => ['id'],
        ]
    ],
    'filters' => [
        'id_brand' => [
            'title' => 'Производитель',
            'type' => 'select',
        ],
        'articule' => [
            'title' => 'Артикул',
            'type' => 'text',
        ],
      
    ]
]
    ;