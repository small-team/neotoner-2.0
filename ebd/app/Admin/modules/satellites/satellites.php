<?php
return [
    'title' => 'Данные сателлита',
    'model' => 'Satellite',
    'fields' => [
        'code' => [
            'title' => 'Ключ проекта',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Название сайта',
            'type' => 'text',
        ],
        'description' => [
            'title' => 'Описание проекта',
            'type' => 'text',
        ],
        'url' => [
            'title' => 'Адрес',
            'type' => 'text',
        ],
        'city' => [
            'title' => 'Город',
            'type' => 'text',
        ],
        'phone' => [
            'title' => 'Телефон',
            'type' => 'text',
        ],
        'email' => [
            'title' => 'Email',
            'type' => 'text',
        ],
        'orders_email' => [
            'title' => 'Email для оповещения о заказах',
            'type' => 'text',
        ],
        'status' => [
            'title' => 'Статус',
            'type' => 'text',
        ],
        'hosting_title' => [
            'title' => 'Название компании',
            'type' => 'text',
        ],
        'hosting_tariff' => [
            'title' => 'Тарифный пакет и условия',
            'type' => 'text',
        ],
        'hosting_date_end' => [
            'title' => 'Срок окончания',
            'type' => 'text',
        ],
        'domen_title' => [
            'title' => 'Название компани',
            'type' => 'text',
        ],
        'domen_tariff' => [
            'title' => 'Тарифный пакет и условия',
            'type' => 'text',
        ],
        'domen_date_end' => [
            'title' => 'Срок окончания',
            'type' => 'text',
        ],
        'satellite_key' => [
            'title' => 'Ключ доступа',
            'type' => 'text',
        ],
        'price_mode' => [
            'title' => 'Режим цен',
            'type' => 'text',
        ],
        'icq' => [
            'title' => 'ICQ',
            'type' => 'text',
        ],
        'skype' => [
            'title' => 'Skype',
            'type' => 'text',
        ],
        'coefficient' => [
            'title' => 'Коеффициент',
            'type' => 'text',
        ],
        'rate' => [
            'title' => 'Курс',
            'type' => 'text',
        ],
        'id_satellite' => [
            'title' => 'Идентификатор проекта',
            'type' => 'text',
        ],


    ],
    'sort' => 'id DESC',
    'tabs' => [
        'hosting'=>['title'=> 'Хостинг','fields'=> ['hosting_title','hosting_tariff','hosting_date_end']],
        'domen'=>['title'=> 'Домен','fields'=> ['domen_title','domen_tariff','domen_date_end']],
        'contacts_data'=>['title'=> 'Контакты','fields'=> ['phone','email','icq','skype',]],
    ],
    'actions' => [
        'list' => [
            'show' => ['id','code','title','description','url','city','phone','email','orders_email','status',
                        'hosting_title','hosting_tariff','hosting_date_end',
                        'domen_title','domen_tariff','domen_date_end',
                        'satellite_key','price_mode','icq','skype','coefficient','rate','id_satellite']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;