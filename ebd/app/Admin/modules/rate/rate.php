<?php
return [
    'title' => 'Данные сателлита',
    'model' => 'Satellite',
    'fields' => [
        'code' => [
            'title' => 'Ключ проекта',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Название сайта',
            'type' => 'text',
        ],
        'coefficient' => [
            'title' => 'Коеффициент',
            'type' => 'text',
        ],
        'rate' => [
            'title' => 'Курс',
            'type' => 'text',
        ],


    ],
    'sort' => 'id DESC',
    'tabs' => [ ],
    'actions' => [
        'list' => [
            'show' => ['id','code','title','rate','coefficient']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;