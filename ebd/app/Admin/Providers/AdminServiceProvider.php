<?php

namespace Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Admin\Admin;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        dd(1);
        $file = app_path().'/Admin/'.'config.php';

        $config = $this->app['files']->getRequire($file);
        $this->app['config']->set('admin' , $config['admin']);

        $this->loadViewsFrom(app_path().'/Admin/views', 'admin');

        Admin::instance();

        require app_path().'/Admin/'.'routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
