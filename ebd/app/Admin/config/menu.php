<?php

return [
    'config'=>[
        'title' => 'Настройки',
        'icon' => 'fa-gears',
        'sub'=>[
            'satellites'=>[ 'title'=> 'Коефициенты сателлитов','icon' => 'fa-gear'],
            'acl'=>['title'=> 'Доступ','icon' => 'fa-user'],
        ]
    ],
    'orders' => [
        'title' => 'Журнал заявок',
    ],
    'satellites' => [
        'title' => 'Сателлиты',
    ],
    'catalog' => [
        'title' => 'Каталоги','icon' => 'fa-tasks',
        'sub'=>[
            'brands'=>[ 'title'=> 'Производители'],
//            'cartridge_types'=>[ 'title'=> 'Типи картриджей'],
        ]
    ],
    'prices'=>[
        'title' => 'Прайс-листы','icon' => 'fa-money',
        'sub'=>[
            'prices_load'=>[ 'title'=> 'Загрузка прайса'],
            'cartridge_prices'=>[ 'title'=> 'Картриджи'],

        ]
    ],
    'rate' => [
        'title' => 'Таблица коэфф-тов',
    ],

];