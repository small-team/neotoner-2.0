<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return ('');
});

get('/data_exchange',['as'=>'data_exchange','uses'=>'LoadController@data_exchange']);

get('/download_price_exel',['as'=>'downloadPriceExel','uses'=>'LoadController@downloadPriceExel']);
