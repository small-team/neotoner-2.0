<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\CartridgePrice;
use App\Models\CartridgeType;
use App\Models\Satellite;
use App\Models\Settings;
use Curl\Curl;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LoadController extends Controller
{

    public function data_exchange(Request $request)
    {

        $key           =   $request->input('key');
        $has_orders    =   $request->input('orders');

        if ((!$key) || (empty($key))) {
            die();
        }
        $key = trim($key);
//        запрос на принятия заявок  if ($has_orders)
        if ($has_orders) {
            Satellite::loadOrders($key) ;
        }

//        последнее обновление
//        Satellite::where('satellite_key',$key)->update('updated_at',time());
        $object     =   Satellite::where('satellite_key',$key)->first();
        $res        =   ['satellite'=>[]];
        $res['satellite'] = $object;

        $id = isset($res['satellite']['id']) ? $res['satellite']['id'] : false;
        unset($res['satellite']['id']);

        $res['cartridge_prices']    =   CartridgePrice::where('active',1)->get()->toArray();
        $res['cartridge_types']     =   CartridgeType::all()->toArray();
        $res['brands']              =   Brand::all()->toArray();

        echo json_encode($res);
        die();
    }

    public function downloadPriceExel()
    {
        $filename   = "neotoner_cartridges__".date("m.d.y").".xlsx";

        $cartridge_price_list=\DB::table('cartridge_prices')
            ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
            ->select('cartridge_prices.id',
                'cartridge_prices.articule',
                'brands.title as brand',
                'cartridge_prices.cartridge_model',
                'cartridge_prices.price_RUB',
                'cartridge_prices.price_USD')
            ->where('active',1)
            ->get();
        $cartridge_price_item   =   array();
        foreach($cartridge_price_list as $value){
            $cartridge_price_item[$value->brand][]=(array)$value;
        }


        $i = 2;
        $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel2007')->load('price_new.xlsx');
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');

//        $contact_info = \DB::table("satellites")->first();
//        $objWorksheet->setCellValue("F1", $contact_info->rate);
//        $objWorksheet->setCellValue("G1", $contact_info->phone);
//        $objWorksheet->setCellValue("G2", $contact_info->email);

        foreach ($cartridge_price_item as $type => $type_items) {

//            $objWorksheet->mergeCells("A$i:D$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("A2:D2"), "A$i:D$i");
            $objWorksheet->setCellValue("A".$i, $type_items[0]['brand']);

            $i++;
            foreach ($type_items as $k_trash => $item) {
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("A6"), "A".$i);
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("B6"), "B".$i);
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("C6"), "C".$i);
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("D6"), "D".$i);

                $objWorksheet->setCellValue("A".$i, $item['articule']);
                $objWorksheet->setCellValue("B".$i, $item['cartridge_model']);
                $objWorksheet->setCellValue("C".$i, $item['price_USD']);
                $objWorksheet->setCellValue("D".$i, $item['price_RUB']);
                $i++;
            }
        }
// Выводим HTTP-заголовки
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=$filename" );

// Выводим содержимое файла
        $objWriter->save('php://output');


        die();
    }

}
