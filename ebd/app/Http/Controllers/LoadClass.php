<?php

namespace App\Http\Controllers;

use App\Models\Satellite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LoadClass extends Controller
{
    public function loadAllOrders(){
        $satellite  =   Satellite::all()->toArray();
        foreach($satellite as $val){
            Satellite::loadOrders($val['satellite_key']);
        }
    }
}
