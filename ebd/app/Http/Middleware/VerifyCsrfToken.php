<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'admin/*/ca_*',
        'admin/*/delete*',
        'admin/*/upload_files*',
        'admin/*/delete_file/*',
        'admin/save_order_elements'
    ];
}
