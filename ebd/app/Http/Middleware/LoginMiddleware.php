<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Admin::isLoggedIn()) {
            if (strpos($_SERVER['REQUEST_URI'], 'admin/login') === false) {
                return \Redirect::to('admin/login');
            }
        }
        return $next($request);
    }
}
