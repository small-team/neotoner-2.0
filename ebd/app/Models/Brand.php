<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Brand extends SmartModel
{
    protected $fillable = ['id','title','_slug','_position'];

}
