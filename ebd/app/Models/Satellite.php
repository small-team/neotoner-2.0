<?php

namespace App\Models;

use Curl\Curl;
use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Satellite extends SmartModel
{
    private static $load_orders_action = 'load_orders';
    private static $curl_error = null;
    //
    public static function loadOrd(){
//        dd('bудем делать запрос на получение заявок');
        $curl = new Curl();
        $curl->get('http://print.site/load_orders');
        $res    =   $curl->response;
        $res    =   json_decode(trim($res), true);
//get information about applications from satellite
        $orders         =   $res['orders'];
        $orders_info    =   $res['orders_info'];
        $orders_photo   =   $res['orders_photo'];
//stores the received data
        foreach($orders as $val){
            \DB::table('cartridge_orders')->insert($val);
        }
        foreach($orders_info as $val){
            \DB::table('orders_info')->insert($val);
        }
        foreach($orders_photo as $val){
            \DB::table('orders_photo')->insert($val);
        }
    }

    public static function loadOrders($satellite_key)
    {
        $satellite_key = trim($satellite_key);

        if (empty($satellite_key)) {
            return false;
        }
        $object = Satellite::where('satellite_key',$satellite_key)->first()->toArray();

        if (!$object || !is_array($object)) {
            return false;
        }

        $uri    =   $object['url'];
        $uri .= substr($uri, strlen($uri) - 1) == '/' ? self::$load_orders_action : '/' . self::$load_orders_action;
        if (strpos($uri, 'https://') === false && strpos($uri, 'http://') === false) {
            $uri = 'https://' . $uri;
        }

        $curl = new Curl();
        $curl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
        $curl->setReferrer('http://google.com/');
        $curl->setOpt(CURLOPT_TIMEOUT, 30);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, true);
        $curl->setHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');

        $curl->get($uri,['key'=>$satellite_key]);
        if ($curl->curlErrorCode) {
            self::$curl_error = 'Error: ' . $curl->curlErrorCode . ': ' . $curl->curlErrorMessage;
            return false;
        }
        $res = $curl->response;
        $res = json_decode($res, true);

        if (!is_array($res) || empty($res) || !isset($res['orders']) || !is_array($res['orders']) || empty($res['orders'])) {
            self::$curl_error = 'Ошибка данных';
            return false;
        }

        $orders         = $res['orders'];
        $orders_info    = $res['orders_info'];
        $orders_photo   = $res['orders_photo'];

        $inserted_orders = [];

        foreach($orders as $val){
            $_id_order = $val['id_satellite_order'] = $val['id'];
            unset($val['id']);

            $new_order_id = \DB::table('cartridge_orders')->insertGetId($val);

            $inserted_orders[$_id_order] = $new_order_id;

            foreach($orders_info[$_id_order] as $val2){
                if (isset($val2['id'])) {
                    unset($val2['id']);
                }
                $val2['id_order'] = $new_order_id;
                \DB::table('orders_info')->insert($val2);
            }
            foreach($orders_photo[$_id_order] as $val3){
                $val3['id_order'] = $new_order_id;
                \DB::table('orders_photo')->insert($val3);
            }

        }
    }
}
