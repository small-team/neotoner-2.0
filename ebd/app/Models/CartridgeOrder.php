<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class CartridgeOrder extends SmartModel
{
    public $table = 'cartridge_orders';
    protected $appends = ['orders_product_info','order_photos_links','url_satellite'];

    public function getOrdersProductInfoAttribute(){
        return( \DB::table('orders_info')
            ->where('id_order',$this->id)->get());
    }

    public function getOrderPhotosLinksAttribute(){
        $url_satellite  =   \DB::table('satellites')->select('url')->where('id_satellite',$this->id_satellite)->first()->url;
        if($url_satellite[strlen($url_satellite)-1] == "/"){
            $url_satellite = substr($url_satellite,0,-1);
        }
        $photos = \DB::table('orders_photo')
            ->where('id_order',$this->id)->get();
        foreach($photos as $val){
            $val->link  = $url_satellite.$val->link;
        }
        return $photos;
    }

    public function getUrlSatelliteAttribute(){
        return( Satellite::where('id_satellite',$this->id_satellite)->select('url')->first()->toArray()['url']);
    }
}
