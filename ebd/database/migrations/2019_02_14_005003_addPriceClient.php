<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        if (!Schema::hasColumn('orders_info','price_client')) {
            Schema::table('cartridge_orders', function ($table) {
                $table->integer('price_client');
                $table->integer('sum_client');
            });
            Schema::table('orders_info', function ($table) {
                $table->integer('price_client');
                $table->integer('sum_client');
            });
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        if (Schema::hasColumn('orders_info','price_client')) {
            Schema::table('cartridge_orders', function ($table) {
                $table->dropColumn(['price_client']);
                $table->dropColumn(['sum_client']);
            });
            Schema::table('orders_info', function ($table) {
                $table->dropColumn(['price_client']);
                $table->dropColumn(['sum_client']);
            });
//        }
    }
}
