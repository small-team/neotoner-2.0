<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSatellitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satellites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('city');
            $table->string('phone');
            $table->string('email');
            $table->string('orders_email');
            $table->tinyInteger('status');
            $table->string('hosting_title');
            $table->text('hosting_tariff');
            $table->date('hosting_date_end');
            $table->string('domen_title');
            $table->text('domen_tariff');
            $table->date('domen_date_end');
            $table->string('satellite_key');
            $table->tinyInteger('price_mode');
            $table->string('icq');
            $table->string('skype');
            $table->float('coefficient');
            $table->float('rate');
            $table->integer('id_satellite');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('satellites');
    }
}
