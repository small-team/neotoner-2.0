<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCartridgePriceColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cartridge_prices','id_type')) {
            Schema::table('cartridge_prices', function ($table) {
                $table->dropColumn(['id_type']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('cartridge_prices','id_type')) {
            Schema::table('cartridge_prices', function ($table) {
                $table->integer('id_type');
            });
        }
    }
}
