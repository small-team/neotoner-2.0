<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->float('rate');
            $table->dateTime('rate_last_update');
            $table->float('custom_rate');
            $table->float('use_custom_rate');
            $table->float('cooficient_for_satellite_step');
            $table->integer('cooficient_for_satellite_from');
            $table->integer('cooficient_for_satellite_to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
