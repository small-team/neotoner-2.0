<?php

namespace App\Helpers;

use App\Helpers\SmsContract;
use Curl\Curl;

class SmsController implements SmsContract
{

    private static $url = 'https://gate.smsaero.ru/send';
    private static $user = 'neotoner.com@yandex.ru';
    private static $password = 'qwerty506';
    private static $from = 'Neotoner';

    /**
     * @param string $phone
     * @param string $text
     */
    public function sendSms($phone, $text)
    {
        $phone = str_replace([' ','(',')','-','+'],'',$phone);

        $smsaero_api = new SmsaeroApiV2('neotoner.com@yandex.ru', 'yJQdTaJkDOaVYrm1m4BwE3RNkGqL', 'news'); // api_key из личного кабинета
        $r = $smsaero_api->send($phone, $text, "DIRECT");
        if (isset($_GET['ds'])) {
            dd($r, $phone);
        }

//        $phone = str_replace([' ','(',')','-'],'',$phone);
//        $curl = new Curl();
//
//        $curl->get(self::$url,
//            [
//                'user' => self::$user,
//                'password' => md5(self::$password),
//                'to' => $phone,
//                'text' => $text,
//                'from' => self::$from,
//                'answer' => 'json'
//            ]);
//
//        if ($curl->error) {
//            self::writeLog('Error: ' . $curl->curlErrorCode . ': ' . $curl->curlErrorMessage);
//        }
//
//        $res = (array)$curl->response;
//        $msg = implode(' ----> ',$res);
//
//        self::writeLog($msg);

    }

    /**
     * @param string $phone
     * @param array $data
     */
    public function sendToClient($phone , $data = [])
    {
        if(empty($phone) || empty($data['code'])){
            self::writeLog('Invalid argument exception '. __METHOD__);
            return;
        }
        
        $text = 'Заявка от '.date('d.m.Y').' '.'№'.$data['code'].' принята в обработку. Neotoner.com';
        self::sendSms($phone, $text);
    }

    /**
     * @param string $phone
     * @param array $data
     */
    public function sendToAdmin($phone, $data = [])
    {
        if(empty($phone) || empty($data['code'])){
            self::writeLog('Invalid argument exception '. __METHOD__);
            return;
        }
        
        $name = mb_substr($data['name'],0, 14);
        $text = '№'.$data['code'].' от '.date('d.m.Y').' '.$name.' '.$data['phone'].', '.$data['city'];
        
        self::sendSms($phone, $text);
    }

    public function writeLog($msg)
    {
        app('sms_logger')->alert($msg);
    }


}

