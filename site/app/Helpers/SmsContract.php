<?php
namespace App\Helpers;

Interface SmsContract
{
    public function sendSms($phone, $text);

    public function sendToClient($phone);
    
    public function sendToAdmin($phone);
    
    public function writeLog($msg);


}