<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Cookie;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'call_back',
        'admin/*/ca_*',
        'admin/*/delete*',
        'admin/*/upload_files*',
        'admin/*/delete_file/*',
        'admin/save_order_elements',
        'admin/kcfinder-master/*',
        '/uploader/',
    ];

    public function handle($request, \Closure $next)
    {
        if ($this->isReading($request) || $this->shouldPassThrough($request) || $this->tokensMatch($request)) {
            return $this->addCookieToResponse($request, $next($request));
        }
        $request->session()->put('_token', Str::random(40));
        \Log::info('TokenMismatchException on pages search '.$request->session()->token());
        return redirect()->back();
    }
    protected function addCookieToResponse($request, $response)
    {
        $config = config('session');

        $response->headers->setCookie(
            new Cookie(
                'XSRF-TOKEN', $request->session()->token(), time() + 60 * 1440,
                $config['path'], $config['domain'], $config['secure'], false
            )
        );
        return $response;
    }

}
