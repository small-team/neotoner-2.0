<?php

namespace App\Http\Controllers;

use App\Models\CartridgeOrder;
use App\Models\CartridgePrice;
use App\Models\MetaTag;
use App\Models\Satellite;
use App\Models\Settings;
use App\Models\SmsAlert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Tests\DataCollector\DumpDataCollectorTest;
use App\Library\Cart;

class CartController extends Controller
{
    public function index()
    {
        MetaTag::getMetaForMenu('cart');
        BreadcrumbsController::getBreadCrumbs(['title'=>'Корзина','href'=>'cart']);

        if(!empty(\Session::get('order'))){
            $cart_item_all  =   \Session::get('order');
            $cart_items =[];

            if (isset($_GET['deb'])) {
                echo '<pre>'; var_dump($cart_item_all); die();
            }

            $newSearchItems = [];
            $newSearchItemsArticuls = [];

            foreach ($cart_item_all as $key=>$val) {
                if($val['checked']){
                    $cart_items[$key]=$val;
                }
                if (strpos($key, 'new-search-') === 0 || strpos($key, 'duplicate-') === 0) {
                    $newSearchItems[$key] = $val;
                    $newSearchItemsArticuls[$key] = !empty($val['articule']) ? $val['articule'] : null;
                }
            }

            if ($newSearchItemsArticuls = array_filter($newSearchItemsArticuls)) {
                $articulBrands = \DB::table('cartridge_prices')
                    ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                    ->select('cartridge_prices.articule',
                        'brands.title as brand'
                    )
                    ->whereIn('cartridge_prices.articule', $newSearchItemsArticuls)->get();
                if (count($articulBrands)) {
                    $articulBrands = collect($articulBrands)->keyBy('articule');
//                    $articulBrands = array_map(function ($ab) {
//
//                    });
                }
            }

            $cart_item_id = array_keys($cart_items);
            $cart_orders = \DB::table('cartridge_prices')
                ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                ->select('cartridge_prices.id',
                        'cartridge_prices.articule',
                        'brands.title as brand',
                        'cartridge_prices.cartridge_model',
                        'cartridge_prices.price_RUB',
                        'cartridge_prices.price_USD'
                    )
                ->whereIn('cartridge_prices.id', $cart_item_id)->get();

            $cart_orders_item   =   array();
            foreach($cart_orders as $value){
                $cart_orders_item[]=$value;
            }

            if (isset($_GET['deb1'])) {
                echo '<pre>'; var_dump($newSearchItems, $cart_item_all); die();
            }

            foreach ($newSearchItems as $key => $itemData) {
                $brand = '';
                $articul = !empty($itemData['articule']) ? $itemData['articule'] : '';
                if (!empty($articulBrands) && ($articulBrand = $articulBrands->get($articul, ''))) {
                    $brand = $articulBrand->brand;
                }

                $item = new \stdClass();
                $item->id = $key;
                $item->brand = $brand;
                $item->cartridge_model = !empty($itemData['desc']) ? $itemData['desc'] : '';
                $item->articule = $articul;
                $item->price_RUB = $itemData['sum_Client'];
                $item->price_USD = 0;
                $cart_orders_item [$key]= $item;
            }

//            echo '<pre>'; var_dump($cart_items, $newSearchItems, $cart_orders_item); die();

            return view('pages.cart',['cart_orders_item'=>$cart_orders_item]);

        }
        return view('pages.cart');
    }

    public function cart_update(Request $request)
    {
        $checked = true;
        if(!$request->input('checked')){
            $checked = false;
        }
//        receive data from the request
        $id_order   =   $request->input('id');
        $count  =    $request->input('count');
        $price  =   $request->input('price_RUB');
        $price_USD  =   $request->input('price_USD');
        $price_Client  =   $request->input('price');
        $desc  =   $request->input('desc');
        $articule  =   $request->input('articule');
        if (empty($articule) && ($cartridge = CartridgePrice::find($id_order))) {
            $articule = $cartridge->articule;
        }

        if ((empty($price) && !empty($price_Client)) || (strpos($id_order, 'new-search-') === 0 || strpos($id_order, 'duplicate-') === 0)) {
            $price = $price_Client;
        }

        $priceAllowed = Settings::checkPrice($price, $price_Client);

        $mergeArray = [];

        if (($price > 0 && !$priceAllowed) || ($price_Client === '')) {
            if ($count === '') {
                $count = 0;
            }
            if ($price_Client === '') {
                $price_Client = 0;
            }
            if (!$priceAllowed) {
                $mergeArray = [
                    'error' => true,
                    'item_price' => $price,//\Session::get("order.$id_order.sum_Client", $price),
                    'item_count' => \Session::get("order.$id_order.count", $count),
                ];
                $price_Client = $price;
            }
        }

        if ($price_Client !== '' && $price_Client >= 0) {
            //        write to the session
            \Session::put("order.$id_order.count", $count);
            \Session::put("order.$id_order.sum", $price);
            \Session::put("order.$id_order.sum_USD", $price_USD);
            \Session::put("order.$id_order.sum_Client", $price_Client);
            \Session::put("order.$id_order.checked", $checked);
            \Session::put("order.$id_order.desc", $desc);
            if (!empty($articule)) {
//                var_dump($articule); die();
                \Session::put("order.$id_order.articule", $articule);
            }
        }

        return array_merge($mergeArray, CartController::recalculation());
    }

    public function cart_item_delete(Request $request)
    {
        $id_order   =   $request->input('id');
        \Session::forget("order.$id_order");

//        CartController::recalculation();

        if ($request->getRealMethod() == 'GET') {
            return redirect()->back();
        }

        return CartController::recalculation();
    }

    public function cart_item_duplicate(Request $request)
    {
        $id_item   =   $request->input('id');
        $data = \Session::get("order.$id_item");
        $data['desc'] = '';

        $id = uniqid('duplicate-');

        \Session::put("order.$id", $data);

//        echo '<pre>'; var_dump(\Session::get("order")); die();

//        CartController::recalculation();

        if ($request->getRealMethod() == 'GET') {
            return redirect()->back();
        }

        return CartController::recalculation();
    }

    public function clean_cart()
    {
        if(!empty(\Session::get('order'))) {
            \Session::forget('order');
        }
        return redirect()->back();
    }

    public function save_order(Request $request){
        $v = \Validator::make($request->all(), [
            'hcptch' => 'required|hiddencaptcha:5,3600',
//            'phone' =>'required|digits_between:2,14',
//            'images'=>'image' 
        ]);

        if ($v->fails())
        {
//            var_dump($v->errors()); die();
            return redirect()->back()
                ->withErrors($v)
                ->withInput();
        }
        else
        {
            if(!empty(\Session::get('order'))) {
                $orders = new CartridgeOrder();

                $cart_items = \Session::get('order');
                $count_items = 0;
                $sum_items = 0;
                $sum_items_usd = 0;
                $sum_items_client = 0;

                $newSearchItems = [];

//                echo '<pre>'; var_dump($cart_items); die();

                foreach ($cart_items as $key => $value) {
                    $count_items += $value["count"];
                    $sum_items += $value["count"] * intval($value["sum"]);
                    $sum_items_usd += $value["count"] * intval($value["sum_USD"]);
                    $sum_items_client += $value["count"] * intval($value["sum_Client"]);

                    if (strpos($key, 'new-search-') === 0 || strpos($key, 'duplicate-') === 0) {
                        $newSearchItems[$key] = $value;
                    }
                }
                $id_satellite   =   Satellite::first()->toArray()['id_satellite'];

                $orders->type = $request->input('type');
                $orders->comment = $request->input('mess');
                $orders->sum = $sum_items;
                $orders->sum_usd = $sum_items_usd;
                $orders->sum_client = $sum_items_client;
                $orders->quantity = $count_items;
                $orders->fio = $request->input('name');
                $orders->city = $request->input('place');
                $orders->phone = $request->input('phone');
                $orders->email = $request->input('email');
                $orders->id_satellite = $id_satellite;
                $orders->currency = \Session::get('currency');


                $orders->save();
                $insertedId = $orders->id;
                $id_satellite = \DB::table('satellites')
                    ->select('id_satellite')
                    ->first();
                $id_satellite = $id_satellite->id_satellite;
                $code   =   $insertedId.'100'.$id_satellite;
                \DB::table('cartridge_orders')
                    ->where('id', $insertedId)
                    ->update(['code' =>$code]);

                $cart_items_ids = array_keys($cart_items);
                $carttridge_prices_item=\DB::table('cartridge_prices')
                    ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                    ->select('cartridge_prices.id as cp_id','cartridge_model','articule','price_RUB','price_USD','brands.title')
                    ->whereIn('cartridge_prices.id', $cart_items_ids)->get();

                foreach($carttridge_prices_item as $value)
                {
                    $data[] = [ 'id_order'=> $insertedId,
                        'brand'   => $value->title,
                        'model'   => $value->cartridge_model,
                        'quantity'=> $cart_items[$value->cp_id]['count'],
                        'price'   => $cart_items[$value->cp_id]['sum'],
                        'price_usd'   => $cart_items[$value->cp_id]['sum_USD'],
                        'price_client'   => $cart_items[$value->cp_id]['sum_Client'],
                        'articule'=> $value->articule,
                        'sum'     => $cart_items[$value->cp_id]['count']*$cart_items[$value->cp_id]['sum'],
                        'sum_usd'     => $cart_items[$value->cp_id]['count']*$cart_items[$value->cp_id]['sum_USD'],
                        'sum_client'     => $cart_items[$value->cp_id]['count']*$cart_items[$value->cp_id]['sum_Client'],
                    ];
                }

                foreach ($newSearchItems as $key => $itemData) {

                    $data[] = [
                        'id_order'=> $insertedId,
                        'brand'    => '',
                        'model'    => !empty($itemData['desc']) ? $itemData['desc'] : '',
                        'quantity' => $cart_items[$key]['count'],
                        'price'    => $cart_items[$key]['sum'],
                        'price_usd'     => $cart_items[$key]['sum_USD'],
                        'price_client'  => $cart_items[$key]['sum_Client'],
                        'articule'      => $itemData['articule'],
                        'sum'           => $cart_items[$key]['count']*intval($cart_items[$key]['sum']),
                        'sum_usd'       => $cart_items[$key]['count']*intval($cart_items[$key]['sum_USD']),
                        'sum_client'    => $cart_items[$key]['count']*intval($cart_items[$key]['sum_Client']),
                    ];
                }

                \DB::table('orders_info')->insert($data);

//save image
                $images =   \Input::file('images');
                $link_image_for_orders=[];
                if(!empty($images[0]))
                {
                    $i=1;
                    $link_image_for_orders=[];
                    foreach($images as $image)
                    {
                        $imageName = "order_".$insertedId."_".$i.".".$image->getClientOriginalExtension();
                        $i++;
                        $image->move(base_path() . "/public/upload/photo-zayavki/"."$insertedId", $imageName);
                        $image_link = "/upload/photo-zayavki/"."$insertedId"."/".$imageName;
                        $link_image_for_orders[]=['id_order'=>$insertedId,
                                                  'link'    => $image_link];
                    }
                    \DB::table('orders_photo')->insert($link_image_for_orders);
                }
                try {
                    $date_time_to_email =   date("d.m.y H:i:s");
                    $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
                    $adresses = explode(';',$addres_email_message);
                    $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
                    if($_address_site[strlen($_address_site)-1] == "/"){
                        $_address_site = substr($_address_site,0,-1);
                    }
                    foreach($link_image_for_orders as $key=>$val){
                        $link_image_for_orders[$key]['link']= $_address_site.$val['link'];
                    }
                    $data_mail=['code'=>$code,
                        'date'=>$date_time_to_email,
                        'name'=>$request->input('name'),
                        'phone'=>$request->input('phone'),
                        'city'=>$request->input('place'),
                        'email'=>$request->input('email'),
                        'comment'=>$request->input('mess'),
                        'site_url'=>$_address_site,
                        'type'=>'Заявка с корзины',
                        'order_info'=>$data,
                        'sum'=>$sum_items,
                        'sum_items'=>$sum_items,
                        'sum_usd'=>$sum_items_usd,
                        'sum_client'=>$sum_items_client,
                        'count_items'=>$count_items,
                        'photo_links'=>$link_image_for_orders];


                    //  ----------------- ----------------- ----------------- -----------------
                    $sms_phone = trim($request->input('phone'));
                    \Sms::sendToClient($sms_phone, $data_mail);

                    if(!empty(SmsAlert::first())){
                        \Sms::sendToAdmin(SmsAlert::first()->toArray()['phone'], $data_mail);
                    }
//    ----------------- ----------------- ----------------- -----------------


                    $from_email = $request->input('email');
//                return view('emails.cart_request',$data_mail);
                    foreach ($adresses as $_address) {
                        \Mail::send('emails.cart_request', $data_mail, function($message) use ($_address,$code,$data_mail)
                        {
                            $message->from(env('MAIL_FROM_EMAIL','info@neotoner.com'));
                            $message->replyTo($data_mail['email'], $data_mail['name']);
                            $message->to($_address)->subject("Заявка с корзины № " .$code);
                        });
                    }
                } catch (\Exception $e) {
                    \Log::info($e->getMessage().'Заявка с корзины');
                }


                \Session::forget('order');

                Cart::putInfoToView();

                return view('pages.cart',['send'=>true,'code'=>$insertedId.'100'.$id_satellite,'date'=>date('d.m.Y')]);
            }

            return view('pages.cart',['send'=>false]);
        }

        return view('pages.cart',['send'=>false]);


    }

    public function recalculation(){
        $cart_items         = \Session::get('order');
        $count_items    = 0;
        $sum_items      = 0;
        $sum_USD        = 0;
        $sum_items_client        = 0;

        if (is_array($cart_items)) {
            foreach($cart_items as $value)
            {
                if($value["checked"]!=0)
                {
                    $count_items += $value["count"];
                    $sum_items   += $value["count"] * intval($value["sum"]);
                    $sum_items_client += $value["count"] * intval($value["sum_Client"]);
                    $sum_USD          += $value["count"] * intval($value["sum_USD"]);
                }
            }
        }
        return array('count'=>$count_items,'sum'=>$sum_items,'sum_USD'=>$sum_USD,'sum_items_client'=>$sum_items_client);
    }
    
    public function changeCurrency(Request $request){
        
        if($request->has('currency')){
            \Session::put('currency', $request->get('currency'));
        }
    }

    public function openExcel(){
        $cart_items         = \Session::get('order');

        $cart_items_ids = array_keys($cart_items);
        $carttridge_prices_item=\DB::table('cartridge_prices')
            ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
            ->select('cartridge_prices.id as cp_id','cartridge_model','articule','price_RUB','price_USD','brands.title')
            ->whereIn('cartridge_prices.id', $cart_items_ids)->get();
        $count_items = 0;
        $sum_items = 0;
        $sum_items_client = 0;
        $sum_items_usd = 0;

        $newSearchItems = [];

        foreach ($cart_items as $key => $value) {
            $count_items += $value["count"];
            $sum_items += $value["count"] * $value["sum"];
            $sum_items_client += $value["count"] * $value["sum_Client"];

            if (strpos($key, 'new-search-') === 0 || strpos($key, 'duplicate-') === 0) {
                $newSearchItems[$key] = $value;
            }
        }

        foreach($carttridge_prices_item as $value)
        {
            $data[] = [
                'brand'   => $value->title,
                'model'   => $value->cartridge_model,
                'quantity'=> $cart_items[$value->cp_id]['count'],
                'price'   => $cart_items[$value->cp_id]['sum'],
                'price_client'   => $cart_items[$value->cp_id]['sum_Client'],
                'articule'=> $value->articule,
                'sum'     => $cart_items[$value->cp_id]['count']*$cart_items[$value->cp_id]['sum'],
                'sum_Client' => $cart_items[$value->cp_id]['count']*$cart_items[$value->cp_id]['sum_Client'],
            ];
        }

        foreach ($newSearchItems as $key => $itemData) {
            $data[] = [
                'brand'    => '',
                'model'    => !empty($itemData['desc']) ? $itemData['desc'] : '',
                'quantity' => $cart_items[$key]['count'],
                'price'    => $cart_items[$key]['sum'],
                'price_client'  => $cart_items[$key]['sum_Client'],
                'articule'      => $itemData['articule'],
                'sum'           => $cart_items[$key]['count']*intval($cart_items[$key]['sum']),
                'sum_Client'    => $cart_items[$key]['count']*intval($cart_items[$key]['sum_Client']),
            ];
        }

        $filename   = "nt_".date('d.m.Y').".xlsx";
        $i = 2;
        $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel2007')->load('open_excel.xlsx');
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        if(\Session::get('currency') == 'USD'){
            $objWorksheet->setCellValue("D1", 'Цена у.е');
            $objWorksheet->setCellValue("F1",'Сумма у.е.');
        }

        foreach ($data as $items => $info_items) {
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("A2"), "A$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("B2"), "B$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("C2"), "C$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("D2"), "D$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("E2"), "E$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("F2"), "F$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("G2"), "F$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("H2"), "F$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("I2"), "F$i");

            $objWorksheet->setCellValue("A".$i, $info_items['brand']);
            $objWorksheet->setCellValue("B".$i, $info_items['articule']);
            $objWorksheet->setCellValue("C".$i, $info_items['model']);

            $objWorksheet->setCellValue("D".$i, $info_items['price']);
            $objWorksheet->setCellValue("F".$i, $info_items['sum']);

            $objWorksheet->setCellValue("E".$i, $info_items['quantity']);

            $objWorksheet->setCellValue("G".$i, $info_items['price_client']);
            $objWorksheet->setCellValue("H".$i, $info_items['sum_Client']);

            $objWorksheet->setCellValue("I".$i, $info_items['price'] ? round(100*$info_items['price_client']/$info_items['price']-100) : 0);

            if ($red_price = $info_items['price_client'] && ($info_items['price_client'] != $info_items['price'])) {
                $styleArray = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FF0000'),
                    ));
                $objWorksheet->getStyle("G".$i)->applyFromArray($styleArray);
                $objWorksheet->getStyle("H".$i)->applyFromArray($styleArray);
            }

            $i++;
        }
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("A1"), "A".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("B1"), "B".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("C1"), "C".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("D1"), "D".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("E1"), "E".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("F1"), "F".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("G1"), "G".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("H1"), "H".$i);
        $objWorksheet->duplicateStyle($objWorksheet->getStyle("I1"), "I".$i);
        $objWorksheet->setCellValue("A".$i,'Итого:');
        $objWorksheet->setCellValue("E".$i, $count_items);
        if(\Session::get('currency') == 'USD'){
            $objWorksheet->setCellValue("F".$i, $sum_items_usd. " y.e.");
        }else{
            $objWorksheet->setCellValue("F".$i, $sum_items. " руб.");
        }

        $objWorksheet->setCellValue("F".$i, $sum_items. " руб.");
        $objWorksheet->setCellValue("H".$i, $sum_items_client. " руб.");

        $objWorksheet->setCellValue("I".$i, $sum_items ? round(100*$sum_items_client/$sum_items-100) : 0);

        if ($red_price = $sum_items_client && ($sum_items_client != $sum_items)) {
            $styleArray = array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FF0000'),
                ));
            $objWorksheet->getStyle("I".$i)->applyFromArray($styleArray);
            $objWorksheet->getStyle("H".$i)->applyFromArray($styleArray);
        }

        // Выводим HTTP-заголовки
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=$filename" );

// Выводим содержимое файла
        $objWriter->save('php://output');


        die();
    }


}
