<?php

namespace App\Http\Controllers;

use App\Facades\Sms;
use App\Models\Brand;
use App\Models\CartridgeOrder;
use App\Models\CartridgePrice;
use App\Models\CartridgeType;
use App\Models\District;
use App\Models\MetaTag;
use App\Models\Metro;
use App\Models\Pages;
use App\Models\Settings;
use App\Models\Slider;
use App\Models\SmsAlert;
use Curl\Curl;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StaticPagesController extends Controller
{

    public function indexHome()
    {
        $brands = Brand::all()->sortBy('_position');
        foreach($brands as $brand){
            $brand->loadFiles();
        }

        $info_page = Settings::first()->toArray();
        return view('pages.home', ['info_page' => $info_page,'brands'=>$brands]);
    }

    public function indexContacts()
    {
        BreadcrumbsController::getBreadCrumbs(['title' => 'Контакты', 'href' => 'about']);
        MetaTag::getMetaForMenu('contacts');

        $about_info = Settings::first()->toArray();

        return view('pages.contacts', [ 'about_info' => $about_info]);
    }

    public function indexUserPages($slug)
    {
        $info_page = Pages::getPageInfo($slug);

        BreadcrumbsController::getBreadCrumbs(['title' => $info_page->title, 'href' => $info_page->slug]);
        MetaTag::getMetaUserMenu($slug);

        return view('pages.user_pages', [
            'info_page' => $info_page,
            'routeParams' => [
                'slug' => $info_page->slug
            ]
        ]);
    }

    public function metro($slug)
    {
        $page = Metro::where(['slug' => $slug, 'is_active' => 1])->first();

        if (!$page) {
            throw (new ModelNotFoundException())->setModel(Metro::class);
        }

        $brands = Brand::all()->sortBy('_position');
        foreach($brands as $brand){
            $brand->loadFiles();
        }

        BreadcrumbsController::getBreadCrumbs(['title' => $page->getPageTitle(), 'href' => route('metro', ['slug' => $page->slug])]);
        MetaTag::_parseMeta([
            'meta_title' => $page->getMetaTitle(),
        ]);

        return view('pages.metro', [
            'page'   => $page,
            'brands' => $brands,
            'coords' => explode(',', $page->coords),
            'routeParams' => [
                'slug' => $page->slug
            ]
        ]);
    }

    public function district($slug)
    {
        $page = District::where(['slug' => $slug, 'is_active' => 1])->first();

        if (!$page) {
            throw (new ModelNotFoundException())->setModel(Metro::class);
        }

        $brands = Brand::all()->sortBy('_position');
        foreach($brands as $brand){
            $brand->loadFiles();
        }

        BreadcrumbsController::getBreadCrumbs(['title' => $page->getPageTitle(), 'href' => route('metro', ['slug' => $page->slug])]);
        MetaTag::_parseMeta([
            'meta_title' => $page->getMetaTitle(),
        ]);

        return view('pages.district', [
            'page'   => $page,
            'brands' => $brands,
            'coords' => explode(',', $page->coords),
            'routeParams' => [
                'slug' => $page->slug
            ]
        ]);
    }
}
