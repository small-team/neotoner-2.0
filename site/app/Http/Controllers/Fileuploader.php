<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Fileuploader extends Controller
{
    public function upload(){
//переменная пришедшая к нам с самим запросом от CKEditor
        $callback = $_GET['CKEditorFuncNum'];
        $error = '';
        $file = \Request::file('upload'); //Сам файл
//создаем ему уникальное название, чтобы нечаянно не перезаписать что-то
        $filename = md5(date("YmdHis").rand(5,50));
//получаем разрешение файла
        $extension = $file->getClientOriginalExtension();

//сохраняем файл в хранилище
        $image =   \Request::file('upload');
        $image->move(base_path() . "/public/images/uploaded/",$filename.'.'.$image->getClientOriginalExtension());
//        \Storage::disk('uploaded')->put($filename.'.'.$extension,  \File::get($file));


//формируем ссылку для ответа
        $http_path = '/images/uploaded/'.$filename.".".$extension;
//собственно сам ответ CKEditor-у
        echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );</script>";
    }
}
