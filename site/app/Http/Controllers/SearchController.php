<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\CartridgePrice;
use App\Models\MetaTag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    private function translit($str)
    {
        $rus = ['#', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
        $lat = ['№', 'A', 'B', 'B', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'H', 'O', 'P', 'P', 'C', 'T', 'U', 'F', 'X', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'b', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'h', 'o', 'p', 'p', 'c', 't', 'y', 'f', 'x', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya'];
        return str_replace($rus, $lat, $str);
    }

    public function autocomplete(Request $request)
    {

        $requestQuery = $request->get('query');
        $requestQuery = $this->translit($requestQuery);

        $keys     = explode(' ', $requestQuery);
        $is_brand = false;
        $brand    = '';

        foreach ($keys as $k => $value) {
            if (!empty(Brand::where('_slug', trim($value))->first())) {
                unset($keys[$k]);
                $is_brand = true;
                $brand    = Brand::where('_slug', $value)->first()->title;
            }
        }
        $requestQuery = implode(' ', array_diff($keys, ['']));


        $requestQuery = preg_replace('/[^a-zA-Z0-9#№]/', '', $requestQuery);
//        $splittedQuery = mb_split('',$requestQuery);
        $splittedQuery   = preg_split('//u', $requestQuery, -1, PREG_SPLIT_NO_EMPTY);
        $cartridgesQuery = '%' . implode('%', $splittedQuery) . '%';
//        dd($requestQuery,$splittedQuery,$cartridgesQuery);

        $search_result = \DB::table('cartridge_prices')
            ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
            ->select('cartridge_prices.id', 'cartridge_prices.articule', 'brands.title as brand', 'cartridge_prices.cartridge_model', 'cartridge_prices.price_RUB', 'cartridge_prices.price_USD')
            ->where(function ($query) use ($cartridgesQuery, $is_brand, $brand) {
                $query->where('active', 1);
                if ($is_brand) {
                    $query->where('brands.title', 'LIKE', "%$brand%");
                }

                $query->where(function ($query) use ($cartridgesQuery) {
                    $query->where('articule', 'like', $cartridgesQuery);
                });

            })
//            ->limit($request->get('limit'))
            ->get();

//        dd($search_result);
        if (count($search_result)) {

            $search_result = array_map(function ($item) use ($splittedQuery) {
//                $label = mb_split('',$item->articule);
                $label    = preg_split('//u', $item->articule, -1, PREG_SPLIT_NO_EMPTY);
                $qI       = 0;
                $iI       = false;
                $dist     = 1;
                $position = -1;
                $dists    = [];
                foreach ($label as $i => $c) {
                    if (strcasecmp($c, $splittedQuery[$qI]) == 0) {
                        if ($iI === false) {
                            $iI = $i;
                        } elseif ($i - $iI == 1) {
                            $dist += 1;
                            if ($position == -1) {
                                $position = $iI;
                            }
                            $iI = $i;
                        } else {
                            if ($dist > 1) {
                                $dists[] = $dist;
                            }
                            $dist = 1;
                            $iI   = false;
                        }
                        $label[$i] = '<span>' . $c . '</span>';
                        if (++$qI >= count($splittedQuery)) {
                            break;
                        }
                    }
                }
                if ($dist > 1) {
                    $dists[] = $dist;
                }
                if (empty($dists)) {
                    $dists[] = 0;
                }

                return ['value' => $item->articule, 'label' => implode('', $label), 'price' => $item->price_RUB, 'weight' => max($dists), 'pos' => $position];
            }, $search_result);

        }

//        dd($search_result);

        usort($search_result, function ($a, $b) {
            if ($a['weight'] == $b['weight']) {
                if ($a['pos'] == $b['pos']) return 0;
                return ($a['pos'] < $b['pos']) ? -1 : 1;;
            }
            return ($a['weight'] > $b['weight']) ? -1 : 1;
        });

        return $search_result;
    }

    public function search(Request $request)
    {
        MetaTag::getMetaForMenu('search');
        BreadcrumbsController::getBreadCrumbs(['title' => 'Поиск', 'href' => 'search']);

        $inputKeys = $request->input('key');

        if (!is_array($inputKeys)) {
            $inputKeys = [$inputKeys];
        }

        $inputKeys = array_filter(array_map(function ($a) {
            return trim($a);
        }, $inputKeys));

        $count                = 0;
        $multiline            = count($inputKeys) > 1;
        $cartridge_price_item = [];
        $not_found_keys       = [];
        $key_search = '';

        foreach ($inputKeys as $key) {
            $originalKey = $key;
            $key         = str_replace('-', ' ', $key);
            $key         = trim($key);
            $key         = htmlspecialchars($key);
            $key         = $this->translit($key);
            $keys        = explode(' ', $key);
            $is_brand    = false;
            $brand       = '';

            foreach ($keys as $k => $value) {
                if (!empty(Brand::where('_slug', trim($value))->first())) {
                    unset($keys[$k]);
                    $is_brand = true;
                    $brand    = Brand::where('_slug', $value)->first()->title;
                }

            }
            $keys = array_diff($keys, ['']);

            $sr = str_replace(' ', '[ -]', implode(' ', $keys));

            $search_result = [];
            if ($key != '') {

                $search_result = \DB::table('cartridge_prices')
                    ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                    ->select('cartridge_prices.id', 'cartridge_prices.articule', 'brands.title as brand', 'cartridge_prices.cartridge_model', 'cartridge_prices.price_RUB', 'cartridge_prices.price_USD')
                    ->where(function ($query) use ($sr, $is_brand, $brand) {
                        $query->where('active', 1);
                        if ($is_brand) {
                            $query->where('brands.title', 'LIKE', "%$brand%");
                        }

                        $query->where(function ($query) use ($sr) {
                            $query->where('cartridge_prices.articule', 'REGEXP', ".*$sr.*");
                            $query->orWhere('cartridge_prices.cartridge_model', 'REGEXP', ".*$sr.*");
                        });

                    })->get();

            }

            $key_search = $originalKey;

            if (empty($search_result)) {
                $not_found_keys[] = $originalKey;
                continue;
            }

            $cartridge_price_list = $this->makeSingleArray($search_result);

            $count += count($cartridge_price_list);

            foreach ($cartridge_price_list as $value) {
                if (!array_key_exists($value->brand, $cartridge_price_item)) {
                    $cartridge_price_item[$value->brand] = [];
                }
                $cartridge_price_item[$value->brand][$value->id] = $value;
            }
        }

        $view = $multiline ? 'pages.search_multi' : 'pages.search';

        return view($view, [
            'cartridge_price_item' => $cartridge_price_item,
            'key_search'           => isset($key_search) && !$multiline ? $key_search : null,
            'count'                => $count,
            'not_found_keys'       => $not_found_keys,
            'multiline'            => $multiline,
        ]);
    }

    private function makeSingleArray($arr)
    {
        if (!is_array($arr)) return false;
        $tmp = [];
        foreach ($arr as $val) {
            if (is_array($val)) {
                $tmp = array_merge($tmp, $this->makeSingleArray($val));
            } else {
                $tmp[] = $val;
            }
        }
        return $tmp;
    }
}
