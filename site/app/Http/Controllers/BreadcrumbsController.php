<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BreadcrumbsController extends Controller
{
    public static $breadCrumbs=[];


    public static function getBreadCrumbs($breadCrumbsArray)
    {
        self::$breadCrumbs[]  = $breadCrumbsArray;

    }
    public static function  printBreadCrumbs()
    {
        $html = '';
        $end_item   =   (end(self::$breadCrumbs));
        if (isset(self::$breadCrumbs) && !empty(self::$breadCrumbs)) {
            foreach(self::$breadCrumbs as $key=>$val)
            {
                if($val['title'] == $end_item['title']){
                    $html .=   "<span>".$val['title']."</span>";
                }else{
                    if(!empty($val['href'])){
                        $href   =   \URL::route($val['href']);
                    }else{
                        $href="";
                    }
                    $html .= "<a href='$href'>".$val['title']."</a> / ";
                }

            }
            return $html;
////            <a href="">Главная</a> / <a href="">Прайс-листы</a> / <span>Скупка картриджей</span>
//            $html .= '<title>'.self::$meta_data['title'].'</title>'.PHP_EOL;
        }


    }
}
