<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\CartridgeOrder;
use App\Models\MetaTag;
use App\Models\Satellite;
use App\Models\Settings;
use App\Models\SmsAlert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class RequestController extends Controller
{
    public function index()
    {
        BreadcrumbsController::getBreadCrumbs(['title'=>'Оставить заявку','href'=>'request']);
        MetaTag::getMetaForMenu('request');

        $brands = Brand::all()->sortBy('_position');
        foreach($brands as $brand){
            $brand->loadFiles();
        }

        $settings = Settings::first()->toArray();

        $info_page = \DB::table('pages')->where('slug', 'request')->first();
        return view('pages.request', ['info_page' => $info_page,'brands'=>$brands, 'seo_text' => $settings['text_seo_page_request']]);
    }

    public function store_request(Request $request)
    {
        $v = \Validator::make($request->all(),
            [
                'hcptch' => 'required|hiddencaptcha:5,3600',
//            'code'  => 'required|captcha',
//            'phone' =>'required|digits_between:2,14',
//            'images'=>'image'
            ]
        );

        if ($v->fails()) {
            return redirect()->back()
                ->withErrors($v)
                ->withInput();
        } else {
            $orders = new CartridgeOrder();
            $id_satellite   =   Satellite::first()->toArray()['id_satellite'];

            $data   =   [
                        'fio'=>$request->input('name'),
                        'phone'=>$request->input('phone'),
                        'email'=>$request->input('email'),
                        'city'=>$request->input('place'),
                        'comment'=>$request->input('mess'),
                        'type'=> $request->input('type'),
                        'id_satellite'=>$id_satellite,
            ];
            $insertedId =   CartridgeOrder::saveRequest($data);

            $code   =   $insertedId."100".$data['id_satellite'];
            CartridgeOrder::where('id', $insertedId)->update(['code' =>$code]);
////save image
            $images =   \Input::file('images');
            $link_image_for_orders=[];
            if(!empty($images[0]))
            {
                $i=1;
                $link_image_for_orders=[];
                foreach($images as $image)
                {
                    $imageName = "order_".$code."_".$i.".".$image->getClientOriginalExtension();
                    $i++;
                    $image->move(base_path() . "/public/upload/photo-zayavki/"."$code", $imageName);
                    $image_link = "/upload/photo-zayavki/"."$code"."/".$imageName;
                    $link_image_for_orders[]=['id_order'=>$insertedId,
                                        'link'=> $image_link];
                }
                \DB::table('orders_photo')->insert($link_image_for_orders);
            }
            $type_request   =   '';

            try {
                $date_time_to_email =   date("d.m.y H:i:s");
                $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
                $adresses = explode(';',$addres_email_message);
                $data_mail  =[];
                $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
                if($_address_site[strlen($_address_site)-1] == "/"){
                    $_address_site = substr($_address_site,0,-1);
                }
                foreach($link_image_for_orders as $key=>$val){
                    $link_image_for_orders[$key]['link']= $_address_site.$val['link'];
                }
                if($request->input('type') == 'quick_request'){
                    $type_request   =   'Быстрая заявка c главной';
                }elseif($request->input('type') == 'retain_request'){
                    $type_request   =   'Заявка с меню Оставить заявку';
                }
                $data_mail=['code'=>$code,
                    'date'=>$date_time_to_email,
                    'name'=>$request->input('name'),
                    'phone'=>$request->input('phone'),
                    'city'=>$request->input('place'),
                    'email'=>$request->input('email'),
                    'comment'=>$request->input('mess'),
                    'site_url'=>$_address_site,
                    'type'=>$type_request,
                    'photo_links'=>$link_image_for_orders];

                foreach ($adresses as $_address) {
//
                    $a = \Mail::send('emails.request', $data_mail, function($message) use ($_address,$code,$type_request,$data_mail)
                    {
//                        $message->from($data['email'], 'Neotoner');
                        $message->from(env('MAIL_FROM_EMAIL','info@neotoner.com'));
                        $message->replyTo($data_mail['email'], $data_mail['name']);
                        $message->to($_address)->subject($type_request." № " .$code);
                    });

                }

//  ----------------- ----------------- ----------------- -----------------
                $sms_phone = trim($request->input('phone'));
                \Sms::sendToClient($sms_phone, $data_mail);

                if(!empty(SmsAlert::first())){
                    \Sms::sendToAdmin(SmsAlert::first()->toArray()['phone'], $data_mail);
                }
//    ----------------- ----------------- ----------------- -----------------

            } catch (\Exception $e) {
//                var_dump($e->getMessage()); die();
                  \Log::info($e->getMessage().$type_request);
            }


            return redirect()->back()->with(['send'=>true,'code'=>$code,'date'=>date('d.m.Y')]);
        }
        return redirect()->back();
    }

    public function call_back(Request $request)
    {


        $orders = new CartridgeOrder();

        $orders->fio = $request->input('name');
        $orders->phone = $request->input('phone');
        $orders->type = $request->input('type');
        $orders->id_satellite=Satellite::first()->toArray()['id_satellite'];

        $orders->save();

        $insertedId = $orders->id;
        $id_satellite = \DB::table('satellites')
            ->select('id_satellite')
            ->first();
        $id_satellite = $id_satellite->id_satellite;
        $code   =   $insertedId.'100'.$id_satellite;
        \DB::table('cartridge_orders')
            ->where('id', $insertedId)
            ->update(['code' =>$code]);
        try {
            $date_time_to_email =   date("d.m.y H:i:s");
            $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
            $adresses = explode(';',$addres_email_message);
            $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
            if($_address_site[strlen($_address_site)-1] == "/"){
                $_address_site = substr($_address_site,0,-1);
            }
            $data_mail=['code'=>$code,
                'date'=>$date_time_to_email,
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'site_url'=>$_address_site,
                'type'=>'Заявка на обратный звонок',];
            foreach ($adresses as $_address) {
//
                \Mail::send('emails.request', $data_mail, function ($message) use ($_address,$code,$data_mail) {
//                    $message->from($_address, 'Neotoner');
                    $message->from(env('MAIL_FROM_EMAIL','info@neotoner.com'));
                    $message->to($_address)->subject("Заявка на обратный звонок № " .$code);
                });
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage().'Заявка на обратный звонок');
        }


        return array('send'=>true);
    }
}
