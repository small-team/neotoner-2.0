<div class="editable_field_block">
    <script type="text/javascript">

        $(window).ready(function() {
            $( "#contests-list" ).sortable({
                cursor: "move",
                distance: 10,
                grid: [ 20, 20 ],
                update: function( event, ui ) {
                    $('#actors-list input.position_input').each(function(key, item) {
                        $(item).attr('value',key+1);
                    });
                }
            });
        });

        function deleteItem(_this){
            $(_this).parents('.contest_item').remove();
        }
        var iterator = 1;
        function addContestItem() {
            var cont = $('#tmp-contest').clone();
            cont.removeAttr('id');
            console.log(cont);
            cont.find('input, select, textarea').each(function(key, item) {
                $(item).attr('name',$(item).attr('name').replace('%newid%','new-'+iterator));
            });
            cont.removeClass('none');
            $('#contests-list').append(cont);
            console.log(iterator);
            iterator++;
        }

    </script>

    <div id="contests-list" class="contests_list">
    <?php $lastpos = 1; ?>
    @if (isset($field_value) && !empty($field_value))
        @foreach ($field_value as $contest)
            <div class="contest_item">
                <div class="sort_handler fleft cp" title="Потяните, чтобы изменить порядок" style="height:15px;width:10px"><i class="fa fa-lg fa-sort"></i></div>
                <div class="fleft">
                    <div class="editable_field_block">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label class="input_label">Фестиваль</label>
                        </div>
                        <div class="title">
                            <input type="form-control input-sm input_text editable_field" value="{{$contest['contest']}}" name="data[contests][{{$contest['id']}}][contest]"/>
                        </div>
                    </div>
                    <div class="editable_field_block">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label class="input_label">Награды</label>
                        </div>
                        <div class="descr mt5">
                            <textarea class="editable_field input_text form-control" style="height:150px;" name="data[contests][{{$contest['id']}}][award]">{{$contest['award']}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="act fleft ml10">
                    <label class="radio_handler">
                        <input type="checkbox" name="data[contests][{{$contest['id']}}][delete]" value="{{$contest['id']}}" /> удалить
                    </label>
                </div>
                <input type="hidden" class="position_input" value="{{$contest['_position']}}" name="data[contests][{{$contest['id']}}][_position]" />
                <div class="clear"></div>
            </div>
            <?php $lastpos = $contest['_position']; ?>
        @endforeach
    @endif
    </div>
    <div class="add_contest cp" onclick="addContestItem();">Добавить</div>
    <div class="none contest_item" id="tmp-contest">
        <div class="sort_handler fleft cp" title="Потяните, чтобы изменить порядок" style="height:15px;width:10px"><i class="fa fa-lg fa-sort"></i></div>
        <div class="fleft">
            <div class="editable_field_block">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label class="input_label">Фестиваль</label>
                </div>
                <div class="title">
                    <input type="form-control input-sm input_text editable_field" value="" name="data[contests][%newid%][contest]"/>
                </div>
            </div>
            <div class="editable_field_block">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label class="input_label">Награды</label>
                </div>
                <div class="descr mt5">
                    <textarea class="editable_field input_text form-control" style="height:150px;" name="data[contests][%newid%][award]"></textarea>
                </div>
            </div>
        </div>
        <div class="act fleft ml10">
            <label class="radio_handler">
                <span class="del_tmp_handler" onclick="deleteItem(this);">удалить</span>
            </label>
        </div>
        <input type="hidden" class="position_input" value="{{$lastpos+1}}" name="data[contests][%newid%][_position]" />
        <div class="clear"></div>
    </div>

</div>