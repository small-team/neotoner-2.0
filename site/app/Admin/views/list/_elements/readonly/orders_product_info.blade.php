@if(!empty($field_value))
<table border="1px">
    <thead>
    <tr>
        <td>Артикул</td>
        <td>Модель</td>
        <td>Количество</td>
        <td class="_rub">Цена руб.</td>
        <td class="_usd" >Цена y.e.</td>
        <td class="_rub">Сумма руб.</td>
{{--        <td class="_usd">Сумма y.e.</td>--}}
        <td class="">Желаемая</td>
        <td class="">Сумма</td>
        <td class="">%</td>
    </tr>

    </thead>
    @if(isset($field_value) && $field_value)
        @foreach($field_value as $product)
            <?php $red_price = $product->price_client && ($product->price_client != $product->price); ?>
                    <tr>
                        <td>{{$product->articule}}</td>
                        <td>{{$product->model}}</td>
                        <td>{{$product->quantity}}</td>
                        <td class="_rub">{{$product->price}}</td>
{{--                        <td class="_usd">{{$product->price_usd}}</td>--}}
                        <td class="_rub">{{$product->sum}}</td>
                        <td class="" style="{{$red_price ? 'color: red' : ''}}">{{$product->price_client}}</td>
                        <td class="" style="{{$red_price ? 'color: red' : ''}}">{{$product->sum_client}}</td>
                        <td class="">{{ $product->price_client ? round(100*$product->price_client/$product->price-100) : '-' }}</td>
        @endforeach
        <?php $red_sum = $object['sum_client'] && ($object['sum_client'] != $object['sum']); ?>
        <tr>
            <td><b>Итог : </b></td>
            <td></td>
            <td><b>{{$object['quantity']}}</b></td>
            <td></td>
            <td class="_rub"><b>{{$object['sum']}}</b></td>
            <td class=""></td>
            <td class="_rub"><b style="{{$red_sum ? 'color: red' : ''}}">{{$object['sum_client']}}</b></td>
            <td class="_rub"><b style="{{$red_sum ? 'color: red' : ''}}">{{ $object['sum_client'] ? round(100*$object['sum_client']/$object['sum']-100) : '-' }}</b></td>
        </tr>
    @endif
</table>
@else
<table border="1px">
    <thead>
        <tr>Товаров нет</tr>
    </thead>
</table>
@endif