<div class="editable_field_block">
    <div class="btn btn-@if(isset($field_info['btn_type'])&& !empty($field_info['btn_type'])){{$field_info['btn_type']}} @else primary @endif btn-sm"
    @if(isset($field_info['link']) && !empty($field_info['link']))
    onclick="document.location.href = '{{$field_info['link']}}'; return false;"
    @endif>
    @if(isset($field_info['btn_title']))
    {{$field_info['btn_title']}}
    @else
    {{$field_info['title']}}
    @endif
    </div>
</div>