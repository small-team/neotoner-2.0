@if (isset($object['id']) && intval($object['id']))

    @if (isset($field_info['model']))
        <?php $model_for_files_upload = $field_info['model']; ?>
    @else
        <?php $model_for_files_upload = isset($module['model']) ? $module['model'] : ''; ?>
    @endif

    @if (isset($field_info['module']))
        <?php $module_for_files_upload = $field_info['module']; ?>
    @else
        <?php $module_for_files_upload = isset($module['name']) ? $module['name'] : ''; ?>
    @endif

    @if (isset($field_info['field_name']))
        <?php $name_for_files_upload = $field_info['field_name']; ?>
    @else
        <?php $name_for_files_upload = isset($field_input_name) ? $field_input_name : ''; ?>
    @endif

    @if (isset($field_info['action']))
        <?php $action_for_files_upload = $field_info['action']; ?>
    @else
        <?php $action_for_files_upload = 'upload_files'; ?>
    @endif

    <input type="hidden" value="{{$object['id']}}" class="field_key">
    <input type="hidden" value="{{$module['name']}}" class="module_name">
    <input type="hidden" value="@if (isset($field_info['file_model_name'])){{$field_info['file_model_name']}}@else{{$module['model']}}@endif" class="field_model">

    <div class="editable_field_block">
        @if (isset($field_value) && !empty($field_value))
        <div id="photo-list" class="photos_list sortable_gallery" module="{{$module['name']}}" upload_model="{{$model_for_files_upload}}">
            @foreach ($field_value as $photo)
                @if (!isset($photo['image']['sizes']['preview']['link']) || empty($photo['image']['sizes']['preview']['link'])) @continue @endif
                <div class="photo_item image_gallery_item_container" el_key="{{$photo['id']}}">
                    <div class="gallery_image_preview img-thumbnail" original-size="{{$photo['image']['link']}}" style="background-image: url('{{$photo['image']['sizes']['preview']['link']}}');"></div>
    
                    <div class="image_gallery_item_tools">
                        <label class="radio_handler" for="gallery-photo-item-active-{{$photo['id']}}">
                            <input type="checkbox" name="data[photos][{{$photo['id']}}][active]" value="{{$photo['id']}}"@if ($photo['active']) checked="checked"@endif id="gallery-photo-item-active-{{$photo['id']}}" />
                            отображать
                        </label>
                        <label class="radio_handler" for="gallery-photo-item-cover-{{$photo['id']}}">
                            <input type="radio" name="data[default_photo_id]" value="{{$photo['id']}}"@if ($photo['is_default']) checked="checked"@endif id="gallery-photo-item-cover-{{$photo['id']}}" />
                            обложка
                        </label>
                        <label class="radio_handler" for="gallery-photo-item-delete-{{$photo['id']}}">
                            <input type="checkbox" name="data[photos][{{$photo['id']}}][delete]" value="{{$photo['id']}}" id="gallery-photo-item-delete-{{$photo['id']}}" />
                            удалить
                        </label>
                    </div>
                </div>
                <input type="hidden" name="data[photos][{{$photo['id']}}][lalala]">
            @endforeach
        </div>
        @endif
        <div class="clear"></div>
    </div>
    
    <link rel="stylesheet" href="/assets/admin/files_upload/css/jquery.fileupload.css">
    
    <div class="clear"></div>
    <div>
        {{--<div style="height: 24px;  width: 65px;" onclick="uploadFiles({print_upload_files_setting $model_for_files_upload $object.id $module_for_files_upload $name_for_files_upload $field_info.multiple})" class="select_file_btn btn btn-info btn-xs">выбрать</div>--}}
        <div style="height: 23px;  width: 65px; margin-bottom: 20px" onclick="uploadFiles( {{{ Admin\Helpers\PrintUploadFilesSettings::process( $model_for_files_upload, $object[$key_field], $module_for_files_upload, $name_for_files_upload, (isset($field_info['multiple']) ? $field_info['multiple'] : false), $action_for_files_upload ) }}} )" class="select_file_btn btn btn-info btn-xs">выбрать</div>
    </div>
@else
    <div class="editable_field_block {{{ isset($field_error) && !empty($field_error) ? 'has-error' : '' }}}">
        Для добавления файлов нажмите кнопку "сохранить" или "применить"
    </div>
@endif