<div class="editable_field_block">
    <div id="contests-list" class="contests_list">
    @if (isset($field_value) && !empty($field_value))
        <table class="table table-bordered table-striped table-hover tablesorter table-data-list">
        @foreach ($field_value as $project)
            <tr>
                <td class="td_left">
                    <input id="da-{{$project['id']}}" name="data[awards][{{$project['id']}}][active]" type="checkbox"
                           class="tacw40 row_item_id select_this_item_input"
                            @if ($project['active']) checked="checked" @endif/>
                </td>
                <td class="">
                    <input type="hidden" name="data[awards][{{$project['id']}}][id_award]" value="{{$project['id']}}">
                    <label for="da-{{$project['id']}}" class="readonly_text_field">{{$project['project']}}</label>
                </td>
            </tr>
        @endforeach
        </table>
    @endif
    </div>
</div>