<?php

return [
    'config'=>[
        'title' => 'Настройки',
        'icon' => 'fa-gears',
        'sub'=>[
            'settings'=>[ 'title'=> 'Настройки сайта','icon' => 'fa-gear'],
            'satellites'=>[ 'title'=> 'Данные сателлита','icon' => 'fa-gear'],
            'acl'=>['title'=> 'Доступ','icon' => 'fa-user'],
        ]
    ],

    'orders' => [
        'title' => 'Журнал заявок',
    ],
    'news' => [
        'title' => 'Новости и статьи',
    ],
    'questions' => [
        'title' => 'Вопрос-ответ',
        'sub'=>[
            'questions_on'=>[ 'title'=> 'Одобренные'],
            'questions_off'=>[ 'title'=> 'На модерации'],
        ]
    ],
    'catalog' => [
        'title' => 'Каталоги','icon' => 'fa-tasks',
        'sub'=>[
            'brands'=>[ 'title'=> 'Производители'],
//            'cartridge_types'=>[ 'title'=> 'Типи картриджей'],

            'districts' => [
                'title' => 'Районы',
            ],
            'metro' => [
                'title' => 'Метро',
            ],
        ],
    ],
    'prices'=>[
        'title' => 'Прайс-листы','icon' => 'fa-money',
        'sub'=>[
            'cartridge_prices'=>[ 'title'=> 'Картриджи'],
        ]
    ],
    'site_menu' => [
        'title' => 'Меню сайта',
    ],

    'meta_tags' => [
        'title' => 'Мета данные',
    ],
    'pages' => [
        'title' => 'Страницы',
    ],
    'alert' => [
        'title' => 'Смс-Уведомления',
    ],


];