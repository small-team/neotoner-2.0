<?php
return [
    'title' => 'Текстовые страницы',
    'model' => 'Pages',
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],
        'content' => [
            'title' => 'Содержание',
            'type' => 'rich',
        ],
        'meta_title' => [
            'title' => 'MetaTitle',
            'type' => 'text',
        ],
        'meta_description' => [
            'title' => 'MetaDescription',
            'type' => 'text',
        ],
        'meta_keywords' => [
            'title' => 'MetaKeywords',
            'type' => 'text',
        ],
        'og_title' => [
            'title' => 'Social title',
            'type' => 'text',
        ],
        'og_description' => [
            'title' => 'Social description',
            'type' => 'text',
        ],

        'is_active' => [
            'title' => 'Опубликована',
            'type' => 'text',
        ],
        'slug' => [
            'title' => 'Ссылка',
            'type' => 'text',
        ],

        'internal_slug' => [
            'title' => 'Ссылка на страницу',
            'type' => 'text',
        ],

    ],
    'sort' => 'id DESC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','title','is_active','_slug']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;