<?php
return [
    'title' => 'Производители',
    'model' => 'Brand',
    'fields' => [
        'id' => [
            'title' => 'ID',
            'type' => 'text',
            'readonly'=> 'true',
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        '_slug' => [
            'title' => 'Транслит',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        '_position' => [
            'title' => 'Позиция',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        'meta_title' => [
            'title' => 'Meta_title',
            'type' => 'text',
        ],
        'meta_description' => [
            'title' => 'Meta_description',
            'type' => 'text',
        ],
        'meta_keywords' => [
            'title' => 'Meta_keywords',
            'type' => 'text',
        ],
        'og_title' => [
            'title' => 'Social title',
            'type' => 'text',
        ],
        'og_description' => [
            'title' => 'Social description',
            'type' => 'text',
        ],
        'seo_text' => [
            'title' => 'Заголовок   ',
            'type' => 'text',
        ],
        'top_text_for_brands' => [
            'title' => 'Текст перед списком картриджей',
            'type' => 'rich',
        ],

        'bottom_text_for_brands' => [
            'title' => 'Текст после списка картриджей',
            'type' => 'rich',
        ],
        'image' => [
            'title' => 'Изображение в списке',
            'type' => 'image',
            'description'=>'Выберите изображение размером 88x78'
        ],


    ],
    'sort' => 'id ASC',
    'tabs' => [
        'metas'=>['title'=> 'Метаданные','fields'=> ['meta_title', 'meta_description', 'meta_keywords', 'og_title', 'og_description']],
        'seo'=>['title'=> 'Покупка картриджей','fields'=> ['seo_text','top_text_for_brands','bottom_text_for_brands']],

    ],
    'actions' => [
        'list' => [
            'show' => ['id','title','_slug','_position']
        ],
        'edit' => [
            'hide' => ['id'],
        ]
    ]
]
    ;