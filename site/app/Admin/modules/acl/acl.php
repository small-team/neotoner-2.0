<?php
return [
    'title' => 'Доступ',
    'model' => 'Admin',
    'fields' => [
        'login' => [
            'title' => 'Логин',
            'type' => 'text',
        ],
         'password' => [
            'title' => 'Пароль',
            'type' => 'password',
            'description'=> 'Не заполняйте это поле, если не хотите изменить пароль'
        ],

    ],
    'sort' => 'id DESC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','login','password']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;