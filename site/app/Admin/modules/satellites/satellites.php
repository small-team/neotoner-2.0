<?php
return [
    'title' => 'Данные сателлита',
    'model' => 'Satellite',
    'fields' => [
        'code' => [
            'title' => 'Идентификатор проекта',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'title' => [
            'title' => 'Название сайта',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'description' => [
            'title' => 'Описание проекта',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'url' => [
            'title' => 'Адрес',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'city' => [
            'title' => 'Город',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'phone' => [
            'title' => 'Телефон',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'email' => [
            'title' => 'Email',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'orders_email' => [
            'title' => 'Email для оповещения о заказах',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'status' => [
            'title' => 'Статус',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'hosting_title' => [
            'title' => 'Название компании',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'hosting_tariff' => [
            'title' => 'Тарифный пакет и условия',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'hosting_date_end' => [
            'title' => 'Срок окончания',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'domen_title' => [
            'title' => 'Название компани',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'domen_tariff' => [
            'title' => 'Тарифный пакет и условия',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'domen_date_end' => [
            'title' => 'Срок окончания',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'satellite_key' => [
            'title' => 'Ключ доступа',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'price_mode' => [
            'title' => 'Режим цен',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'icq' => [
            'title' => 'ICQ',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'skype' => [
            'title' => 'Skype',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'coefficient' => [
            'title' => 'Коеффициент',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'rate' => [
            'title' => 'Курс',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'id_satellite' => [
            'title' => 'ID проекта',
            'type' => 'text',
            'readonly'=>'true'
        ],


    ],
    'sort' => 'id DESC',
    'tabs' => [
        'hosting'=>['title'=> 'Хостинг','fields'=> ['hosting_title','hosting_tariff','hosting_date_end']],
        'domen'=>['title'=> 'Домен','fields'=> ['domen_title','domen_tariff','domen_date_end']],
        'contacts_data'=>['title'=> 'Контакты','fields'=> ['phone','email','icq','skype',]],
    ],
    'actions' => [
        'list' => [
            'show' => ['id','code','title','description','url','city','phone','email','orders_email','status',
                        'hosting_title','hosting_tariff','hosting_date_end',
                        'domen_title','domen_tariff','domen_date_end',
                        'satellite_key','price_mode','icq','skype','coefficient','rate','id_satellite']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;