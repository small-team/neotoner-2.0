<?php
return [
    'title' => 'Районы',
    'model' => 'District',
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Название района',
            'type' => 'text',
        ],
        'title_where' => [
            'title' => 'Название района (где)',
            'type' => 'text',
            'description' => 'в [название]',
        ],
        'slug' => [
            'title' => 'Ссылка',
            'type' => 'text',
        ],
        'coords' => [
            'title' => 'Координаты',
            'type' => 'text',
        ],

        'is_active' => [
            'title' => 'Опубликована',
            'type' => 'checkbox',
        ],

        'seo' => [
            'title' => 'Текст',
            'type' => 'rich',
        ],

        'seo2' => [
            'title' => 'Текст 2',
            'type' => 'rich',
        ],

    ],
    'sort' => 'id DESC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','title','is_active','slug']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;