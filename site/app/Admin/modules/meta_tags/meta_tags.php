<?php
return [
'title' => 'Мета данные',
    'model' => 'MetaTag',
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],
        'slug' => [
            'title' => 'Ссылка',
            'type' => 'text',
        ],
        'meta_title' => [
            'title' => 'MetaTitle',
            'type' => 'text',
        ],
        'meta_description' => [
            'title' => 'MetaDescription',
            'type' => 'text',
        ],
        'meta_keywords' => [
            'title' => 'MetaKeywords',
            'type' => 'text',
        ],
        'og_title' => [
            'title' => 'og_title',
            'type' => 'text',
        ],
        'og_description' => [
            'title' => 'og_description',
            'type' => 'text',
        ],


    ],
    'sort' => 'id DESC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','title','is_active','_slug']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;