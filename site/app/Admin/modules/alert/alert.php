<?php
return [
    'title' => 'Смс-Уведомления',
    'model' => 'SmsAlert',
    'fields' => [
        'phone' => [
            'title' => 'Телефон администратора',
            'type' => 'text',
        ],

    ],
    'sort' => 'id ASC',

    'actions' => [
        'list' => [
            'show' => ['id','phone']
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'add' => [
            'hide' => ['id'],
        ],
    ]
]
    ;