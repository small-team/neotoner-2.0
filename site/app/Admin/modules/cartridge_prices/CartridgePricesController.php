<?php
/**
 * @date: 09.08.2014 15:15:13
 */
class CartridgePricesController extends Admin\Controllers\ListController{

    /**
     * @description List of items action
     * */
    public function anyIndex($page_number = 1) {
        /* @var SmartModel $obj */
        $class_name = 'App\\Models\\'.$this->_module['model'];
        $obj = new $class_name;
        $table = $obj->getTable();
        $query = DB::table($table);

        $this->view->setTemplate('admin::list.default');

        $sort = $this->getSortCriteria();
        if(is_object($sort)) {
            return Redirect::to('admin/' . $this->_module_name . '/');
        }
        $this->getFilterCriteria($query);

        $query->where('active','=',1);

        if(isset($sort['field']) && !empty($sort['field'])) {
            if ($sort['dir'] !== false) {
                $sort['dir'] = $sort['dir'] && in_array($sort['dir'], array('asc', 'desc')) ? $sort['dir'] : 'asc';
                $query->orderByRaw($sort['field'].' '.$sort['dir']);
            } else {
                $query->orderByRaw($sort['field']);
            }
        }

        if ($per_page = $this->getModuleParam('per_page')) {
//            Paginator::setCurrentPage($page_number);

            $currentPage = $page_number;
            \Illuminate\Pagination\Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $this->view->count_orders = count($query->get());

            $table_data = $query->paginate($per_page);

            $this->view->pager = array(
                'current_page' => $table_data->currentPage(),
                'on_page' => $table_data->perPage(),
                'pages_count' => $table_data->lastPage(),
                'results_count' => $table_data->total(),
            );
            $table_data = $table_data->items();

            if(is_array($table_data)) {
//                $model = $this->_module['model'];
                foreach ($table_data as &$item) {
                    $item = $class_name::find($item->{$this->_key});
                    $item = is_object($item) ? $item->loadFiles()->toArray() : array();
                }
            }
            $this->view->link = 'admin/'.$this->_module_name.'/';
            $this->view->pre_page_link = 'page/';
            $this->setModuleSessionParam('page',$this->view->pager['current_page']);
        } else {
            $this->view->count_orders = count($query->get());
            $table_data = $query->get();
            $table_data = ArrayTools::useValue($table_data, $this->_key);
            if(is_array($table_data)) {
                $data = array();
//                $model = $this->_module['model'];
                foreach ($table_data as $k => $id) {
                    /* @var SmartModel $object */
                    $object = $class_name::find($id);
                    $data[$k] = $object->loadFiles()->toArray();
                }
                $table_data = $data;
            }
        }
        if (!$table_data) $this->view->table_data = array();
        $this->view->table_data = $table_data;
        return $this->view->make($this);
    }

    public function loadIdBrandItems() {
        $items = \App\Models\Brand::all(array('id','title'))->keyBy('id')->toArray();

        $list = array();
        foreach ($items as $id=>$items) {
            $list[$id] = $items['title'];
        }
        return $list;
    }

    public function loadIdTypeItems() {
        $items = \App\Models\CartridgeType::all(array('id','title'))->keyBy('id')->toArray();

        $list = array();
        foreach ($items as $id=>$items) {
            $list[$id] = $items['title'];
        }
        return $list;
    }

}
