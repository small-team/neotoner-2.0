<?php
return [
    'title' => 'Картриджи',
    'model' => 'CartridgePrice',
    'per_page' => 50,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        'id_brand' => [
            'title' => 'Производитель',
            'type' => 'select',
            'readonly'=> 'true'
        ],
        'articule' => [
            'title' => 'Артикул',
            'type' => 'text',
            'readonly'=> 'true'
        ],
//        'id_type' => [
//            'title' => 'Тип картриджа',
//            'type' => 'select',
//            'readonly'=> 'true'
//        ],
        'cartridge_model' => [
            'title' => 'Модель принтера',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        'price_USD' => [
            'title' => 'Цена(USD)',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        'price_RUB' => [
            'title' => 'Цена(RUB)',
            'type' => 'text',
            'readonly'=> 'true'
        ],


    ],
    'group' => [
       'id_brand',
    ],
    'sort' => 'id ASC, articule ASC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','id_brand','articule','cartridge_model','price_RUB','price_USD']
        ],
        'edit' => [
            'hide' => ['id'],
        ]
    ],
    'filters' => [
        'id_brand' => [
            'title' => 'Производитель',
            'type' => 'select',
        ],
        'articule' => [
            'title' => 'Артикул',
            'type' => 'text',
        ],
        
    ]
]
    ;