<?php
return [
    'title' => 'Меню сайта',
    'model' => 'Menu',
    'sortability' => true,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],
        '_position' => [
            'title' => 'Позиция',
            'type' => 'text',
        ],
        'object_type' => [
            'title' => 'Тип',
            'type' => 'select',
            'items'=>['user_pages'=>'Пользовательские','fixed_pages'=>'Фиксированые']
        ],
        'id_user_pages' => [
            'title' => 'Страница',
            'type' => 'select',
            'virtual' => true,
        ],
        'id_fixed_pages' => [
            'title' => 'Страница',
            'type' => 'select',
            'virtual' => true,
        ],

        'show_head' => [
            'title' => 'Выводить в основном меню',
            'type' => 'checkbox',
        ],
        'show_footer' => [
            'title' => 'Выводить в футер-меню',
            'type' => 'checkbox',
        ],


    ],
    'sort' => '_position ASC',
    'tabs' => [ ],
    'actions' => [
        'list' => [
            'show' => ['id','title','_position','object_type','object_id','show_head','show_footer']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;