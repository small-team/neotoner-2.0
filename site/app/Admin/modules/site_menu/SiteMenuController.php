<?php
/**
 * @date: 09.08.2014 15:15:13
 */
class SiteMenuController extends Admin\Controllers\ListController
{


    public function loadIdUserPagesItems() {
                $items = \App\Models\Pages::all(array('id','title'))->keyBy('id')->toArray();
        $list = array();
        foreach ($items as $id=>$items) {
        $list[$id] = $items['title'];
        }
        return $list;
        }

    public function loadIdFixedPagesItems() {
        $items = \App\Models\MetaTag::all(array('id','title'))->keyBy('id')->toArray();
        $list = array();
        foreach ($items as $id=>$items) {
            $list[$id] = $items['title'];
        }
        return $list;
    }


}
