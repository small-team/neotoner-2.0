<?php
return [
    'title' => 'Настройки сайта',
    'model' => 'Settings',
    'fields' => [
        'edb_uri' => [
            'title' => 'Ссылка на ЕДБ',
            'type' => 'text',
        ],
        'satellite_key' => [
            'title' => 'Ключ доступа',
            'type' => 'text',
        ],
        'data_exchange' =>[
            'title'=>'Обмен данными',
            'type'=>'btn',
            'btn_type'=>'success',
            'btn_title'=>'Запустить',
            'link'=>'/data_exchange'
        ],
        'main_page_content' => [
            'title' => 'Текст для главной страницы',
            'type' => 'rich',
        ],
//        'timeblade' => [
//            'title' => 'График работы компании в Head',
//            'type' => 'text',
//        ],
//        'company_phone' => [
//            'title' => 'Телефон компании в Head',
//            'type' => 'text',
//        ],
//        'company_email' => [
//            'title' => 'Email компании в Head',
//            'type' => 'text',
//        ],
//        'company_address' => [
//            'title' => 'Адрес компании в Head',
//            'type' => 'text',
//        ],
//        'timeblade_footer' => [
//            'title' => 'График работы компании в Footer',
//            'type' => 'text',
//        ],
        'company_phone_footer' => [
            'title' => 'WhatsApp',
            'type' => 'text',
        ],
//        'footer_email' => [
//            'title' => 'Email компании в Footer',
//            'type' => 'text',
//        ],
//        'footer_address' => [
//            'title' => 'Адрес компании в Footer',
//            'type' => 'text',
//        ],


        'copyright' => [
            'title' => 'Копирайт',
            'type' => 'rich',
        ],
        'vk' => [
            'title' => 'Код для ВКонтакте',
            'type' => 'text',
        ],
        'fb' => [
            'title' => 'Код для Facebook',
            'type' => 'text',
        ],
        'google' => [
            'title' => 'Код для Google+',
            'type' => 'text',
        ],
        'twitter' => [
            'title' => 'Код для Twitter',
            'type' => 'text',
        ],
        'content_error' => [
            'title' => '404',
            'type' => 'rich',
        ],
        'metrika' => [
            'title' => 'Код для Яндекс метрики',
            'type' => 'area',
        ],
        'best_deals' => [
            'title' => 'Заголовок',
            'type' => 'rich',
        ],
        'about_address' => [
            'title' => 'Адресс',
            'type' => 'text',
        ],
        'about_contact' => [
            'title' => 'Телефоны',
            'type' => 'text',
        ],
        'about_timeblade' => [
            'title' => 'Время работы',
            'type' => 'text',
        ],
        'about_email' => [
            'title' => 'Email',
            'type' => 'text',
        ],
        'about_coordinates_l' => [
            'title' => 'Координаты - широта',
            'type' => 'text',
        ],
        'about_coordinates_d' => [
            'title' => 'Координаты - долгота',
            'type' => 'text',
        ],
        'text_page_request' => [
            'title' => 'Текст на странице оставить заявку',
            'type' => 'area',
        ],
        'text_seo_page_request' => [
            'title' => 'SEO Текст на странице оставить заявку',
            'type' => 'rich',
        ],
        'contact_content' => [
            'title' => 'Контент',
            'type' => 'rich',
        ],
        'robots' => [
            'title' => 'robots.txt',
            'virtual' => true,
            'type' => 'area',
        ],
        'client_price' => [
            'title' => 'Цена клиента',
            'type' => 'area',
            'description' => 'Формат: 4999,15;9999,20;50000,15'
        ],
//        'sitemap' => [
//            'title' => 'sitemap.xml',
//            'virtual' => true,
//            'type' => 'area',
//        ],


//        'content_car_price' => [
//            'title' => 'Контент прайсов',
//            'type' => 'rich',
//        ],


    ],
    'sort' => 'id ASC',
    'tabs' => [
                'main_page'=>['title'=> 'Текст на главной','fields'=> ['main_page_content']],
                'text_seo_page_request'=>['title'=> 'Оставить заявку','fields'=> ['text_page_request', 'text_seo_page_request']],
                'content_error'=>['title'=> '404','fields'=> ['content_error']],
                'footer'=>['title'=> 'Футер','fields'=> ['copyright','timeblade_footer',
                                                        'company_phone_footer','footer_email','footer_address',]],
//                'head'=>['title'=> 'Head (Шапка)','fields'=> ['timeblade','company_phone','company_email','company_address']],
                'best_deals'=>['title'=> 'Лучшие предложения','fields'=> ['best_deals']],
                'contacts'=>['title'=> 'Контакты','fields'=> ['about_address','about_contact','company_phone_footer','about_timeblade',
                                                                'about_email','about_coordinates_l','about_coordinates_d','contact_content']],
                'robots'=>['title'=> 'Robots.txt','fields'=> ['robots']],
//                'sitemap'=>['title'=> 'Sitemap.xml','fields'=> ['sitemap']],



    ],
    'actions' => [
        'list' => [],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;