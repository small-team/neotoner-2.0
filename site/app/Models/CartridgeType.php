<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class CartridgeType extends SmartModel
{
    protected $fillable = ['id','title','created_at','updated_at'];
}
