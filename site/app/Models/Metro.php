<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Metro extends SmartModel
{
    public $table   =   'metro';
    public $timestamps = false;

    public function getPageTitle() {
        return sprintf('Скупка картриджей - %s в районе %s', $this->title, $this->title_district);
    }

    public function getMetaTitle() {
        return sprintf('Покупка картриджей метро %s в %s', $this->title, $this->title_district_where);
    }
}
