<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Slider extends SmartModel
{
    public static $files = array(
        'image' => array(
            'sizes' => array(
                'poster'=>array(
                    'size' => '500x202',
                    'process' => 'fitOut',
                ),
            ),
        ),
    );

    public static function getBanners(){
        $banners = Slider::orderBy('_position')->get();
        return $banners;
    }

}
