<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class District extends SmartModel
{
    public $timestamps = false;

    public function getPageTitle() {
        return sprintf('Скупка картриджей - %s', $this->title);
    }

    public function getMetaTitle() {
        return sprintf('Покупка картриджей в %s', $this->title_where);
    }
}
