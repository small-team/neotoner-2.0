<?php

namespace App\Models;

use Curl\Curl;
use Faker\Provider\File;
use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Settings extends SmartModel
{
    protected $appends = ['robots','sitemap'];
    public $rules = array(
        'twitter' => 'url',
        'google' => 'url',
        'vk' => 'url',
        'fb' => 'url',
    );
    static $_settings = null;
    private static $curl_error = null;
    private static $edb_action = 'data_exchange';

    public static function getSettings() {

        if(is_null(self::$_settings)) {
            self::$_settings = \App\Models\Settings::first();
            self::$_settings = is_object(self::$_settings) ? self::$_settings->toArray() : [];
            if ($client_price = explode(';', self::$_settings['client_price'])) {
                $client_price = array_map(function ($item) {
                    $info = explode(',', $item);
                    return ['price' => $info[0], 'percent' => $info[1]];
                }, $client_price);
                self::$_settings['client_price'] = $client_price;
            }
        }
        return self::$_settings;
    }

    public static function checkPrice($price, $client_price) {
        if (($client_price < $price) || (empty($price))) {
            return true;
        }

        $settings = self::getSettings();

        $percent = 100 * ($client_price - $price) / $price;
        foreach ($settings['client_price'] as $item) {

            if ($price <= $item['price']) {
                return $percent <= $item['percent'];
            }
        }

        return false;
    }

    public static function dataExchange()
    {
        $settings = self::getSettings();
        $key = isset($settings['satellite_key']) && !empty($settings['satellite_key']) ? trim($settings['satellite_key']) : false;
        $uri = isset($settings['edb_uri']) && !empty($settings['edb_uri']) && (strpos($settings['edb_uri'], 'http://')!==false || strpos($settings['edb_uri'], 'https://')!==false)
            ? trim($settings['edb_uri'])
            : false;

        if(!$key || !$uri) {
            self::$curl_error = 'Заполните настройки сателита';
            return false;
        }

        $uri .= substr($uri, strlen($uri)-1) == '/' ? self::$edb_action : '/'.self::$edb_action;

        $orders = \DB::table('cartridge_orders')->select('id')->where('sended',0)->get();
//        dd($orders);
        $curl = new Curl();
        $curl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:30.0) Gecko/20100101 Firefox/30.0');
        $curl->setReferrer('http://google.com/');
        $curl->setOpt(CURLOPT_TIMEOUT, 30);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, true);
        $curl->setHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');

        $curl->get($uri,['key'=>$key,'orders'=>($orders ? 1 : 0)]);
        if ($curl->error) {
            self::$curl_error = 'Error: ' . $curl->curlErrorCode . ': ' . $curl->curlErrorMessage;
            return false;
        }

        $res_str = $curl->response;
        $res = json_decode(trim($res_str), true);
        if(!is_array($res) || empty($res)) {
            self::$curl_error = 'Ошибка данных';
            return false;
        }

        if(isset($res['satellite']) && is_array($res['satellite']) && !empty($res['satellite'])) {
            $satellite = Satellite::first()->toArray();
            if (!empty($satellite)) {
                unset($res['satellite']['id']);
                unset($res['satellite']['satellite_key']);
                Satellite::where('id', 1)->update($res['satellite']);
            } else {
                $res['satellite']['id'] = 1;
                Satellite::insert($res['satellite']);
            }
        }

        $coefficient = isset($res['satellite']['coefficient']) ? floatval($res['satellite']['coefficient']) : 1;
        $coefficient = $coefficient > 0 ? $coefficient : 1;

        \DB::table('cartridge_prices')->update(['active' => 0]);

        if(isset($res['cartridge_prices']) && is_array($res['cartridge_prices']) && !empty($res['cartridge_prices'])) {

            foreach ($res['cartridge_prices'] as $item) {
                $item['price_RUB'] = $item['price_RUB']*$coefficient;
                unset($item['created_at']);
                unset($item['updated_at']);
                $item['slug'] = CartridgePrice::buildSlug($item['articule']);
                if ($item['cartridge_model'] == 'EcoSys P7240cdn') {
//                    var_dump($item); die();
                }
                CartridgePrice::updateOrCreate(['id'=>$item['id']],$item);
            }
//            \DB::table('cartridge_prices')->delete();
//            CartridgePrice::insert($res['cartridge_prices']);
        }


        if(isset($res['brands']) && is_array($res['brands']) && !empty($res['brands'])) {
            $_exists_brands = [];
            foreach ($res['brands'] as $item) {
//                if($bottom_text_for_brands =  \DB::table('brands')->where('id',56)->select('bottom_text_for_brands')->first()){
//                    $item['bottom_text_for_brands'] =   $bottom_text_for_brands->bottom_text_for_brands;
//                }
                unset($item['meta_title']);
                unset($item['meta_description']);
                unset($item['meta_keywords']);
                unset($item['og_title']);
                unset($item['og_description']);
                unset($item['bottom_text_for_brands']);
                unset($item['created_at']);
                unset($item['updated_at']);
                Brand::updateOrCreate(['id'=>$item['id']],$item);
                $_exists_brands []= $item['id'];
            }
            \DB::table('brands')->whereNotIn('id',$_exists_brands)->delete();
//            Brand::insert($res['brands']);
        }
        if(isset($res['cartridge_types']) && is_array($res['cartridge_types']) && !empty($res['cartridge_types'])) {
            foreach ($res['cartridge_types'] as $item) {
                unset($item['created_at']);
                unset($item['updated_at']);

                CartridgeType::updateOrCreate(['id'=>$item['id']],$item);
            }
//            \DB::table('cartridge_types')->delete();
//            CartridgeType::insert($res['cartridge_types']);
        }
//запись о последнем обновлении
//        Q::create('settings')->update(array('last_update' => date("Y-m-d H:i:s")))->exec();
        return true;
    }

    public static function getExchangeError() {
        return self::$curl_error;
    }



    public  function getRobotsAttribute(){
        $filename = public_path().'/robots.txt';
        if (file_exists($filename)) {
            $contents = \File::get(public_path().'/robots.txt');
        } else {
            return '';
        }
        return $contents;
    }

    public  function getSitemapAttribute(){
        $filename = public_path().'/sitemap.xml';
        if (file_exists($filename)) {
            $contents = \File::get(public_path().'/sitemap.xml');
        } else {
            return '';
        }
        return $contents;
    }
    public  function setRobotsAttribute($val){
        \File::put(public_path().'/robots.txt', $val);
    }

    public  function setSitemapAttribute($val){
        \File::put(public_path().'/sitemap.xml', $val);
    }

    public static function buildSitemap() {

        $begin = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
        $end   = '</urlset>';

        $filename = public_path().'/sitemap.xml';
        $tmpFilename = public_path().'/sitemap-bcp.xml';
        $bcpFilename = public_path().'/sitemap.old';

        if (\File::exists($tmpFilename)) {
            \File::delete($tmpFilename);
        }

        \File::append($tmpFilename, $begin . PHP_EOL);

        \File::append($tmpFilename, '<url><loc>https://neotoner.com</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        \File::append($tmpFilename, '<url><loc>https://neotoner.com/pages/rules</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        \File::append($tmpFilename, '<url><loc>https://neotoner.com/request</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        \File::append($tmpFilename, '<url><loc>https://neotoner.com/questions</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        \File::append($tmpFilename, '<url><loc>https://neotoner.com/pages/regions</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        \File::append($tmpFilename, '<url><loc>https://neotoner.com/contacts</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);

        $brands = Brand::all()->sortBy('_position');
        foreach ($brands as $brand) {
            $url = str_replace('http://localhost/', 'https://neotoner.com/', route('price_brand', ['slug' => $brand->_slug]));
            \File::append($tmpFilename, '<url><loc>' . $url . '</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        }

        $news = News::where('is_active','=',1)->whereNotNull('_slug')->where('_slug','<>','')
            ->orderBy('created_at','desc')
            ->get();

        foreach ($news as $newsItem) {
            $url = str_replace('http://localhost/', 'https://neotoner.com/', route('newsitem', ['_news_slug' => $newsItem->_slug]));
            \File::append($tmpFilename, '<url><loc>' . $url . '</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        }

        $districts = District::where(['is_active' => 1])->get();

        foreach ($districts as $district) {
            $url = str_replace('http://localhost/', 'https://neotoner.com/', route('district', ['slug' => $district->slug]));
            \File::append($tmpFilename, '<url><loc>' . $url . '</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        }

        $metro = Metro::where(['is_active' => 1])->get();

        foreach ($metro as $metroItem) {
            $url = str_replace('http://localhost/', 'https://neotoner.com/', route('metro', ['slug' => $metroItem->slug]));
            \File::append($tmpFilename, '<url><loc>' . $url . '</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        }

        $price = CartridgePrice::where(['active' => 1])->get();

        foreach ($price as $priceItem) {
            if (empty($priceItem->slug)) {
                continue;
            }
            $url = str_replace('http://localhost/', 'https://neotoner.com/', route('price_position', ['slug' => $priceItem->slug]));
            \File::append($tmpFilename, '<url><loc>' . $url . '</loc><changefreq>weekly</changefreq></url>' . PHP_EOL);
        }

        \File::append($tmpFilename, $end);

        if (\File::exists($filename)) {
            \File::move($filename, $bcpFilename);
            \File::delete($filename);
        }

        \File::move($tmpFilename, $filename);
    }
}
