<?php

namespace App\Exceptions;

use App\Library\Cart;
use App\Models\News;
use App\Models\Settings;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        return parent::render($request, $e);
    }

    protected function renderHttpException(HttpException $e)
    {
        $status = $e->getStatusCode();

        if (view()->exists("errors.{$status}")) {
            if($status == 404){
                $content_page_404  = Settings::first()->toArray()['content_error'];



                $menu_item = \DB::table('menus')
                    ->select('pages.slug as user_slug',
                        'meta_tags.slug as fixed_slug',
                        'object_type',
                        'show_head',
                        'show_footer',
                        '_position',
                        'menus.title')
                    ->leftJoin('pages', 'menus.object_id', '=', 'pages.id')
                    ->leftJoin('meta_tags', 'menus.object_id', '=', 'meta_tags.id')
                    ->orderBy('_position')
                    ->get();
                $menu_footer = array_chunk($menu_item, count($menu_item) / 2 + 1);

                \View::share(['menu_item' => $menu_item, 'menu_footer' => $menu_footer]);

                //cartridge brands and count
                $info_brands = \DB::table('cartridge_prices')->select(\DB::raw('count(id_brand) as brand_count, title, brands._slug'))
                    ->rightJoin('brands', 'id_brand', '=', 'brands.id')
                    ->having('brand_count', '>', 0)
                    ->groupBy('brands.id')
                    ->orderBy('_position')
                    ->get();
                \View::share('brands', $info_brands);

                //info_company
                $info_company = \DB::table('settings')->first();
                \View::share('info_company', $info_company);

                //news
                $new_news = News::getNewNews();
                \View::share('new_news', $new_news);

                Cart::putInfoToView();
                //info_footer
                $info_footer = \DB::table('settings')->first();
                \View::share('info_footer', $info_footer);

                $info_satellite =   \DB::table('satellites')->first();
                \View::share('info_satellite', $info_satellite);
                return response()->view("errors.{$status}", ['exception' => $e,'content_page_404'=>$content_page_404], $status);
            }
            return response()->view("errors.{$status}", ['exception' => $e], $status);
        } else {
            return $this->convertExceptionToResponse($e);
        }
    }
}
