<?php

namespace App\library;

class RusMonth {

    static public function date_to_ru($str_date) {
        $ru = array( 1=>'Января',
                        'Февраля',
                        'Марта',
                        'Апреля',
                        'Мая',
                        'Июня',
                        'Июля',
                        'Августа',
                        'Сентября',
                        'Октября',
                        'Ноября',
                        'Декабря');
        $number_month   =   date('n',strtotime($str_date));
        echo $ru[$number_month];
    }

}