<?php

namespace App\Library;

class Cart {

    static public function putInfoToView() {
        //      count the number of products and their sum
        $cart_items = \Session::get('order');
        $count_items = 0;
        $sum_items = 0;
        $sum_items_client = 0;
        $sum_USD   = 0;
        if($cart_items) {
            foreach($cart_items as $value)
            {
                if($value["checked"]!=0) {
                    $count_items      += $value["count"];
                    $sum_items        += $value["count"] * intval($value["sum"]);
                    $sum_items_client += $value["count"] * intval($value["sum_Client"]);
                    $sum_USD          += $value["count"] * intval($value["sum_USD"]);
                }

            }
        }
        \View::share(['cart_items'=>$cart_items,'count_items'=>$count_items,'sum_items'=>$sum_items,'sum_items_client'=>$sum_items_client,'sum_USD'=>$sum_USD]);
    }

}