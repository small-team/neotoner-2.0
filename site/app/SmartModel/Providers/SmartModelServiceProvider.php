<?php

namespace SmartModel\Providers;

use Illuminate\Support\ServiceProvider;
use SmartModel\SmartModel;


class SmartModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $file = app_path().'/SmartModel/'.'config.php';

        $config = $this->app['files']->getRequire($file);
        $this->app['config']->set('smart-model' , $config);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
