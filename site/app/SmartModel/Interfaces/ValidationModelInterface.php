<?php namespace SmartModel\Interfaces;

interface ValidationModelInterface
{
	/**
	 * @param $data
	 * @param array $rules
	 * @throws \SmartModel\Exceptions\ValidationException
	 * @return void
	 */
	public function validate($data, $rules = []);

	/**
	 * @return array
	 */
	public function getValidationRules();

} 