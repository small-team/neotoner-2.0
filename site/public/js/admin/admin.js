window.addEvent('domready', function() {

    if ($('save-btn')) {
        $('save-btn').addEvent('click',function() {
            $('form-info').set('action', window.location.href + '?back_to_list=true');
            $('form-info').submit();
        });
    }

    initTinyMCE();
    initDataRowClick();

    $$('.delete_file_handler').each(function(item) {
        var title = item.get('title');
        if (title.indexOf(':::') !== -1) {
            var start_index = title.indexOf(':::');
            var end_index   = title.lastIndexOf(':::');
            var params      = title.substr(start_index+3, end_index-start_index-3).split('::');
            var title       = title.substr(0, start_index) + title.substr(end_index+3);
            item.set('title', title);
            item.addEvent('click', function() {
                removeFile(params[0],params[1],params[2],params[3]);
            });
        }
    });

    $$('.checkbox_handler').addEvent('click', function(evt) {
        $('checkbox-input-' + this.get('id').split('-').pop()).set('value', this.get('checked') ? 1 : 0);
    });

    if ($('general-checkbox')) {
        $('general-checkbox').addEvent('click',function(){
            this.getParent('.table_data').getElements('input[type=checkbox]').set('checked',this.get('checked'));
        });
    }
    $$('.group_delete_handler').addEvent('click', function(evt) {
        if (!confirm('Вы точно хотите удалить все выбранные элементы ?')) return false;
        $('table-data-form').set('action', $('table-data-form').get('action') + 'delete/');
        $('table-data-form').submit();
    });

    $$('.group_action_handler').addEvent('click',function(){
        if (this.get('id').indexOf('confirm') > -1) {
            if (!confirm('Вы уверены?')) return false;
        }

        $('table-data-form').set('action', $('table-data-form').get('action') + this.get('id').split('-').pop() + '/');
        $('table-data-form').submit();
    });

    $$('.module_info_tab').addEvent('click', function(evt) {
        $$('.module_info_tab').removeClass('module_info_tab_active');
        $$('.module_info_tab_content').addClass('none');
        $('module-tab-content-' + this.get('id').split('-').pop()).removeClass('none');
        this.addClass('module_info_tab_active');
    });

    $$('.cats_sub_menu.top_level').slide('hide');
    $$('.top_category_group').addEvent('click',function(){
       this.getParent('li').getElement('.cats_sub_menu.top_level').slide();
    });

    $$('.cats_sub_menu a.active').getParent('.top_level').slide('show');

    if (Locale) Locale.use('ru-RU');
    var dps = [];
    $$('.datepicker_holder').each(function(item){
        var input = item.getElement('.datepicker_input');
        var dp = new DatePicker(input,{
            timePicker: input.hasClass('timepicker_input'),
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_jqui',
            useFadeInOut: !Browser.ie,
            format: '%d.%m.%Y' + (input.hasClass('timepicker_input') ? ' %H:%M' : ''),
            'onSelect': function(date){
                var value;
                if (date instanceof Date && date.isValid()) {
                    value = date.format('db');
                } else {
                    value = '';
                }
                item.getElement('.datepicker_hidden').set('value',value);
            }
        });
        dps.push(dp);
        item.getElement('.date_toggler').addEvent('click',function(evt){
            evt.stop();
            dps.each(function(dp){dp.close();});
            input.fireEvent('focus',{target: input, type: 'focus'});
        });
    });

    if ($('filter-data-form')) {
        $('filter-to-hide').set('slide',{'duration':200});
        $('hide-filter').addEvent('click',function(){
            if ($('hide-filter').hasClass('shown')) {
                $('hide-filter').removeClass('shown');
                $('hide-filter').set('text','показать фильтр');
                Cookie.write('hide_filter',true,{'path':'/','duration':1});
            } else {
                $('hide-filter').addClass('shown');
                $('hide-filter').set('text','скрыть фильтр');
                Cookie.dispose('hide_filter');
            }
            $('filter-to-hide').slide();
        });
        if (Cookie.read('hide_filter')) {
            $('hide-filter').set('text','показать фильтр');
            $('hide-filter').removeClass('shown');
            $('filter-to-hide').slide('hide');
        }
    }

    $$('.sort_handler').each(function(item) {
        item.addEvent('click', function() {
            var field = item.get('id').split('-').pop();
            var uri = new URI(window.location.href);
            uri.set('query', 'sort=' + field);
            uri.go();
        });
    });

    $$('.auto_validate_block').each(function(item){
        new Autovalidator(item.getElement('.editable_field'),item.getElement('.errors_block'));
    });

    if ($('add-empty-row')) {
        $('add-empty-row').addEvent('click',function(){
           new Request({
               'url' : window.location.href.substr(0, window.location.href.lastIndexOf('/'))+'/get_blank_row/',
               'onComplete': function(res){
                   if (res != 'false') {
                       if ($('no-data-cell')) $('no-data-cell').destroy();
                       var tr = new Element('tr',{'class': ($('table-data-form').getElements('tr').length % 2 == 0 ? 'data_row data_row_odd': 'data_row'),'html':res})
                       $('table-data-form').getElement('table').adopt(tr);
                       tr.getElements('.auto_validate_block').each(function(item){
                           new Autovalidator(item.getElement('.editable_field'),item.getElement('.errors_block'));
                       });
                       var fx = new Fx.Scroll(window);
                       fx.toBottom();
                   }
               }
           }).post();
        });
    }

    if ($('btn-deposit-calc')) {
        $('btn-deposit-calc').addEvent('click',function(){
            tinyMCE.execCommand('mceInsertContent', false, '<div id="deposit-calc">Депозитный калькулятор</div>');
        });
    }

    if ($('btn-autocredit-calc')) {
        $('btn-autocredit-calc').addEvent('click',function(){
            tinyMCE.execCommand('mceInsertContent', false, '<div id="autocredit-calc">Калькулятор автокредита</div>');
        });
    }

    if ($('btn-corp-tariff')) {
        $('btn-corp-tariff').addEvent('click',function(){
            tinyMCE.execCommand('mceInsertContent', false, '<div id="corp-tariff-widget">Виджет подбора корторативного тарифа</div>');
        });
    }

    if ($('btn-person-list-banned')) {
        $('btn-person-list-banned').addEvent('click',function(){
            tinyMCE.execCommand('mceInsertContent', false, '<div id="person-list-banned">Блок &quot;Им не место в ПриватБанке&quot;</div>');
        });
    }

    if ($('btn-person-list-cheated')) {
        $('btn-person-list-cheated').addEvent('click',function(){
            tinyMCE.execCommand('mceInsertContent', false, '<div id="person-list-cheated">Блок &quot;Узнай лицо&quot;</div>');
        });
    }

    if ($('btn-faq')) {
        $('btn-faq').addEvent('click',function(){
            tinyMCE.execCommand('mceInsertContent', false, '<div id="faq-block">Блок &quot;ЧаВо&quot;</div>');
        });
    }

    if ($('btn-multishow-widget')) {
        $('btn-multishow-widget').addEvent('click',function() {
            var id = $('multishow-widget-id').get('value');
            if (!id) {alert('Выберите виджет из списка'); return;}
            var title = $('multishow-widget-id').getElement('option[value='+id+']').get('text');
            tinyMCE.execCommand('mceInsertContent', false, '<div class="multishow_widget" id="multishow-widget-'+id+'">Виджет Multishow '+title+'</div>');
        });
        $('multishow-widget-id').addEvent('change',function(){
            var id = $('multishow-widget-id').get('value');
            if (id) {
                $('btn-multishow-widget').set('disabled',false);
            } else {
                $('btn-multishow-widget').set('disabled',true);
            }
        })
    }

});

function initDataRowClick() {
    $$('.data_row').addEvent('click', function(evt) {
        var chk = this.getElement('input');
        if (evt.target == chk) { chk.set('checked', !chk.get('checked')); }
        if (!chk.get('checked')) {
            this.addClass('data_row_selected');
            chk.set('checked', true);
        } else {
            this.removeClass('data_row_selected');
            chk.set('checked', false);
        }
    });
}

function initTinyMCE() {
    tinyMCE.init({
        mode: 'none',
//        editor_selector : 'rich_field',

        theme: 'advanced',

        plugins : "lists,safari,pagebreak,style,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,filemanager,imagemanager",

        theme_advanced_buttons1 : "fullscreen,code,|,undo,redo,pastetext,pasteword,|,link,unlink,anchor,|,image,cleanup,bold,italic,underline,strikethrough,bullist,numlist,table,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect",
        theme_advanced_buttons2: "styleselect,fontsizeselect|,|,forecolor,backcolor",
        theme_advanced_buttons3: "",

        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,

        content_css : "/css/general.css,/css/initial.css,/css/style.css,/css/tiny_mce_css.css",

        valid_elements : "*[*]",
//        extended_valid_elements : "div*, span*",

        force_br_newlines : true,
        force_p_newlines : false,
        //forced_root_block : '',

        remove_redundant_brs: true,
        verify_css_classes : false,

        convert_urls: false,
        template_external_list_url : '/admin/tiny_mce_template_list/',
        style_formats : [

            {title : 'Заголовок 1', block : 'h1'},
            {title : 'Заголовок 2', block : 'h2'},
            {title : 'Заголовок 3', block : 'h3'},
            {title : 'Заголовок 4', block : 'h4'},
            {title : 'Заголовок 5', block : 'h5'},
            {title : 'Заголовок 6', block : 'h6'},
            {title : 'Врезка текста', block : 'blockquote'},
            {title : 'Текст-справка', block : 'div', classes : 'additional'}
        ]
    });

    $$('textarea.rich_field').each(function(item){
        tinyMCE.execCommand("mceAddControl", false, item.get('id'));
    });
}
function removeFile(uri, id, rel, name) {
        new Request.JSON({
            'url':  uri,
            'onComplete': function(res) {
                if (res && $(id)) {
                   $(id).destroy();
                }
            }
        }).post({
            name: name,
            rel: rel
        });
}

var Autovalidator = new Class({

    initialize: function(input, errors_block){
        var data = input.get('id').split('-');
        this.id = data[2];
        this.module = data[0];
        this.field = data[1];

        this.input = input;
        this.errors_block = errors_block;

        this.request = new Request.JSON({
            link: 'cancel',
            url : 'admin/'+data[0]+'/auto_validate.json',
            onComplete: function(res){
                for (var i=0; i < res.length; i++){
                    new Element('div',{'class':'input_error','text':res[i]}).inject(this.errors_block);
                }
            }.bind(this)
        });

        this.input.addEvents({
           'keydown': function(){
               clearTimeout(this.delay);
           }.bind(this),
           'keyup'  : function(){
               clearTimeout(this.delay);
               this.delay = this.sendRequest.delay(2000, this);
           }.bind(this),
           'blur' : function(){
               clearTimeout(this.delay);
               this.delay = this.sendRequest.delay(1, this);
           }.bind(this)
        });
    },

    sendRequest: function() {
        this.errors_block.set('html');
        this.request.post({
            'id' : this.id,
            'field':this.field,
            'value': this.input.get('value')
        });
    }

});