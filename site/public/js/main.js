//X-CSRF-TOKEN SETUP
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//
function add_order(sell, only_print) {
    if (only_print === undefined) {
        only_print = 0;
    }
    var fio = $('#fio').val();
    var phone = $('#phone').val();
    var email = $('#email').val();
    var comment = $('#comment').val();
    var city = $('#city').val();
    var mess = $('#mess').val();
    var comments = new Array();
    var i = 0;
    $('.comment').each(function () {
        id = $(this).attr('product-id');
        val = $(this).val();
        comments[i] = {'id': id, 'val': val};
        i++;
    });

    var location = 0;
    var services = new Array();
    if (only_print == 0) {
        var error_message = '<b>Ошибки:</b>';
        $('#fio').css('border', '1px solid #39a748');
        if (!check_valid_fio(fio)) {
            error_message += '<br/>Поле "Имя" не должно быть короче 2-х символов';
            $('#fio').css('border', '1px solid #f00');
        }
        $('#phone').css('border', '1px solid #39a748');
        if (!check_valid_phone(phone)) {
            if (phone.length < 1) {
                error_message += '<br/>Поле "Телефон" не должно быть короче 11-ти символов';
            } else {
                error_message += '<br>В поле "Телефон" были допущены ошибки';
            }
            $('#phone').css('border', '1px solid #f00');
        }
        $('#email').css('border', '1px solid #39a748');
        if (email.length > 0) {
            if (!check_valid_email(email)) {
                error_message += '<br/>В поле "Email" были допущены ошибки';
                $('#email').css('border', '1px solid #f00');
            }
        }
        else {
            error_message += '<br/>Заполните Поле "Email"';
            $('#email').css('border', '1px solid #f00');
        }

        if($('#mess').length){
            $('#mess').css('border', '1px solid #39a748');
            if (mess.length < 2) {
                    error_message += '<br/>Выберите Ваше предложение';
                    $('#mess').css('border', '1px solid #f00');
            }
        }
        //
        // if (($('.location:radio:checked').length === 0) && (sell === true)) {
        //     error_message += '<br/>Выберите Ваше местоположение';
        // }
        // else {
        //     location = $('.location:radio:checked').val();
        // }
    }
        if (error_message != '<b>Ошибки:</b>') {
            $('#error_block').html(error_message);
            $('#error_block').show();
            return false;
        } else {
            $('#error_block').hide();
        }

    $('.popup_wrap').css('display', 'block');
    $('.popup_confirm').css('display', 'block');
    
    var js = "$('#request-form').submit();";

    if (sell == true) {
        $(".button_confirm").attr("onclick", js);
    } else {
        js = "$('#cart-form').submit();";
        $(".button_confirm").attr("onclick", js);
    }
}

$('#print_cart').click(function (e) {
    var print = window.open($(this).attr('data-href'));
    window.opener.focus();
    window.close();
});

$('#search_button').click(function (e) {

   if($('#search_key').val().trim() == ''){
       return false;
   }
});

$('#multi_handler').click(function (e) {
    var isActive = $(this).hasClass('active');

    if (isActive) {
        $(this).removeClass('active');
        $('.searchtop').show();
        $('.searchtop_multi').hide();
        $('.multi_search_popup').hide();
    } else {
        $(this).addClass('active');
        $('.searchtop').hide();
        $('.searchtop_multi').show();
        $('.multi_search_popup').show();
    }

    $('#multi_search_input').val('');
});

function multiSearchClick() {
    window.location.href = '/search?'+$.param(new Object({key: $('#multi_search_input').val().split("\n")}));
}

function selectCurrency(currency) {
    if ($('#currency_' + currency).hasClass("on")) {
        return false;
    }
    $('#currency_RUB').removeClass("on");
    $('#currency_USD').removeClass("on");
    $('#current_currency').val(currency);
    var currency_rate = parseFloat($('#currency_rate').text().replace(',', '.'));

    $.ajax({
        type: 'POST',
        url: '/change_cur',
        data: {currency: currency},
        success: function (data) {

        }
    });

        if (currency == 'RUB') {
            $('.price_RUB').show();
            $('.price_USD').hide();
            $('.price_RUB_sell').show();
            $('.price_USD_sell').hide();
            $('.price_USD_th').hide();
            $('.price_RUB_th').show();
        } else {
            $('.price_RUB').hide();
            $('.price_RUB_sell').hide();
            $('.price_USD').show();
            $('.price_USD_sell').show();
            $('.price_RUB_th').hide();
            $('.price_USD_th').show();
        }

    $('#currency_' + currency).addClass("on");
}

function check_valid_fio(fio) {
    if (fio.length >= 2) {
        return true;
    } else {
        false
    }
}

function check_valid_phone(phone) {
    if (phone.length > 5) {
        var re = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,10}$/;
        var valid = re.test(phone);
        if (valid) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function check_valid_email(email) {
    if (email.length) {
        var re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
        var valid = re.test(email);
        if (valid) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
$(document).ready(function () {


    $('.quantity__inp').bind('focus',function () {
        var count = $(this).val();
        if(count < 1){
            $(this).val('');
        }
    });

    $('.quantity__inp').bind('focusout',function () {
        var count = $(this).val();
        if(count < 1){
            $(this).val(0);
        }
    });
    $('.price__inp').bind('focus',function () {
        var count = $(this).val();
        if(count < 1){
            $(this).val('');
        }
    });

    $('.price__inp').bind('focusout',function () {
        var count = $(this).val();
        if(count < 1){
            $(this).val(0);
        }
    });



    // $("[name='phone']").mask("+7 (999) 999-99-99", {
    //     completed: function () {
    //         $("[name='phone']").val(this.val());
    //     }
    // });
    $(".phone_mask").mask("+7 (999) 999-99-99", {
        completed: function () {
            $(".phone_mask").val(this.val());
        }
    });
    $('.clin').click(function (event) {
        location.href = $(this).attr('data-href');
    });

    $('.icon_delete').click(function (e) {
        var id = $(this).attr('id');
        var group_type = $(this).attr('data-filter');
        $.ajax({
            type: 'POST',
            url: '/cart_item_delete',
            data: {id: id},
            success: function (data) {
                $(".cart_num").text(data.count);
                $(".cart_sum").text(data.sum);
                $(".cart_sum_USD").text(data.sum_USD);


                $('#tr-' + id).remove();
                if ($(".type-" + group_type).length == 0) {
                    $("#type-" + group_type).remove();
                }
            }
        });

    });
    function cartUpdate(id, count) {

        $('.popup_wrap').css('display', 'none');
        $('.popup_price_error').css('display', 'none');

        // console.log(id, count);
        var price_RUB = $(".price_RUB-" + id).text();
        var price_USD = $(".price_USD-" + id).text();
        var price = $("#price-" + id).val();
        var desc = $("#desc-" + id).val();
        var articule = $("#articule-" + id).val();
        if (count > 100000) {
            count = 100000;
        }
        count = parseInt(count, 10);
        if (isNaN(count)) {
            count = 0;
        }
        var checked = $('.checked-' + id).val();
        if (count > 0) {
            checked = 1;
            $("#id_check-" + id).prop('checked', checked);
        }
        else {
            checked = 0;
            $("#id_check-" + id).prop('checked', false);
        }
        var data = {
            'id': id,
            'price_RUB': price_RUB,
            'price_USD': price_USD,
            'price': price,
            'count': count,
            'checked': checked,
            'desc': desc,
            'articule': articule
        };
        //use in a cart
        var sum = price;
        // console.log("#order_" + id+".price", $("#order_" + id+".price"));
        $("#order_" + id+".price").text(sum * count);
        //
        // var sum_USD = price_USD;
        // $("#order_" + id+".price_USD").text(sum_USD*count);

        // console.log(data);

        if (count == 0) {

            $.ajax({
                type: 'POST',
                url: '/cart_item_delete',
                data: data,
                success: function (data) {
                    $(".cart_num").text(data.count);
                    $(".cart_sum").text(data.sum_items_client);
                    $(".cart_sum_USD").text(data.sum_USD);
                    //
                    $("#cart_count").text(data.count + "шт.");
                    $("#cart_sum").text(data.sum_items_client);
                }
            });
        }
        else {
            $('.full_cart').show();
            $('.empty_cart').hide();

            $.ajax({
                type: 'POST',
                url: '/cart_update',
                data: data,
                success: function (data) {
                    // console.log(data);
                    if (typeof data.error != "undefined") {
                        $('.popup_wrap').css('display', 'block');
                        $('.popup_price_error').css('display', 'block');

                        // $("#price-" + id).val(data.price);
                        // $(".quantity__inp.item_" + id).val(data.count);
                    }

                    if (typeof data.item_price != "undefined") {
                        $("#price-" + id).val(data.item_price);
                        sum = data.item_price;
                        // $(".quantity__inp.item_" + id).val(data.count);
                    }

                    if (typeof data.item_count != "undefined") {
                        $(".quantity__inp.item_" + id).val(data.item_count);
                        count = data.item_count;
                    }

                    $("#order_" + id+".price").text(sum * count);

                    $(".cart_num").text(data.count);
                    $(".cart_sum").text(data.sum_items_client);
                    $(".cart_sum_USD").text(data.sum_USD);

                    $("#cart_count").text(data.count + "шт.");
                    $("#cart_sum").text(data.sum_items_client);
                }
            });
        }
    }

    if($(".float_price").length){
        var objToStick = $(".float_price"); //Получаем нужный объект
        var priceTableHead = $(".l-table_price tr.head"); //Получаем нужный объект

        var topOfObjToStick = $(objToStick).offset().top; //Получаем начальное расположение нашего блока
        var heightOfObjToStick = $(objToStick).height(); //Получаем начальное расположение нашего блока
        var topOfBottomText = $('.bottom_text_for_brands').length ? $('.bottom_text_for_brands').offset().top : false;

        $(window).scroll(function () {
            var windowScroll = $(window).scrollTop(); //Получаем величину, показывающую на сколько прокручено окно

            if (windowScroll > topOfObjToStick && (windowScroll + heightOfObjToStick < topOfBottomText)) {
                $(objToStick).addClass("topWindow");
                $(priceTableHead).addClass("topWindow");
                $('.go_top_block').show();
                $('.float_right_block').css('margin-right','133px');
                    // if($(window).width()> 1700){
                    //     $('.topWindow').css('right', 380);
                    // }
            } else {
                $(objToStick).removeClass("topWindow");
                $(priceTableHead).removeClass("topWindow");
                $('.go_top_block').hide();
                $('.float_right_block').css('margin-right','0');
            };
        });

    }
    if ($('.number').length) {
        $('.number').keypress(function (event) {
            event = event || window.event;
            if (event.charCode && (event.charCode < 48 || event.charCode > 57))// проверка на event.charCode - чтобы пользователь мог нажать backspace, enter, стрелочку назад...
                return false;
        });
    }
    $('.quantity__inp').keyup(function(e) {
        var id  =   $(this).attr('id');
        var count = $(this).val();
        cartUpdate(id, count);
    });
    $('.price__inp').keyup(function(e) {
        var id  =   $(this).attr('id').replace('price-', '');
        var count = $(this).closest('.item_row').find('.quantity__inp').val();
        if (count > 0) {
            cartUpdate(id, count);
        }
    });
    $('.item_desc_input').keyup(function(e) {
        var id  =   $(this).attr('id').replace('desc-', '');
        var count = $(this).closest('.item_row').find('.quantity__inp').val();
        if (count > 0) {
            cartUpdate(id, count);
        }
    });

    $('.upload_block').click(function (e) {
        $('.content-body-form-file__file').click();
    });

    $('.cart_add').change(function (event) {
        var id = $(this).data('id');
        var count = $(this).val();
        cartUpdate(id, count);
    });

    $('.content-table__check').change(function (e) {

        var id = $(this).attr('data-id');
        var price_RUB = $(".price_RUB-" + id).text();
        var price_USD = $(".price_USD-" + id).text();
        var count = $(".cart_add-" + id).val();
        if (count > 100000) {
            count = 100000;
        }
        if (count == 0) {
            $(".cart_add-" + id).val(1);
            count = 1;
        }
        count = parseInt(count, 10);
        if (isNaN(count)) {
            count = 0;
        }

        var checked = $('#id_check-' + id).prop("checked");
        if (checked) {
            checked = 1;
        } else {
            checked = 0;
        }
        var data = {'id': id, 'price_USD': price_USD, 'price_RUB': price_RUB, 'count': count, 'checked': checked};
        if (checked == 1) {
            $.ajax({
                type: 'POST',
                url: '/cart_update',
                data: data,
                success: function (data) {
                    $(".cart_num").text(data.count);
                    $(".cart_sum").text(data.sum);
                    $(".cart_sum_USD").text(data.sum_USD);

                    $("#cart_count").text(data.count + "шт.");
                    $("#cart_sum").text(data.sum);
                }
            });
        }
        else {
            $.ajax({
                type: 'POST',
                url: '/cart_update',
                data: data,
                success: function (data) {
                    $(".cart_num").text(data.count);
                    $(".cart_sum").text(data.sum);
                    $(".cart_sum_USD").text(data.sum_USD);

                    $("#cart_count").text(data.count + "шт.");
                    $("#cart_sum").text(data.sum);
                }
            });
        }
    });


    $('a[hrefpop="true"]').addClass('highslide');
    $('a[hrefpop="true"]').click(function () {
        return hs.expand(this);
    });

    /*---------------------------выпадающее меню ---*/
    $('.navi li').hover(function () {
        clearTimeout($.data(this, 'timer'));
        $('ul', this).stop(true, true).slideDown(400);
    }, function () {
        $.data(this, 'timer', setTimeout($.proxy(function () {
            $('ul', this).stop(true, true).slideUp(200);
        }, this), 100));
    });
    $('#print_order').click(function () {
        add_order(0, 1);
        yaCounter28909060.reachGoal('PRINT-ZAJAVKU-FORMAZAJAVKI');
    });
    $('#send_order').click(function () {
        add_order();
    });
    $('#popup_client_price_handler').click(function () {
        $('.popup_wrap').css('display', 'block');
        $('.popup_client_price').css('display', 'block');
    });

    /*-------------yandex metrika---------------------*/
    $('#main_load_price').click(function () {
        yaCounter28909060.reachGoal('DOWNLOAD-PRICELIST-GLAVNAYA');
    });
    $('#brand_price').click(function () {
        yaCounter28909060.reachGoal('DOWNLOAD-PRICELIST-BREND');
    });


    /*-------------yandex metrika---------------------*/
    $('a.l-table_price_hidden_link').click(function () {

        if ($(this).text() == 'Скрыть') {
            $('tr.l-table_product_' + $(this).data('hide-id')).hide()
            $(this).text('Показать')
        }
        else {
            $('tr.l-table_product_' + $(this).data('hide-id')).show()
            $(this).text('Скрыть')
        }
        return false
    });

    $('input.cart_clear').click(function () {
        if (!confirm('Удалить все позиции из корзины?'))
            return false;
        yaCounter28909060.reachGoal('CLEAR-ZAJAVKU-FORMAZAJAVKI ');  //yandex
        $.post('ClearCart/', {
            type: 'ajaxCart',
            action: 'ClearCart'
        }, function (data) {
            $('div.l-main_order').remove()
            window.document.location = "/";
        }, 'json')
    });

    /*------------------order_sell ---------------------*/
    jQuery('.order_services input[type="checkbox"]').each(function (i) {
        jQuery(this).change(function () {
                var richtext = jQuery(this).parent().find('textarea');
                var checked = jQuery(this).is(':checked');
                if (checked) {
                    richtext.attr('disabled', false);
                }
                else {
                    richtext.attr('disabled', true);
                }
            }
        );
    });
});

function changeCount(value, productId, type) {
    $.post('addToCart/', {
        type: 'ajaxCart',
        action: 'addToCard',
        data: {productId: productId, type: type, count: value}
    }, function (data) {
        if ($('#current_currency').val() == 'USD') {
            var currency_rate = parseFloat($('#currency_rate').text().replace(',', '.'));
            var res = parseFloat(data.sum) / currency_rate;
            $('#all_price_sum').text(res.toFixed(0));
        } else {
            $('#all_price_sum').text(data.sum.toFixed(0))
        }
        var old_price_sum = parseFloat($('#price_' + productId + '-' + type).text());
        var new_price_sum = old_price_sum * parseFloat(value);
        if ((new_price_sum + "").indexOf(".") > 0) {
            $('#sum_' + productId + '-' + type).text(new_price_sum.toFixed(0));
        } else {
            $('#sum_' + productId + '-' + type).text(new_price_sum);
        }
        if ($('#tr_' + productId + ' select').val() === '0') {
            if ($('.zakaz tr').length < 4) {
                window.location.href = '/';
            }
            $('#tr_' + productId).remove();

        }

    }, 'json')
}

function map_1() {

    var about_coordinates_l = $('#map').attr('data-about_coordinates_l');
    var about_coordinates_d = $('#map').attr('data-about_coordinates_d');
    var data_address = $('#map').attr('data-address');
    //var styles = [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    //var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});

    var latlng = new google.maps.LatLng(about_coordinates_l, about_coordinates_d);
    //var latlng2 = new google.maps.LatLng(48.6232863,22.2961346);
    var options = {
        zoom: 16,
        panControl: false,
        mapTypeControl: false,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), options);
    var image = 'http://avtomotoresurs.ru/stern/img/icons/map-icon.png';
    //var image2 = '/templates/zyx/img/ico-akon.png';
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: data_address
        //icon: image
    });
    /*
     var marker = new google.maps.Marker({
     position: latlng2,
     map: map,
     icon: image2
     });*/
    //map.mapTypes.set('map_style', styledMap);
    //map.setMapTypeId('map_style');
}
if ($("#map").length) {
    // map_1();
}

//    popup
$('.header__callback').click(function (e) {
    e.preventDefault();
    $('.popup_window').css('display', 'block');
    $('#title_call_true').css('display', 'none');
    $('#title_call').css('display', 'block');
    //$('.call_none').css('display','block');
    //$('.popup_window').innerHeight(290);
    //$('.popup_window').innerWidth(350);
    $('.popup_wrap').css('display', 'block');
});


$('.make_request').click(function (e) {
    e.preventDefault();
    add_order(true);
});




$('.popup_wrap').click(function (e) {
    e.preventDefault();
    $('.popup_window').css('display', 'none');
    $('.popup_price_error').css('display', 'none');
    $('.popup_confirm').css('display', 'none');
    $('.popup_client_price').css('display', 'none');
    $('.popup_wrap').css('display', 'none');
});


$('.icon_delete_popup').click(function (e) {
    e.preventDefault();
    $('.popup_window').css('display', 'none');
    $('.popup_price_error').css('display', 'none');
    $('.popup_confirm').css('display', 'none');
    $('.popup_client_price').css('display', 'none');
    $('.popup_wrap').css('display', 'none');
});


$("#btn_call_back").bind("submit", function (event) {
    event.preventDefault();
    var name = $("#name_call").val();
    var phone = $("#phone_call").val();
    var error = 0;
    if (name.length < 2) {
        $("#name_call").addClass('error_border');
        error ++;
    }else {
        $("#name_call").removeClass('error_border');
    }

    if (phone.length < 2) {
        error ++;
        $("#phone_call").addClass('error_border');
    }else{
        $("#phone_call").removeClass('error_border');
    }

    if(error > 0 ){
        return false;
    }

    var type = $("#type_call").val();
    var data = {
        'name': name,
        'phone': phone,
        'type': type
    };
    $.ajax({
        type: 'POST',
        url: '/call_back',
        data: data,
        beforeSend:function () {
            $('.popup_window').fadeTo(500, 0.8);
            $('.load_gif').show();
        },
        success: function (data) {
            if (data.send) {

                $('.load_gif').hide();
                $('.popup_window').fadeTo(500, 1);
                //$('#name_call').val('');
                //$('#phone_call').val('');
                $("#btn_call_back").trigger('reset');
                $('#title_call_true').css('display', 'block');
                $('#title_call').css('display', 'none');
                //$('.call_none').css('display','none');
                //$('.popup_window').innerHeight(90);
                //$('.popup_window').innerWidth(580);
            }
        }
    });


});