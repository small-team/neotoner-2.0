/**
* Code from http://www.boutell.com/newfaq/creating/printpart.html
image <a class="control" onclick="return hs.getExpander(this).printIframe()" href="#">Print</a>
html <a class="control" onclick="return hs.getExpander(this).printHtml()" href="#">Print</a>
*/
hs.print= function (el)
{
   var exp = hs.expanders[hs.getWrapperKey(el)];
   if (exp.isImage)
   {
    return hs.printImage(el);
   }
   else
   {
    return hs.getExpander(el).printHtml()
   }
}

hs.printImage = function (el)
{
   var exp = hs.expanders[hs.getWrapperKey(el)];
   link = "about:blank";
   var pw = window.open(link, "_new");
   pw.document.open();
   pw.document.write(exp.getPrintPage());
   pw.document.close();
   return false;
};
hs.Expander.prototype.getPrintPage = function()
{
   // We break the closing script tag in half to prevent
   // the HTML parser from seeing it as a part of
   // the *main* page.

   return "<html>\n" +
      "<head>\n" +
      "<title>Печать изображения</title>\n" +
      "<script>\n" +"function step1() {\n" +
      "  setTimeout('step2()', 10);\n" +
      "}\n" +
      "function step2() {\n" +
      "  window.print();\n" +
      "  window.close();\n" +
      "}\n" +
      "</scr" + "ipt>\n" +
      "</head>\n" +
      "<body onLoad='step1()'>\n" +
      "<img src='" + this.content.src + "'/>\n" +
      "</body>\n" +
      "</html>\n";
};

hs.Expander.prototype.printIframe = function () {
   var name = this.iframe.name;
   frames[name].focus();
   frames[name].print();
   return false;
};

hs.Expander.prototype.printHtml = function ()
{
    var pw = window.open("about:blank", "_new");
    pw.document.open();
    pw.document.write(this.getHtmlPrintPage());
    pw.document.close();
    return false;
};
hs.Expander.prototype.getHtmlPrintPage = function()
{
    // We break the closing script tag in half to prevent
    // the HTML parser from seeing it as a part of
    // the *main* page.
    var body = hs.getElementByClass(this.innerContent, 'DIV', 'highslide-body')
        || this.innerContent;

    return "<html>\n" +
        "<head>\n" +
        "<title>Печать изображения</title>\n" +
        "<script>\n" +"function step1() {\n" +
        "  setTimeout('step2()', 10);\n" +
        "}\n" +
        "function step2() {\n" +
        "  window.print();\n" +
        "  window.close();\n" +
        "}\n" +
        "</scr" + "ipt>\n" +
        "</head>\n" +
        "<body onLoad='step1()'>\n" +
        body.innerHTML +
        "</body>\n" +
        "</html>\n";
};

hs.Expander.prototype.onAfterExpand = function (sender, e) {

   // create a new DOM element
   var div = document.createElement('div');

   // add a class name to allow CSS styling
   div.className = "highslide-overlay";

   // add print link
   div.innerHTML = '<a href="#" onclick="return hs.print(this)"><img alt="Печать" src=js/libs/highslide/graphics/print.gif" /></a>';

   // optional: set full width based on the image size minus the CSS padding
   //div.style.width = (sender.x.span - 20) +'px';

   // attatch it to this hs.Expander instance and add some options
   sender.createOverlay( { overlayId: div, position: "top left", 
      hideOnMouseOut: true, opacity: 0.9 } );
}