hs.graphicsDir = 'js/libs/highslide/graphics/';
hs.restoreTitle = 'Нажмите для закрытия картинки';
hs.loadingText = 'Загрузка...';
hs.loadingTitle = 'Нажмите для отмены';
hs.fullExpandTitle = 'Развернуть до фактических размеров';
hs.previousText = 'Предыдущая';
hs.nextText = 'Следующая';
hs.moveText = 'Переместить';
hs.closeText = 'Закрыть';
hs.closeTitle = 'Нажмите для закрытия';
hs.resizeTitle = 'Изменить размер';

hs.showCredits = false;
hs.numberOfImagesToPreload = 0;
hs.fadeInOut = true;
hs.cacheAjax = false;
hs.allowMultipleInstances = false;
hs.captionEval = 'this.thumb.alt';
