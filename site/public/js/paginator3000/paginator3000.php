<?
class paginator3000 {
    public $num_positions = 30; //Позиция на странице
    public $pg = 1;				//Номер страницы
    public $from = 0;
    public $num_pages = 10;		//Количество страниц
    public $url_pager = 'page/9.html?pg='; //Ссылка которую подсталять
	public $num_line = 10; //Количество страниц выводить в линии, если больше, появляется бегунок

    function  __construct() {
		global $oCfg;
        echo '<link rel="stylesheet" type="text/css" href="'.$oCfg->url.'mod/paginator3000/paginator3000.css" />
              <script type="text/javascript" src="'.$oCfg->url.'mod/paginator3000/paginator3000.js"></script>';

        $clear_url = 'http://'. $_SERVER['HTTP_HOST'] . preg_replace('/[&?]pg=[0-9]+|[&?]+pg=/i', '', $_SERVER['REQUEST_URI']);
		$this->url_pager = $clear_url . (strpos($clear_url,'?') === false ? '?' : '&') .'pg=';
    }

    function get_res ($res)
    {
		$this->num_objects = count($res);
        $this->num_pages = ceil($this->num_objects / $this->num_positions);
        $this->pg = (isset($_GET['pg']) && is_numeric($_GET['pg'])) ? $_GET['pg'] : 1;
        
        if ($this->pg > $this->num_pages)
			$this->pg = $this->num_pages;
			
        $this->from = ($this->pg - 1) * $this->num_positions;
        return array_slice($res, $this->from, $this->num_positions,true);
    }

    function paginator()
    {
        if ($this->num_pages > 1 and $this->num_objects > 0)
		{
			$links = '';
			for ($i = 1; $this->num_pages >= $i; $i++)
			{
				$links .= '<a href="'.$this->url_pager.$i.'">'.$i.'</a> ';
			}
			return '
			<div class="paginator" id="paginator_example">'.$links.'</div>
			<script type="text/javascript">
			$(document).ready(function() {
				var paginator_example = new Paginator(
					"paginator_example",
					'.$this->num_pages.',
					'.$this->num_line.',
					'.$this->pg.',
					"'.$this->url_pager.'"
				);
			});
			</script>';
		}
        else return false;
    }
}
