window.onload = function() {
    if (document.getElementById('dev-tools-wrapper')) {
        ffDevTools.initialize();
    }
}

window.onunload = function(){
    setCookie('ffdev_console_history', ffDevTools.console_history.dump());
}

var ffDevTools = {

    dev_tools_wrapper: null,
    dev_tools_bar: null,
    dev_tools_close_button: null,
    switchers: [],
    console_input: null,
    console_display: null,
    loading_indicator: null,

    initialize: function() {
        this.dev_tools_wrapper = document.getElementById('dev-tools-wrapper');
        this.dev_tools_wrapper.onclick = ffDevTools.showContainer;

        this.dev_tools_close_button = document.getElementById('dev-tools-close-button')
        this.dev_tools_close_button.onclick = ffDevTools.closeContainer;
        var switchers = ['cp','db','log','console'];
        for (var i = 0; i < switchers.length; i++) {
            this.switchers[i] = document.getElementById('switcher-'+switchers[i]);
            this.switchers[i].onclick = ffDevTools.activateTab;
        }

        this.console_input = document.getElementById('console-input');
        this.console_input.onkeyup = ffDevTools.consoleInputKeyUp;

        this.console_display = document.getElementById('console-display');

        this.loading_indicator = document.getElementById('loading-indicator');

        this.dev_tools_bar = document.getElementById('dev-tools-bar');
        document.body.appendChild(this.dev_tools_bar);

        ffDevTools.console_history.load(getCookie('ffdev_console_history'));
        ffDevTools.activateTab({target:{id:getCookie('ffdev_container_tab') ? getCookie('ffdev_container_tab') : 'switcher-console'}});
        if (getCookie('ffdev_container_state') == 'open') {
            ffDevTools.showContainer();
        }
    },

    consoleInputKeyUp: function(evt) {
        if (!(e = ffDevTools.getKeyCode(evt))) return true;

        if (e == 27) {
            ffDevTools.console_input.value = '';
        } else if (e == 37) {
            //left
        } else if (e == 38) {
            //up
            if (ffDevTools.console_history.isLast() && ffDevTools.console_history.getLast() != ffDevTools.console_input.value) {
                ffDevTools.console_input.value = ffDevTools.console_history.getLast();
            } else {
                ffDevTools.console_input.value = ffDevTools.console_history.getPrev();
            }
        } else if (e == 39) {
            //right
        } else if (e == 40) {
            //down
            if (ffDevTools.console_history.isLast()) {
                if (ffDevTools.console_history.getLast() == this.console_input.value) {
                    ffDevTools.console_input.value = '';
                }
            } else {
                ffDevTools.console_input.value = ffDevTools.console_history.getNext();
            }
        } else if (e == 13) {
            var value = ffDevTools.console_input.value;
            ffDevTools.console_input.value = '';
            ffDevTools.console_display.innerHTML += '> '+value+'<br/>';
            ffDevTools.console_history.add(value);

            if (value == 'clear') {
                ffDevTools.console_display.innerHTML = '';
                return true;
            }

            var url = ffDevTools.parseCL(value);

            ffDevTools.loading_indicator.style.visibility = 'visible';
            ffDevTools.xhr.send(url, function(res) {
                var code = '<div style="color:#888;">'+res + '</div>';
                ffDevTools.console_display.innerHTML += code;
                ffDevTools.console_display.scrollTop = ffDevTools.console_display.scrollHeight;
                ffDevTools.loading_indicator.style.visibility = 'hidden';
            }, function(){
                ffDevTools.loading_indicator.style.visibility = 'hidden';
            });
            ffDevTools.console_display.scrollTop = ffDevTools.console_display.scrollHeight;
        }
    },

    showContainer: function() {
        ffDevTools.dev_tools_bar.style.display = 'block';
        ffDevTools.dev_tools_wrapper.style.display = 'none';
        ffDevTools.console_input.focus();
        setCookie('ffdev_container_state', 'open');
    },

    closeContainer :function() {
        ffDevTools.dev_tools_bar.style.display = 'none';
        ffDevTools.dev_tools_wrapper.style.display = 'block';
        cleanCookie('ffdev_container_state');
        cleanCookie('ffdev_container_tab');
    },

    activateTab: function(evt) {
        var ids = ['cp', 'db', 'log', 'console'];
        setCookie('ffdev_container_tab', evt.target.id);
        var id = evt.target.id.split('-').pop();
        for(var i=0,l=ids.length; i<l; i++) {
            document.getElementById('container-'+ids[i]).style.display = 'none';
            document.getElementById('switcher-'+ids[i]).style.color = '#666';
        }
        document.getElementById('container-'+id).style.display = 'block';
        document.getElementById('switcher-'+id).style.color = '#999';
    },


    getKeyCode: function(e) {
        if( !e ) {
            if( window.event ) {
                e = window.event;
            } else {
                return null;
            }
        }
        if( typeof( e.keyCode ) == 'number'  ) {
            e = e.keyCode;
        } else if( typeof( e.which ) == 'number' ) {
            e = e.which;
        } else if( typeof( e.charCode ) == 'number'  ) {
            e = e.charCode;
        } else {
            return null;
        }
        return e;
    },

    parseCL: function(value){
        var index = 0;
        var matches = [''];
        var need_space = false;
        var need_quote = false;
        for (var i=0; i < value.length; i++){
            var char = value.charAt(i);
            if (char != ' '){
                if (char == '"' && value.charAt(i-1) != '\\') {
                    if (need_quote) {
                        need_space = true;
                        need_quote = false;
                    } else {
                        need_space = false;
                        need_quote = true;
                    }
                } else {
                    if (!need_quote) {
                        need_space = true;
                    }
                    if (char=='\\' && value.charAt(i+1) == '"'){
                        char = '';
                    }
                    matches[index] += char;
                }
            } else {
                if (need_space) {
                    index += 1;
                    matches[index] = '';
                    need_space = false
                } else {
                    matches[index] += ' ';
                }
            }
        }
        var url = '/console/'+matches[0].replace(':','/');
        if (matches.length > 1) {
            url += '/?';
        }
        var index = 0;
        for(var i=1; i < matches.length; i++) {
            if (matches[i].charAt(0) == '-' && matches[i].charAt(1) == '-'){
                if (matches[i].indexOf('=') > 0) {
                    var tmp = matches[i].substr(2).split('=');
                    url += 'console_keys['+tmp[0]+']='+encodeURIComponent(tmp[1])+'&';
                } else {
                    url += 'console_keys['+matches[i].substr(2)+']=true&';
                }
            } else if (matches[i].charAt(0) == '-' && matches[i].indexOf('=') > 0){
                url += matches[i].substr(1)+'&';
            } else {
                url += 'params['+index+']='+encodeURIComponent(matches[i])+'&';
                index++;
            }
        }
        return url;
    }

}

ffDevTools.xhr = {

    XMLHttpFactories: [
        function () {return new XMLHttpRequest()},
        function () {return new ActiveXObject("Msxml2.XMLHTTP")},
        function () {return new ActiveXObject("Msxml3.XMLHTTP")},
        function () {return new ActiveXObject("Microsoft.XMLHTTP")}
    ],

    createXMLHTTPObject: function() {
        var xmlhttp = false;
        for (var i=0;i<ffDevTools.xhr.XMLHttpFactories.length;i++) {
            try {
                xmlhttp = ffDevTools.xhr.XMLHttpFactories[i]();
            }
            catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    },

    send: function(url, callback, fail_callback, data, method) {
        var req = ffDevTools.xhr.createXMLHTTPObject();
        if (!req) return;
        if (!method) method = 'POST';

        req.open(method, url, true);
        req.setRequestHeader('User-Agent','XMLHTTP/1.0');
        req.setRequestHeader('X_REQUESTED_WITH', 'XMLHttpRequest');
        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        if (data)
            req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
            req.onreadystatechange = function () {
                if (req.readyState != 4) return;
                if (req.status != 200 && req.status != 304) {
                    fail_callback();
                    return;
                }
                callback(req.responseText);
            }
        if (req.readyState == 4) return;
        req.send(data);
    }

}

ffDevTools.console_history = {

    history: [],
    index: -1,

    add: function(value){
        if (this.history[this.history.length -1 ] == value || value == '') return false;
        this.history.push(value);
        this.index = this.history.length-1;
    },

    getLast: function(){
        if (this.history.length == 0){
            return '';
        } else {
            return this.history[this.history.length-1];
        }
    },

    isLast: function(){
        if (this.history.length == 0){
            return false;
        } else {
            return this.index == this.history.length-1;
        }
    },

    getPrev: function(){
        if (this.history.length == 0){
            return '';
        } else {
            if (this.index > 0){
                this.index--;
            }
            var value = this.history[this.index];
            return value;
        }
    },

    getNext: function(){
        if (this.history.length == 0){
            return '';
        } else {
            if (this.index < this.history.length - 1){
                this.index++;
            }
            var value = this.history[this.index];
            return value;
        }
    },

    dump: function() {
        var dump_length = 5;
        var from = (dump_length > this.history.length) ? 0 : this.history.length - dump_length;
        var dump = [];
        for (var i = from; i < this.history.length; i++) {
            dump.push(this.history[i]);
        }
        return 'a:{"'+dump.join('"}{"')+'"}';
    },

    load: function(string) {
        if (string === null || string.indexOf('a:{"') != 0 || string.lastIndexOf('"}') != string.length - 2) return false;
        if ((string = string.substr(4, string.length - 6)) != '') {
            this.history = string.split('"}{"');
            this.index = this.history.length-1;
        }
    }
};

Array.prototype.inArray = function(val) {
    for(var i in this) {
        if (this[i] == val) return true;
    }
    return false;
}

function setCookie(name, value) {
    var expires = new Date();
    expires = expires.setTime(expires.getTime() + (1000 * 60 * 60 * 24 * 30));
//    console.log(expires);
    document.cookie = name + "=" + escape(value) + "; path=/" + ((expires == null) ? "" : "; expires=" + expires);
}

function getCookie(name) {
    var dc = document.cookie;
    var cname = name + "=";

    if (dc.length > 0) {
      begin = dc.indexOf(cname);
      if (begin != -1) {
        begin += cname.length;
        end = dc.indexOf(";", begin);
        if (end == -1) end = dc.length;
        return unescape(dc.substring(begin, end));
        }
      }
    return null;
}
function cleanCookie(name) {
    document.cookie = name + "=; expires=Thu, 01-Jan-70 00:00:01 GMT" + "; path=/";
}