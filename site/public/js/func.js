$(document).ready(function () {
    $('a[hrefpop="true"]').addClass('highslide');
    $('a[hrefpop="true"]').click(function () {
        return hs.expand(this);
    });
    /*---------------------------выпадающее меню ---*/
    $('.navi li').hover(function () {
        clearTimeout($.data(this, 'timer'));
        $('ul', this).stop(true, true).slideDown(400);
    }, function () {
        $.data(this, 'timer', setTimeout($.proxy(function () {
            $('ul', this).stop(true, true).slideUp(200);
        }, this), 100));
    });

    $('a.l-table_price_hidden_link').click(function () {

        if ($(this).text() == 'Скрыть') {
            $('tr.l-table_product_' + $(this).data('hide-id')).hide()
            $(this).text('Показать')
        }
        else {
            $('tr.l-table_product_' + $(this).data('hide-id')).show()
            $(this).text('Скрыть')
        }
        return false
    });
    $('select.cart_add').live('change keyup', function (e) {
        var oldValue = this.oldValue == undefined ? 0 : this.oldValue
        var value = parseInt($(this).val());

        if (value == -1) {
            $(this).val('0');
            return;
        }

        if (value <= 0 || !/^\d+$/.test(value)) {
            value = 0;
            $(this).val('');
        }

        if (!$.isNumeric(value))
            value = 0;

        if (oldValue == value && value != 0)
            return false;

        this.oldValue = value;

        $('.cart_sum').addClass('ajax').text('');
        var productId = parseInt($(this).data('id'));
        var type = $('#prices').attr('type');
        var brand = location.href.split('/')[location.href.split('/').length - 2];
        var t = this.parentNode.parentNode.classList[0].split('_');
        var catr_type = t[t.length - 1];
        var model = '';
        if (this.parentNode.previousElementSibling.childNodes.length > 0)
            model = this.parentNode.previousElementSibling.childNodes[0].data;
        $.post('addToCart/', {
            type: 'ajaxCart',
            action: 'addToCard',
            data: {productId: productId, type: type, count: value, catr_type: catr_type, model: model, brand: brand}
        }, function (data) {
            $('.cart_num').text(data.num);
            $('.cart_sum').removeClass('ajax').text(data.sum)
        }, 'json')

    });

    //$('input.cart_delete').live('click', function () {
    //    var productId = parseInt($(this).data('id'));
    //    var type = $('#prices').attr('type');
    //    var item = this;
    //    $(this).attr('disabled', 'disabled');
    //    $('.cart_sum').addClass('ajax').text('');
    //
    //    $.post('DeleteItemCart/', {
    //        type: 'ajaxCart',
    //        action: 'delete',
    //        data: {productId: productId, type: type}
    //    }, function (data) {
    //        if (data.num == 0) {
    //            $('div.l-main_order').remove()
    //            window.document.location = "/";
    //        } else {
    //            var $tr = $('tr.l-table_product_' + $(item).data('top'));
    //
    //            if ($tr.size() == 1) {
    //                $('#subname-' + $(item).data('top')).remove()
    //            }
    //
    //            $(item).closest('tr').remove()
    //        }
    //
    //        $('.cart_num').text(data.num);
    //        if ($('.currency_flag.on').attr('id') === 'currency_USD') {
    //            $('.cart_sum').removeClass('ajax').text(data.sum / parseFloat($('#currency_rate').html()));
    //        }
    //        else {
    //            $('.cart_sum').removeClass('ajax').text(data.sum);
    //        }
    //
    //    }, 'json')
    //
    //})

    $('input.cart_clear').click(function () {
        if (!confirm('Удалить все позиции из корзины?'))
            return false;

        $.post('ClearCart/', {
            type: 'ajaxCart',
            action: 'ClearCart'
        }, function (data) {
            $('div.l-main_order').remove()
            window.document.location = "/";
        }, 'json')
    });
    /*------------------order_sell ---------------------*/
    jQuery('.order_services input[type="checkbox"]').each(function (i) {
        jQuery(this).change(function () {
                var richtext = jQuery(this).parent().find('textarea');
                var checked = jQuery(this).is(':checked');
                if (checked) {
                    richtext.attr('disabled', false);
                }
                else {
                    richtext.attr('disabled', true);
                }
            }
        );
    });
});

function changeCount(value, productId, type) {
    $.post('addToCart/', {
        type: 'ajaxCart',
        action: 'addToCard',
        data: {productId: productId, type: type, count: value}
    }, function (data) {
        //$('.cart_num').text(data.num);
        if ($('#current_currency').val() == 'USD') {
            var currency_rate = parseFloat($('#currency_rate').text().replace(',', '.'));
            var res = parseFloat(data.sum) / currency_rate;
            $('#all_price_sum').text(res.toFixed(2));
        } else {
            $('#all_price_sum').text(data.sum.toFixed(2))
        }
        var old_price_sum = parseFloat($('#price_' + productId + '-' + type).text());
        var new_price_sum = old_price_sum * parseFloat(value);
        if ((new_price_sum + "").indexOf(".") > 0) {
            $('#sum_' + productId + '-' + type).text(new_price_sum.toFixed(2));
        } else {
            $('#sum_' + productId + '-' + type).text(new_price_sum);
        }
        if ($('#tr_' + productId + ' select').val() === '0') {
            if($('.zakaz tr').length<4){
                window.location.href ='/';
            }
            $('#tr_' + productId).remove();

        }

    }, 'json')
}