DELETE FROM news;
INSERT INTO news (id,title,content,_slug,is_active,meta_title,meta_description,meta_keywords,created_at,updated_at)
	SELECT id,title,content,_slug,is_active,_meta_title,_meta_description,_meta_keywords,_created_at,_updated_at
	FROM nt_site_content.news;