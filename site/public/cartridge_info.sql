DELETE FROM brands;
INSERT INTO brands (id,title,_slug) SELECT id,title,_slug FROM nt_content.brands;

DELETE FROM cartridge_types;
INSERT INTO cartridge_types (id,title) SELECT id,title FROM nt_content.cartridge_types;

DELETE FROM cartridge_prices;
INSERT INTO cartridge_prices (id,id_brand,id_type,cartridge_model,articule,price_RUB,price_USD,active)
		SELECT id,id_brand,id_type,printer_ru,articule,price*50,price,1 FROM nt_content.cartridge_prices;
