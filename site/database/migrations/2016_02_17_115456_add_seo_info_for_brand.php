<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoInfoForBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function($table)
        {
            $table->string('seo_text');
            $table->string('top_text_for_brands');
            $table->string('bottom_text_for_brands');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function($table)
        {
            $table->dropColumn('seo_text');
            $table->dropColumn('top_text_for_brands');
            $table->dropColumn('bottom_text_for_brands');

        });
    }
}
