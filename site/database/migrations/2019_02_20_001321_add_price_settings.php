<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        if (!Schema::hasColumn('settings','client_price')) {
            Schema::table('settings', function ($table) {
                $table->text('client_price');
            });
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        if (Schema::hasColumn('settings','client_price')) {
            Schema::table('settings', function ($table) {
                $table->dropColumn(['client_price']);
            });
//        }
    }
}
