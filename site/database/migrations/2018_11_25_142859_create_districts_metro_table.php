<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsMetroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('title_where');
            $table->string('slug');
            $table->string('coords')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->longText('seo');
        });

        Schema::create('metro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('title_district');
            $table->string('title_district_where');
            $table->string('slug');
            $table->string('coords')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->longText('seo');
        });

        if (!Schema::hasColumn('cartridge_prices','slug')) {
            Schema::table('cartridge_prices', function ($table) {
                $table->string('slug');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('districts');
        Schema::drop('metro');

        if (Schema::hasColumn('cartridge_prices','slug')) {
            Schema::table('cartridge_prices', function ($table) {
                $table->dropColumn(['slug']);
            });
        }
    }
}
