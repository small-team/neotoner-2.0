<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSumUsdToCartridgeOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cartridge_orders','currency')) {
            Schema::table('cartridge_orders', function ($table) {
                $table->string('currency');
                $table->integer('price_usd');
                $table->integer('sum_usd');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cartridge_orders','currency')) {
            Schema::table('cartridge_orders', function (Blueprint $table) {
                $table->dropColumn(['currency']);
                $table->dropColumn(['price_usd']);
                $table->dropColumn(['sum_usd']);
            });
        }
    }
}
