<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceUsdToOrdersInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('orders_info','price_usd')) {
            Schema::table('orders_info', function ($table) {
                $table->integer('price_usd');
                $table->integer('sum_usd');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('orders_info','price_usd')) {
            Schema::table('orders_info', function ($table) {
                $table->dropColumn(['price_usd']);
                $table->dropColumn(['sum_usd']);
            });
        }
    }
}
