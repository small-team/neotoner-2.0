<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE cartridge_orders MODIFY COLUMN `email` varchar(255) NULL;');
        DB::statement('ALTER TABLE cartridge_orders MODIFY COLUMN `city` varchar(255) NULL;');
        DB::statement('ALTER TABLE cartridge_orders MODIFY COLUMN `comment` varchar(255) NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE cartridge_orders MODIFY COLUMN `email` varchar(255) NOT NULL;');
        DB::statement('ALTER TABLE cartridge_orders MODIFY COLUMN `city` varchar(255) NOT NULL;');
        DB::statement('ALTER TABLE cartridge_orders MODIFY COLUMN `comment` varchar(255) NOT NULL;');
    }
}
