<p>Номера заказа : @if(isset($code)){{$code}}@endif</p>
<p>Дата и время обращения : @if(isset($date)){{$date}}@endif</p>
<p>Имя клиента : @if(isset($name)){{$name}}@endif</p>
<p>Контактный телефон : @if(isset($phone)){{$phone}}@endif</p>
<p>Город : @if(isset($city)){{$city}}@endif</p>
<p>E-mail : @if(isset($email)){{$email}}@endif</p>
<p>Что вы хотите нам предложить : @if(isset($comment)){{$comment}}@endif</p>
<p>Адрес сайта : @if(isset($site_url)){{$site_url}}@endif</p>
<p>Тип заявки : @if(isset($type)){{$type}}@endif</p>
@if(!empty($order_info))
<table style="border: 1px solid black;border-collapse: collapse ">
    <thead>
        <tr style="border: 1px solid black">
            <th style="border: 1px solid black;font-weight: bold">Бренд</th>
            <th style="border: 1px solid black;font-weight: bold">Артикул</th>
            <th style="border: 1px solid black;font-weight: bold">Модель устройства</th>
            <th style="border: 1px solid black;font-weight: bold">Цена</th>
            <th style="border: 1px solid black;font-weight: bold">Кол-во</th>
            <th style="border: 1px solid black;font-weight: bold">Сумма</th>
            <td class="">Желаемая</td>
            <td class="">Сумма</td>
            <td class="">%</td>
        </tr>
    </thead>
    <tbody>
        @foreach($order_info as $key=>$product)
            <?php $red_price = $product['price_client'] && ($product['price_client'] != $product['price']); ?>
            <tr style="border: 1px solid black">
                <td style="border: 1px solid black">{{$product['brand']}}</td>
                <td style="border: 1px solid black">{{$product['articule']}}</td>
                <td style="border: 1px solid black">{{$product['model']}}</td>
                <td style="border: 1px solid black">{{$product['price']}}</td>
                <td style="border: 1px solid black">{{$product['quantity']}}</td>
                <td style="border: 1px solid black">{{$product['sum']}}</td>
                <td class="" style="{{$red_price ? 'color: red' : ''}}">{{$product['price_client']}}</td>
                <td class="" style="{{$red_price ? 'color: red' : ''}}">{{$product['sum_client']}}</td>
                <td class="">{{ $product['price_client'] ? round(100*$product['price_client']/$product['price']-100) : '-' }}</td>
            </tr>
        @endforeach
        <?php $red_sum = $sum_client && ($sum_client != $sum); ?>
            <tr style="border: 1px solid black">
                <td style="border: 1px solid black">Итог :</td>
                <td style="border: 1px solid black"></td>
                <td style="border: 1px solid black"></td>
                <td style="border: 1px solid black"></td>
                <td style="border: 1px solid black">{{$count_items}}</td>
                <td style="border: 1px solid black">{{$sum}}</td>
                <td class=""></td>
                <td class="_rub"><b style="{{$red_sum ? 'color: red' : ''}}">{{$sum_client}}</b></td>
                <td class="_rub"><b style="{{$red_sum ? 'color: red' : ''}}">{{ $sum_client ? round(100*$sum_client/$sum-100) : '-' }}</b></td>
            </tr>
    </tbody>

</table>
@else
<table>
    <thead>
        <tr>Товаров нет</tr>
    </thead>
</table>
@endif
<br>
@if(!empty($photo_links))
<table >
    <thead>
    <tr>
            <td>Ссылки на прикрепленные фото </td>
    </tr>
    </thead>
    @if(isset($photo_links) && $photo_links)
        @foreach($photo_links as $key=>$photo)
                    <tr>
                        <td>{{$photo['link']}}</td>
                    </tr>
        @endforeach
    @endif
</table>
@else
<table>
    <thead>
        <tr>Нет прикреплённых фото</tr>
    </thead>
</table>
@endif
