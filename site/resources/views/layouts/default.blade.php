<!DOCTYPE html>
<html class="no-js" lang="ru">
<head>
	{{--<title>Главная</title>--}}
	{{\App\Models\MetaTag::printMetaData()}}
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

	<meta name="viewport" content="width=device-width" />
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    @if(isset($routeName))
        <link rel="canonical" href="{{URL::route($routeName, $routeParams ?? [])}}"/>
    @else
        <link rel="canonical" href="{{Request::url()}}"/>
    @endif

    <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="/css/general.css">
    <link async="async" rel="stylesheet" type="text/css" href="/js/libs/highslide/highslide.css" />
    <link rel="stylesheet" href="/css/jquery.autocompleter/jquery.autocompleter.css">
    <link rel="stylesheet" href="/css/style.css?6">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="/js/jquery.autocompleter/jquery.autocompleter.js?1"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
{{--POPUP--}}
			{{----}}
<img src="{{asset('img/load.gif')}}" class="load_gif">
			<div class="popup_wrap noprint"></div>
			<div class="content-body-centerside_popup popup_window noprint" style="display: none">
                <div class="content-body-form">
                <img class="icon_delete_popup"   src="/img/exit_popup.ico">
                    <div id="title_call" class="content-body-form__title">Обратный звонок</div>
                    <div id="title_call_true" class="content-body-form__title">Ваша заявка на обратный звонок отправлена</div>
                    <form action="#" id="btn_call_back">
                        <div class="content-body-form-row row">

                            <div class="content-body-form-column_popup">
                                <input data-value="Имя *" placeholder="Имя *" type="text" class="content-body-form__input" name="name"  id="name_call"/>
                                <input data-value="Телефон *" placeholder="Телефон *" type="text" class="content-body-form__input number_phone phone_mask" name="phone" id="phone_call"/>
                                <input type="hidden" value="call_back"  id="type_call" name="type"/>
                            </div>
                        </div>
                        <div class="content-body-form__info">
                            <span>*</span> Поля обязательные для заполнения
                        </div>
                        <button class="content-body-form__btn btn" type="submit" @if(!env('TEST_SITE', false)) onclick="yaCounter28909060.reachGoal('CALLBACKSEND');" @endif>Отправить заявку</button>
                    </form>
                </div>
            </div>
			{{----}}
            <div class="content-body-centerside_popup popup_confirm noprint" style="display: none">
                <div class="content-body-form">
                <img class="icon_delete_popup"   src="/img/exit_popup.ico">
                    <span class="confirm_title">Подтвердите!</span>
                    <table width="400px" style="margin-top: 10px;">
                        <tr >
                            <td><b>Картриджи новые и оригинальные?</b></td>
                            <td><span class="button button_confirm" onclick="add_order(true);" >Да</span></td>
                        </tr>
                        <tr>
                            <td><b>Бывшие в употреблении, пустые?</b></td>
                            <td><span class="button button_inconfirm" onclick="$(location).attr('href','/pages/rules');">Да</span></td>
                        </tr>
                    </table>
                </div>
            </div>
			{{----}}
            <div class="content-body-centerside_popup popup_price_error noprint" style="display: none">
                <div class="content-body-form">
                    <div id="title_call" class="content-body-form__title">Слишком высокая цена</div>
                    <div class="content-body-body">
                        <p>
                            Введите цену ниже
                        </p>
                        <button class="content-body-form__btn btn" type="button" onclick="$('.popup_wrap').css('display', 'none');$('.popup_price_error').css('display', 'none');">ОК</button>
                    </div>
                </div>
            </div>
			{{----}}
            <div class="content-body-centerside_popup popup_client_price noprint" style="display: none">
                <div class="content-body-form">
                    <div id="title_call" class="content-body-form__title">Рассмотрим цены выгодные для Вас</div>
                    <div class="content-body-body">
                        <ul>
                            <li>Укажите нужную стоимость в окне <span class="green">Желаемая</span></li>
                            <li>Введите <span class="green">Кол-во</span> и перейдите к другим позициям</li>
                            <li>В итоге нажмите <span class="green">Продать</span> или войдите в Корзину</li>
                            <li>Мы рассмотрим и ответим на Ваше предложение!</li>
                        </ul>
                        <button class="content-body-form__btn btn" type="button" onclick="$('.popup_wrap').css('display', 'none');$('.popup_client_price').css('display', 'none');">ОК</button>
                    </div>
                </div>
            </div>
			{{----}}

			{{--POPUP--}}
    <div class="l-body">
        <header class="l-header">
            @include('components.header')
        </header>
        @include('components.menu')
        @yield('template_content')
    </div>
        @include('components.footer')
    @if(!empty($info_company->metrika))
            {!!$info_company->metrika!!}
        @endif
        @if(isset($_SERVER['REMOTE_ADDR']))
            <script type="text/javascript">
                var yaParams = {ip_adress: "<?php echo $_SERVER['REMOTE_ADDR'];?>"};
            </script>
        @endif

        <script type="text/javascript">
            $(function () {
                $('#search_key').autocompleter(
                    {
                        source: '/autocomplete',
                        minLength: 2,
                        limit: 30,
                        cache: false,
                        // template: '<div><div class="left">\{\{ label \}\}</div><div class="clr"></div></div>',
                        callback: function(value,index) {
                            $('#search_key').parent('form').submit();
                        }
                    }
                );
            });
        </script>
</body>
</html>
