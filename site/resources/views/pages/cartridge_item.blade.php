@extends('layouts.default')
@section('template_content')

<div class="g-clrfix">
    <section class="l-main">
        <div class="l-main_inner">

{{--            <div id="nav"><a href="/">Главная</a> » <a>Покупка картриджей</a> » <a>{{$cartridge_brand->title}}</a></div>--}}

            @if(isset($cartridge_brand->seo_text) && strlen(trim($cartridge_brand->seo_text)))
            <div class="h1">
                {!!$cartridge_brand->seo_text!!}
            </div>
            @endif

            @if(isset($cartridge_brand->top_text_for_brands) && strlen(trim($cartridge_brand->top_text_for_brands)))
                <article>
                    {!!$cartridge_brand->top_text_for_brands!!}
                </article>
            @endif

            <h1>Скупка картриджей {{$cartridge_brand->title}}</h1>

                <div class="l_main_order_block">
{{--                    <div class="order-info">--}}
{{--                        <a id="brand_price" target="_blank" href="{{route('exel_download',['slug'=>$cartridge_brand->_slug])}}">Скачать прайс-лист {{$cartridge_brand->title}}--}}
{{--                            <img width="16px" height="16px"--}}
{{--                                 src="/img/prlist.png"/></a>--}}
{{--                    </div>--}}
                    <td width="30%" style="vertical-align:middle" rowspan="2">
                        <div class="block_kurs noprint" >
                            <strong style="float: right;margin-bottom: 2px;">1 у.е. =  <span class="green " id="currency_rate">{{$info_satellite->rate}} руб.</span></strong>
                            <div class="">
                                <strong>Валюта:&nbsp;</strong>

                                <a id="currency_USD" class="currency_flag @if(\Session::get('currency') == 'USD') on @endif"  data-filter="{{route('change_cur',['currency'=>'USD'])}}" onclick="selectCurrency('USD');">у.е.</a>
                                <a id="currency_RUB" class="currency_flag @if(\Session::get('currency') == 'RUB') on @endif" data-filter="{{route('change_cur',['currency'=>'RUB'])}}" onclick="selectCurrency('RUB');" >руб.</a>
                                <input type="hidden" value="RUB" id="current_currency"/>
                            </div>
                        </div>
                    </td>


                </div>

                @include('components/right_price_block', ['count_items' => $count_items, 'sum_items' => $sum_items_client])

                <table class="l-table_price" type="cartridge" id="prices" >
                    <tbody>
                    <tr class="head">
                        <th class="th_articul">Артикул</th>
                        <th class="th_model">Модель устройства</th>
                        <th class="th_price_RUB price_RUB">Наша цена</th>
                        <th class="th_price_want">Желаемая</th>
{{--                        @if(\Session::get('currency') == 'RUB') <th class=" price_RUB">Цена руб</th>  <th class="price_USD" style="display: none">Цена y.e.</th>@endif--}}
{{--                        @if(\Session::get('currency') == 'USD') <th class="price_USD" >Цена y.e.</th> <th class=" price_RUB" style="display: none">Цена руб</th> @endif--}}
                        <th class="th_count">Кол-во</th>
                    </tr>
                    @if(!empty($cartridge_price_item))
                        @foreach($cartridge_price_item as $item)
                            <tr class="item_row">
                                <td class="td-left-pl15 td_articul">{{$item->articule}}</td>
                                <td class="td-left-pl15 td_model">{{$item->cartridge_model}}</td>
                                {{--<td class=" price_RUB">{{$item->price_RUB}}</td>--}}
                                {{--@if(\Session::get('currency') == 'RUB')--}}
                                    <td class="td_price_RUB price_RUB price_RUB-{{$item->id}}">{{$item->price_RUB}}</td>
                                    <td class="price_USD price_USD-{{$item->id}}" style="display: none">{{$item->price_USD}}</td>
                                {{--@endif--}}
                                {{--@if(\Session::get('currency') == 'USD')--}}
                                    {{--<td class="price_USD price_USD-{{$item->id}}" >{{$item->price_USD}}</td>--}}
                                    {{--<td class=" price_RUB price_RUB-{{$item->id}}" style="display: none">{{$item->price_RUB}}</td>--}}
                                {{--@endif--}}
                                <td class="td_price_want price__inp_td">
                                    @if(isset($cart_items[$item->id]))
                                        <input type="text" name="price[]" value="{{$cart_items[$item->id]["sum_Client"]}}" id="price-{{$item->id}}" autocomplete="off" class="number price__inp item_{{$item->id}}" />
                                    @else
                                        <input type="text" name="price[]" value="{{$item->price_RUB}}" id="price-{{$item->id}}" autocomplete="off" class="number price__inp item_{{$item->id}}" />
                                    @endif
                                </td>
                                <td class="td_count quantity__inp_td">
                                    @if(isset($cart_items[$item->id]))
                                        <input type="text" name="form[]" value="{{$cart_items[$item->id]["count"]}}" autocomplete="off" id="{{$item->id}}" class="number quantity__inp item_{{$item->id}}" />
                                    @else
                                        <input type="text" name="form[]" value="0" id="{{$item->id}}" autocomplete="off" class="number quantity__inp item_{{$item->id}}" />
                                    @endif
                                </td>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            @if(isset($cartridge_brand->bottom_text_for_brands))
                <div class="bottom_text_for_brands">{!!$cartridge_brand->bottom_text_for_brands!!}</div>
            @endif

            <div>
            </div>
        </div>
    </section>
</div>
<div class="l-empty"></div>
@stop