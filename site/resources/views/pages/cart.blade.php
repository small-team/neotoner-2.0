@extends('layouts.default')
@section('template_content')
<div class="g-clrfix">
    <section class="l-main">
        <div class="l-main_inner">
            <article>
                @if(!isset($cart_orders_item))
                    <div class="content__title send_successfully">
                        @if(isset($send) && $send)
                            Ваша заявка @if(isset($code)) № {{$code}}  от {{$date}} @endifуспешно отправлена.<br><br>Вам выслано СМС с номером заявки. После обработки заявки с Вами свяжется менеджер.
                        @else
                            Корзина пуста
                        @endif
                    </div>
                @endif
            </article>
            @if(isset($cart_orders_item))
                <h1>Заполните форму и отправьте заявку</h1>
            <div class="l-main_order">
                <div class="cart">
                    <div id="error_block"></div>
{{--                    <span class="form_error" id="error_phone" style="display:none;">Поле Контактный телефон обязательно к заполнению</span>--}}
{{--                    <span class="form_error" id="error_email" style="display:none;">Поле E-mail обязательно к заполнению</span>--}}
{{--                    <span class="form_error" id="error_models" style="display:none;">Необходимо выбрать картриджи для продажи</span>--}}
                  {!! Form::open(
                                      array(
                                          'route' => 'save_order',
                                          'method'=>'POST',
                                          'novalidate' => 'novalidate',
                                          'files' => true,
                                          'id' => 'cart-form')) !!}
                                                  {!! csrf_field() !!}
                                                    {{ HiddenCaptcha::render() }}
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="zayavka">
                        <tbody>
                            <tr>
                                <td width="32%" class="col2">
                                    <input type="text" class="inp2" name="name" placeholder="Имя"  id="fio" value="{{Input::old('name')}}">
                                </td>
                            </tr>
                            <tr>
                                <td class="col2">
                                    <input type="text" class="inp2 phone_mask" name="phone" placeholder="+7 (___) ___-__-__" id="phone" value="{{Input::old('phone')}}"
                                </td>
                            </tr>
                            <tr>
                                <td ><input type="text" class="inp2" placeholder="{{$info_company->company_email}}"   value="{{Input::old('email')}}" name="email" id="email">
                                </td>
                            </tr>
                            <tr>
                                <td class="col2">
                                    <div class="upload_block">
                                        Прикрепить файл
                                    </div>
                                    <input class="content-body-form-file__file" type="file" name="images[]" multiple  />
                                </td>
                            </tr>

                        </tbody>
                    </table>

                    <input type="hidden" value="cart_request"  name="type"/>

                    {!! Form::close() !!}
                </div>
            </div>
            <br>
                <div class="block_kurs noprint">
                    <strong style="float: right;margin-bottom: 2px;">1 у.е. =  <span class="green " id="currency_rate">{{$info_satellite->rate}} руб.</span></strong>
                    <div class="">
                        <strong>Валюта:&nbsp;</strong>
                        <a id="currency_USD" class="currency_flag @if(\Session::get('currency') == 'USD') on @endif"   onclick="selectCurrency('USD');">у.е.</a>
                        <a id="currency_RUB" class="currency_flag @if(\Session::get('currency') == 'RUB') on @endif" onclick="selectCurrency('RUB');" >руб.</a>
                        <input type="hidden" value="RUB" id="current_currency"/>
                    </div>
                </div>

                <div style="text-align: right; margin-bottom: 20px">
                    <a id="brand_price" target="_blank" href="{{route('open_excel')}}" @if(!env('TEST_SITE', false)) onclick="yaCounter28909060.reachGoal('KORZINAFORMPRINT');" @endif>Перечень позиций в Excel
                        <img width="16px" height="16px"
                             src="/img/prlist.png"/></a>
                </div>

                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="zakaz_new">
                    <tbody>
                    <tr>
                        <th class="cart_th_brand">
                            Бренд
                        </th>
                        <th class="cart_th_articul">
                            Артикул
                        </th>
                        <th class="cart_th_model">
                            Модель устройства
                        </th>
{{--                        @if(\Session::get('currency') == 'RUB')--}}
                            <th width="7%" class="price_RUB_th">
                                Цена
                            </th>
                            {{--<th width="7%" class="price_USD_th" style="display: none">--}}
                                {{--Цена y.e.--}}
                            {{--</th>--}}
                        {{--@endif--}}
                        {{--@if(\Session::get('currency') == 'USD')--}}
                            {{--<th width="7%" class="price_RUB_th" style="display: none">--}}
                                {{--Цена руб.--}}
                            {{--</th>--}}
                            {{--<th width="7%" class="price_USD_th">--}}
                                {{--Цена y.e.--}}
                            {{--</th>--}}
                        {{--@endif--}}


                        <th width="6%">
                            Кол-во
                        </th>
{{--                        @if(\Session::get('currency') == 'RUB')--}}
                            <th width="7%" class="price_RUB_th">
                                Сумма
                            </th>
                            {{--<th width="7%" class="price_USD_th"  style="display: none">--}}
                                {{--Сумма y.e.--}}
                            {{--</th>--}}
                        {{--@endif--}}
                        {{--@if(\Session::get('currency') == 'USD')--}}
                            {{--<th width="7%" class="price_RUB_th" style="display: none">--}}
                                {{--Сумма руб.--}}
                            {{--</th>--}}
                            {{--<th width="7%" class="price_USD_th">--}}
                                {{--Сумма y.e.--}}
                            {{--</th>--}}
                        {{--@endif--}}

                        <th class="noprint" width="8%" style="padding-left: 15px;">
                            Удалить
                        </th>
                    </tr>
                        @foreach($cart_orders_item as $item)
                            <tr id="tr-{{$item->id}}" class="item_row">
                                <td class="nom">
                                    {{$item->brand}}
                                </td>
                                <td class="nom articule">
                                    <a class="duphandler" href="{{URL::route('cart_item_duplicate', ['id' => $item->id])}}" id="{{$item->id}}"></a>
                                    {{$item->articule}}
                                </td>
                                <td class="nom model">
                                    @if (strpos($item->id, 'duplicate-') === 0)
                                        <input type="text" name="desc[]" value="{{$cart_items[$item->id]["desc"]}}" id="desc-{{$item->id}}" autocomplete="off" class="item_desc_input desc_{{$item->id}}" />
                                    @else
                                        {{$item->cartridge_model}}
                                    @endif
                                </td>
                                <td class="price__inp_td">
{{--                                    @if(\Session::get('currency') == 'RUB')--}}
                                        <span class="price_RUB price_RUB-{{$item->id}}" style="display: none">{{$item->price_RUB}}</span>
                                        <span class="price_USD price_USD-{{$item->id}}" style="display: none">{{$item->price_USD}}</span>
                                    {{--@endif--}}
                                    {{--@if(\Session::get('currency') == 'USD')--}}
                                            {{--<span class="price_RUB price_RUB-{{$item->id}}" style="display: none">{{$item->price_RUB}} </span><span style="display: none" class="price_RUB"></span>--}}
                                            {{--<span class="price_USD price_USD-{{$item->id}}" >{{$item->price_USD}}</span><span  class="price_USD"></span>--}}
                                    {{--@endif--}}
                                    @if(isset($cart_items[$item->id]))
                                        <input type="text" name="price[]" value="{{$cart_items[$item->id]["sum_Client"]}}" id="price-{{$item->id}}" autocomplete="off" class="number price__inp item_{{$item->id}}" />
                                    @else
                                        <input type="text" name="price[]" value="{{$item->price_RUB}}" id="price-{{$item->id}}" autocomplete="off" class="number price__inp item_{{$item->id}}" />
                                    @endif
                                </td>
                                <td class="quantity__inp_td">
                                    @if(isset($cart_items[$item->id]))
                                        <input type="text" name="form[]" value="{{$cart_items[$item->id]["count"]}}" id="{{$item->id}}" autocomplete="off" class="number quantity__inp item_{{$item->id}}" />
                                    @else
                                        <input type="text" name="form[]" value="0" id="{{$item->id}}" autocomplete="off" class="number quantity__inp item_{{$item->id}}" />
                                    @endif
                                </td>
                                <td>
{{--                                    @if(\Session::get('currency') == 'RUB')--}}
                                        <span class="price_RUB price" id="order_{{$item->id}}" >{{$cart_items[$item->id]["count"]*$cart_items[$item->id]["sum_Client"]}}</span>
{{--                                        <span class="price_USD" style="display: none" id="order_{{$item->id}}" >{{$cart_items[$item->id]["count"]*$item->price_USD}}</span>--}}
{{--                                    @endif--}}
{{--                                    @if(Session::get('currency') == 'USD')--}}
{{--                                        <span class="price_RUB" id="order_{{$item->id}}" style="display: none" >{{$cart_items[$item->id]["count"]*$item->price_RUB}}</span>--}}
{{--                                        <span class="price_USD" id="order_{{$item->id}}" >{{$cart_items[$item->id]["count"]*$item->price_USD}}</span>--}}
{{--                                    @endif--}}
                                </td>
                                <td>
                                    <a class="delete_key icon_delete red_button" href="{{URL::route('cart_item_delete', ['id' => $item->id])}}" id="{{$item->id}}"><i class="fas fa-times"></i></a>
                                </td>
                            </tr>
                    @endforeach
                    <tr class="last">
                        <td class="grey" >
                            Итог :
                        </td>
                        <td colspan="3"></td>
                        <td>
                                <span class="cart_num">
                                @if(isset($count_items)){{$count_items}}
                                    @else 0
                                    @endif
                                </span><span> шт.</span>
                        </td>
{{--                        @if(\Session::get('currency') == 'RUB')--}}
                        <td class="price_RUB">
                            <span id="all_price_sum" class="costs_value cart_sum cart_cart_sum">
                                 @if(isset($count_items)){{$sum_items_client}}
                                @else 0
                                @endif
                            </span><span></span>
                        </td>

{{--                            <td class="price_USD" style="display: none">--}}
{{--                            <span id="all_price_sum" class="costs_value cart_sum_USD">--}}
{{--                                 @if(isset($count_items)){{$sum_USD}}--}}
{{--                                @else 0--}}
{{--                                @endif--}}
{{--                            </span><span></span>--}}
{{--                            </td>--}}
{{--                        @endif--}}
{{--                        @if(\Session::get('currency') == 'USD')--}}
{{--                            <td class="price_RUB" style="display: none">--}}
{{--                            <span id="all_price_sum" class="costs_value cart_sum">--}}
{{--                                 @if(isset($count_items)){{$sum_items}}--}}
{{--                                @else 0--}}
{{--                                @endif--}}
{{--                            </span><span></span>--}}
{{--                            </td>--}}
{{--                            <td class="price_USD" >--}}
{{--                            <span id="all_price_sum" class="costs_value cart_sum_USD">--}}
{{--                                 @if(isset($count_items)){{$sum_USD}}--}}
{{--                                @else 0--}}
{{--                                @endif--}}
{{--                            </span><span></span>--}}
{{--                            </td>--}}
{{--                        @endif--}}
                        <td>
                            <input  class="content-body-form__btn2 btn_2 clin noprint red_button clear_cart_button" type="button" data-href="{{URL::route('clean_cart')}}" @if(!env('TEST_SITE', false)) onclick="yaCounter28909060.reachGoal('KORZINAFORMCLEAR');" @endif  value="Все"/>
                        </td>



                    </tr>

                    </tbody>
                </table>
            <br>

                <div class="buttons">
{{--                    <div class="col-lg-4">--}}
{{--                        <span class="">--}}
{{--                            <input  class="content-body-form__btn2 btn_2 clin noprint button_in_cart" type="button" data-href="{{URL::route('clean_cart')}}" @if(!env('TEST_SITE', false)) onclick="yaCounter28909060.reachGoal('KORZINAFORMCLEAR');" @endif  value="Очистить корзину"/>--}}
{{--                        </span>--}}
{{--                    </div>--}}
                    <div class="col-lg-4">
                        <span class="">
                            <input id="send_order" type="button" style="float:right;" class="send noprint button_in_cart" @if(!env('TEST_SITE', false)) onclick="yaCounter28909060.reachGoal('KORZINAFORMOTPR');" @endif value="Отправить заявку" />
                        </span>
                    </div>
                </div>
                <div>
                    <div class="l-main_back noprint"><a href="javascript:history.back();">Назад</a></div>
                    <div class="l-main_up noprint"><a href="javascript:window.scrollTo(0,0);">Наверх</a></div>
                </div>
        </div>
        @endif

    </section>
</div>
<div class="l-empty noprint"></div>
@stop