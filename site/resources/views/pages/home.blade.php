@extends('layouts.default')
@section('template_content')
<div class="g-clrfix">
    <section class="l-main l-main-is_blocks_right">
        <div class="l-main_inner">
        @if(!empty(Session::get('send')) && Session::get('send'))
            <h1>Ваша звявка № {{Session::get('code')}} от {{Session::get('date')}} успешно отправлена<br><br>Вам выслано СМС с номером заявки. После обработки заявки с Вами свяжется менеджер.</h1>
        @else
            <h1>Скупка картриджей - быстрая заявка</h1>
        @endif
            <div class="request " style="padding-bottom: 10px">
                <div class="request-inner main">
                    <div id="error_block"></div>
                    {!! Form::open(
                                            array(
                                                'route' => 'store_request',
                                                'method'=>'POST',
                                                'novalidate' => 'novalidate',
                                                'files' => true,
                                                'id'=>'request-form')) !!}
                                            {!! csrf_field() !!}
                                            {{ HiddenCaptcha::render() }}
                    <table cellspacing="0" cellpadding="0" border="0" class="zayavka">
                        <tbody>
                        <tr>
                            <td rowspan="5" class="order_services">
                                <textarea  class="area" name="mess" id="mess" placeholder="Ваше предложение">{{Input::old('mess')}}</textarea>
                            </td>
                            <td>
                                <input type="text" class="inp2" name="name" value="{{Input::old('name')}}" placeholder="Имя" id="fio">
                            </td>
                        </tr>
                        <tr>
                            <td ><input type="text" class="inp2 phone_mask" placeholder="+7 (___) ___-__-__"  value="{{Input::old('phone')}}" name="phone"
                                                    id="phone"> </td>
                        </tr>
                        <tr>
                            <td ><input type="text" class="inp2" placeholder="{{$info_company->company_email}}"   value="{{Input::old('email')}}" name="email" id="email">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="upload_block">
                                    Прикрепить файл
                                </div>
                                <input class="content-body-form-file__file" type="file" name="images[]" multiple  />
                            </td>
                        </tr>
                        <tr>
                            <td class="td_bottom_button"><input type="button" class="make_request" onclick="@if(!env('TEST_SITE', false))yaCounter28909060.reachGoal('GLAVFORMOTPR');@endif"  value="Отправить заявку"/></td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <input type="hidden" value="quick_request"  name="type"  />
                 {!! Form::close() !!}
            </div>
            @if(!empty($info_page['main_page_content']))
                {!!$info_page['main_page_content']!!}
            @endif
        </div>
    </section>
    @include('components.right_column')
</div>
<div class="l-empty"></div>
<div class="l-empty"></div>
<div class="l-empty"></div>
<div class="grid">
    <div class="cartridge_purchase">
        <ul class="grid">
            @foreach($brands as $brand)
                <li><a href="{{ route('price_brand', ['slug'=>$brand->_slug]) }}"><img src="{{ asset($brand->image['sizes']['list']['link']) }}" alt="{{ $brand->title }}"></a></li>
            @endforeach
        </ul>
    </div>
</div>
<div class="l-empty"></div>
@stop