@extends('layouts.default')
@section('template_content')
<div class="g-clrfix">
    <section class="l-main l-main-is_blocks_right">
        <div class="l-main_inner">

            <h1>{!!$news->title!!}</h1>
            <article>
                {!!$news->content!!}
            </article>
            <div>
                <div class="l-main_back"><a href="javascript:history.back();">Назад</a></div>
                <div class="l-main_up"><a href="javascript:window.scrollTo(0,0);">Наверх</a></div>
            </div>
        </div>
    </section>
         @include('components.right_column')
</div>
<div class="l-empty"></div>
@stop