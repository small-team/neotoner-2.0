@extends('layouts.default')
@section('template_content')
<div class="g-clrfix">
    <section class="l-main l-main-is_blocks_right">
        <div class="l-main_inner">
                @if(isset($info_page->title))<h1>{{$info_page->title}}</h1>@endif
                 @if(isset($info_page->content))
                            {!!$info_page->content!!}
                    @endif
                <div>

                </div>
        </div>
    </section>
    @include('components.right_column')
</div>


<div class="l-empty"></div>
@stop