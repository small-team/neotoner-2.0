@extends('layouts.default')
@section('template_content')

<div class="g-clrfix">
    <section class="l-main l-main-is_blocks_right">
        <div class="l-main_inner">
            <aside class="content-center content-center_noright">
{{--                <div class="content__breadcrumbs">--}}
{{--                    --}}{{--<a href="">Главная</a> / <a href="">Прайс-листы</a> / <span>Скупка картриджей</span>--}}
{{--                    --}}{{--{!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}--}}
{{--                </div>--}}
                @if(!empty(Session::get('send')) && Session::get('send'))
                    <div class="content__title">Спасибо, Ваш вопрос принят на предварительную модерацию.</div>
                @else
                    <div class="content__title">Вопросы-ответы</div>
                @endif
                <div class="content-form content-form_faq" style=" margin-bottom: 20px;">
                    <form action="{{URL::route('store_questions')}}"  class="">


                    <table cellspacing="0" cellpadding="0" border="0" class="zayavka" width="100%">

                        <tbody>
                        <tr>
                            <td rowspan="3" class="">
                                <textarea data-value="Ваш вопрос"   placeholder="Ваш вопрос"  name="mess" class="inp2 inp2_pad"   @if($errors->first('mess')) style="border-color: red" @endif>{{Input::old('mess')}}</textarea>
                            </td>
                            <td>
                                <input data-value="Имя"  placeholder="Имя" type="text" value="{{Input::old('name')}}" name="name" class="inp2 inp2_pad"  @if($errors->first('name')) style="border-color: red" @endif />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="inp2 phone_mask" placeholder="+7 (___) ___-__-__"  value="{{Input::old('phone')}}" name="city" @if($errors->first('city')) style="border-color: red" @endif
                                       id="phone">
{{--                                <input data-value="Город" placeholder="Город" type="text" name="city"  value="{{Input::old('city')}}" class="inp2 inp2_pad" @if($errors->first('city')) style="border-color: red" @endif />--}}
                            </td>
                        </tr>

                        <tr>
                            <td class="td_bottom_button"><input class="content-form__btn" type="submit" value="Отправить вопрос"></td>
                        </tr>



                        </tbody>
                    </table>
                        {{--<div class="content-form-row content-form-row_faq row">--}}
                            {{--<div class="content-form-el">--}}
                                {{--<div class="content-form-el-inp icon"><input data-value="Имя"  placeholder="Имя" type="text" value="{{Input::old('name')}}" name="name" class="content-form__input"  @if($errors->first('name')) style="border-color: red" @endif /></div>--}}
                                {{--<div class="content-form-el-inp icon"><input data-value="Город" placeholder="Город" type="text" name="city"  value="{{Input::old('city')}}" class="content-form__input" @if($errors->first('city')) style="border-color: red" @endif /></div>--}}

                            {{--</div>--}}
                            {{--<div class="content-form-el content-form-el__faqtextarea">--}}
                                {{--<div class="content-form-el-inp content-form-el-inp_textarea icon"><textarea data-value="Ваш вопрос"   placeholder="Ваш вопрос"  name="mess" class="content-form__input req"   @if($errors->first('mess')) style="border-color: red" @endif>{{Input::old('mess')}}</textarea></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="content-form-row row">--}}
                            {{--<div class="content-form-el content-form-el__last">--}}
                                {{--<div class="content-form-el__info"></div>--}}
                            {{--</div>--}}
                            {{--<div class="content-form-el content-form-el__last">--}}
                                {{--<button class="content-form__btn" type="submit"><span>Отправить вопрос</span></button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </form>
                </div>

                <div class="content-faq">
                @if(!empty($questions))
                    @foreach($questions as $question)
                        @if($question->is_moderate == 1)
                            <div class="content-faq-item">
                                <div class="content-faq-item-top">
                                    <div class="content-faq-item-top__data"><span>{{$question->created_at}}</span></div>
                                    <div class="content-faq-item-top__name">{{$question->name}}</div>
                                </div>
                                <div class="content-faq-item__title">
                                    {{--<p>Вопрос № {{$question->position}}:</p>--}}
                                    <p>{{$question->title}}</p>
                                </div>
                                <div class="content-faq-item__text">
                                    {{$question->question}}
                                </div>
                                <div class="content-faq-item-answer">
                                    <div class="content-faq-item-answer__top">{{$question->answer_author}}, <span>{{$question->answer_position}}</span></div>
                                    <div class="content-faq-item-answer__text">
                                        <span>Ответ:</span>
                                        {{$question->answer}}
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif

                    @include('components.pagination', ['paginator' => $questions])

                </div>
            </aside>
                <div>
                </div>
        </div>
    </section>
     @include('components.right_column')
</div>

<div class="l-empty"></div>

@stop