@extends('layouts.default')
@section('template_content')
<div class="g-clrfix">
    <section class="l-main l-main-is_blocks_right">
        <div class="l-main_inner">
            <h1>Новости и статьи компании</h1>
             @if(!empty($news))
                 @foreach($news as $news_item)
                    <div class="l-main_article">
                        @if ($news_item->image)
                            <img src="{{$news_item->image['sizes']['list']['link']}}" class="l-main_article_image" alt="{{$news_item->title}}">
                        @endif

                        <i>{{$news_item->created_at}}</i>
                        <h4><a href="{{URL::route('newsitem',$news_item->_slug)}}">{{$news_item->title}}</a></h4>
                        <div class="clear"></div>
                    </div>
                @endforeach
            @endif
                <div class="paginator fullsize" id="paginator_example">
                    <table width="100%">
                        <tbody>
                        @include('components.pagination', ['paginator' => $news])

                        </tbody>
                    </table>
                </div>
        </div>
    </section>
{{----------------------------------------}}
     @include('components.right_column')
{{----------------------------------------}}
</div>
<div class="l-empty"></div>

@stop