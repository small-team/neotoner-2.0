@extends('layouts.default')
@section('template_content')
    <div class="g-clrfix">
        <section class="l-main l-main-is_blocks_right">
            <div class="l-main_inner">
                    <h1>Контакты</h1>
                     {{--@if(isset($info_page->content))--}}
                                {{--{!!$info_page->content!!}--}}
                        {{--@endif--}}

                        <div class="content-contact-column">
                            @if(isset($about_info['about_contact']))
                                <div class="contact_icon_block_container">
                                    <div class="contact_icon_block contact_icon_block_phone"></div>
                                    <div class="contact_icon_block_content">{{ $about_info['about_contact']}}</div>
                                    <div class="clear"></div>
                                </div>
                            @endif

                            @if(isset($about_info['about_timeblade']))
                                <div class="contact_icon_block_container">
                                    <div class="contact_icon_block contact_icon_block_worktime"></div>
                                    <div class="contact_icon_block_content">{{ $about_info['about_timeblade']}}</div>
                                    <div class="clear"></div>
                                </div>
                            @endif
                            @if(isset($about_info['about_email']))
                                <div class="contact_icon_block_container">
                                    <div class="contact_icon_block contact_icon_block_email"></div>
                                    <div class="contact_icon_block_content"><a href="mailto:manager@toner-sell.ru">{{ $about_info['about_email']}}</a></div>
                                    <div class="clear"></div>
                                </div>
                            @endif

                        </div>

                        <div class="content-contact-column">
                            @if(isset($about_info['company_phone_footer']))
                                <div class="contact_icon_block_container">
                                    <div class="contact_icon_block contact_icon_block_whatsapp"></div>
                                    <div class="contact_icon_block_content">{{ $about_info['company_phone_footer']}}</div>
                                    <div class="clear"></div>
                                </div>
                            @endif

                            <div class="contact_icon_block_container">
                                <div class="contact_icon_block contact_icon_block_icq"></div>
                                <div class="contact_icon_block_content">676-684-228</div>
                                <div class="clear"></div>
                            </div>

                            @if(isset($about_info['about_address']))
                                <div class="contact_icon_block_container">
                                    <div class="contact_icon_block contact_icon_block_address"></div>
                                    <div class="contact_icon_block_content">{{ $about_info['about_address']}}</div>
                                    <div class="clear"></div>
                                </div>
                            @endif
                        </div>


                <div class="contact_bottom_line">
                    <div class="content-contact__info">Обязательно позвоните нам перед выездом!</div>
                    <a class="header__callback noprint">Обратный звонок</a>
                    <div class="clear"></div>
                </div>

                    @if(isset($about_info['about_coordinates_l'])&& isset($about_info['about_coordinates_d']))
                        <div id="map" class="content-contact__map" data-address="{{$about_info['about_address']}}" data-about_coordinates_l="{{$about_info['about_coordinates_l']}}" data-about_coordinates_d="{{$about_info['about_coordinates_d']}}"> </div>
                    @else
                        <div id="map" class="content-contact__map" data-address="{{$about_info['about_address']}}" data-about_coordinates_l="56.8683385" data-about_coordinates_d="60.5947636"> </div>
                    @endif

                    @if(!empty($about_info['contact_content']))
                       {!!$about_info['contact_content']!!}
                    @endif
                    <div>
                    </div>


            </div>
        </section>
        @include('components.right_column')
    </div>


    <div class="l-empty"></div>
    {{--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>--}}

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script>
        ymaps.ready(function () {
            if (!document.getElementById('map')) {
                return;
            }
            var myMap = new ymaps.Map('map', {
                center: [{{$about_info['about_coordinates_l']}}, {{$about_info['about_coordinates_d']}}],
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            });

            var myPlacemark = new ymaps.Placemark([{{$about_info['about_coordinates_l']}}, {{$about_info['about_coordinates_d']}}] , {},
                { iconLayout: 'default#image',
                    iconImageSize: [40, 51],
                    iconImageOffset: [-20, -47] });

            myMap.geoObjects.add(myPlacemark);

        });

        var $btn = $('.mobile-menu-btn');

        $btn.click(function(){
            $('header').toggleClass('show');

        })
    </script>
    
@stop