@extends('layouts.default')
@section('template_content')

<div class="g-clrfix">
    <section class="l-main">
        <div class="l-main_inner">

            <article>
                @if(isset($cartridge_brand->top_text_for_brands))
                    {!!$cartridge_brand->top_text_for_brands!!}
                @endif
            </article>
            <div class="l_main_order_block">
                <td width="30%" style="vertical-align:middle" rowspan="2">
                    <div class="block_kurs noprint">
                        <strong style="float: right;margin-bottom: 2px;">1 у.е. =  <span class="green " id="currency_rate">{{$info_satellite->rate}} руб.</span></strong>
                        <div class="">
                            <strong>Валюта:&nbsp;</strong>
                            <a id="currency_USD" class="currency_flag @if(\Session::get('currency') == 'USD') on @endif"   onclick="selectCurrency('USD');">у.е.</a>
                            <a id="currency_RUB" class="currency_flag @if(\Session::get('currency') == 'RUB') on @endif" onclick="selectCurrency('RUB');" >руб.</a>
                            <input type="hidden" value="RUB" id="current_currency"/>
                        </div>
                    </div>
                </td>
            </div>

            @include('components/right_price_block', ['count_items' => $count_items, 'sum_items' => $sum_items_client])

                <table  width="100%" class="l-table_price" type="cartridge" id="prices">
                    <tbody>
                    <tr class="head">
                        <th class="th_articul">Артикул</th>
                        <th class="th_model">Модель устройства</th>
                        <th class="th_price_RUB price_RUB">Наша цена</th>
                        <th class="th_price_want">Желаемая</th>
                        <th class="th_count">Кол-во</th>
                    </tr>
                    @if(!empty($cartridge_price_item))
                        @foreach($cartridge_price_item as $brand => $items)
                            <tr >
                                <td colspan="5" style="font-weight: bold; text-align: left; background-color: #EEE9E9; padding-left: 25px">{{$brand}}</td>
                            </tr>
                            @foreach($items as $item)
                                <tr class="item_row">
                                    <td class="td_articul td-left-pl15">{{$item->articule}}</td>
                                    <td class="td_model td-left-pl15">{{$item->cartridge_model}}</td>
                                        <td class="td_price_RUB price_RUB price_RUB-{{$item->id}}">{{$item->price_RUB}}</td>
                                        <td class="price_USD price_USD-{{$item->id}}" style="display: none">{{$item->price_USD}}</td>
                                    <td class="td_price_want price__inp_td">
                                        @if(isset($cart_items[$item->id]))
                                            <input type="text" name="price[]" value="{{$cart_items[$item->id]["sum_Client"]}}" id="price-{{$item->id}}" autocomplete="off" class="number price__inp item_{{$item->id}}" />
                                        @else
                                            <input type="text" name="price[]" value="{{$item->price_RUB}}" id="price-{{$item->id}}" autocomplete="off" class="number price__inp item_{{$item->id}}" />
                                        @endif
                                    </td>
                                    <td class="td_count quantity__inp_td">
                                        @if(isset($cart_items[$item->id]))
                                            <input type="text" name="form[]" value="{{$cart_items[$item->id]["count"]}}" autocomplete="off" id="{{$item->id}}" class="number quantity__inp item_{{$item->id}}" />
                                        @else
                                            <input type="text" name="form[]" value="0" id="{{$item->id}}" autocomplete="off" class="number quantity__inp item_{{$item->id}}" />
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @else
                        <?php $newItemID = sprintf('new-search-%s', md5($key_search)); ?>
                        <tr class="item_row">
                            <td class="td-left-pl15">
                                {{$key_search}}
                                <input type="hidden" name="articule[]" value="{{$key_search}}" id="articule-{{$newItemID}}" autocomplete="off" class="item_desc_input articule_{{$newItemID}}" />
                            </td>
                            <td class="td-left-pl15"></td>
                            <td class=" price_RUB price_RUB-{{$newItemID}}"></td>
                            <td class="price_USD price_USD-{{$newItemID}}" style="display: none"></td>
                            <td class="price__inp_td">
                                @if(isset($cart_items[$newItemID]))
                                    <input type="text" name="price[]" value="{{$cart_items[$newItemID]["sum_Client"]}}" id="price-{{$newItemID}}" autocomplete="off" class="number price__inp item_{{$newItemID}}" />
                                @else
                                    <input type="text" name="price[]" value="" id="price-{{$newItemID}}" autocomplete="off" class="number price__inp item_{{$newItemID}}" />
                                @endif
                            </td>
                            <td class="quantity__inp_td">
                                @if(isset($cart_items[$newItemID]))
                                    <input type="text" name="form[]" value="{{$cart_items[$newItemID]["count"]}}" autocomplete="off" id="{{$newItemID}}" class="number quantity__inp item_{{$newItemID}}" />
                                @else
                                    <input type="text" name="form[]" value="0" id="{{$newItemID}}" autocomplete="off" class="number quantity__inp item_{{$newItemID}}" />
                                @endif
                            </td>
                        </tr>
                    @endif

                    </tbody>
                </table>

            @if(empty($cartridge_price_item))
                <span class="custom_position">Искомой модели нет в базе, но продать ее возможно!<br/>Введите желаемую цену и укажите количество для продажи</span>
            @endif

{{----}}
            <div class="bottom_text_for_brands">
            @if(isset($cartridge_brand->bottom_text_for_brands))
                {!!$cartridge_brand->bottom_text_for_brands!!}
            @endif
            </div>


        </div>
    </section>
</div>
<div class="l-empty"></div>
@stop