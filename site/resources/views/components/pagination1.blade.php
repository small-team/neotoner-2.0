<ul>
<li><a href="{{$paginator->url($paginator->currentPage()-1)}}" class="content-pagging__link left">&laquo;</a></li>
@if ($paginator->lastPage() > 1)
         @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            @if ($paginator->currentPage() == $i)
               <li><a  class="content-pagging__link active">{{ $i }}</a></li>
            @else
               <li><a href="{{ $paginator->url($i) }}" class="content-pagging__link">{{ $i }}</a></li>
            @endif
         @endfor
   {{--</div>--}}
@endif
@if($paginator->currentPage()==$paginator->lastPage())
<li><a  class="content-pagging__link right">&raquo;</a></li>
@else
<li><a href="{{$paginator->url($paginator->currentPage()+1)}}" class="content-pagging__link right">&raquo;</a></li>
@endif


</ul>