<tr>
    @if ($paginator->lastPage() > 1)
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <td width="">
            <span>
                @if ($paginator->currentPage() == $i)
                    <strong>{{$i}}</strong>
                @else
                   <a href="{{ $paginator->url($i) }}">{{$i}}</a>
                @endif
            </span>
        </td>
        @endfor
    @endif
</tr>
<tr>
    <td colspan="{{$paginator->lastPage()}}">
        <div class="scroll_bar">
            <div class="scroll_trough"></div>
            <div class="scroll_thumb" style="width: 8px; left: 0px;">
                <div class="scroll_knob"></div>
            </div>
            <div class="current_page_mark"
                    style="width: {{100/$paginator->lastPage()}}%;
                             left: {{((100/$paginator->lastPage())*$paginator->currentPage() - (100/$paginator->lastPage()))}}%;">
            </div>
        </div>
    </td>
</tr>