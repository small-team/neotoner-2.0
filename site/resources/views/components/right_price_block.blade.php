<div class="float_right_block">
    <div class="float_price_border float_price">
        <div class="float_inner_border">
            <input class="float_button red_button" id="popup_client_price_handler" type="button" value="Хочу дороже">

            <p class="sell_price_text price_RUB_sell" >Картриджей <br><span class="cart_num "> {{$count_items}}</span> шт.</p>
            <p class="sell_price_text price_RUB_sell">На сумму <br><span class="cart_sum "> {{$sum_items}}</span> руб.</p>

            <p style="text-align: center; font-weight: bold">
                Нужен самовывоз?
                <br/>
                <span class="green"><a href="/pages/regions">Условия тут</a></span>
            </p>

            <input class="float_button sell_price" onclick="location.href='{{URL::route('cart')}}';@if(!env('TEST_SITE', false)) yaCounter28909060.reachGoal('PRICESELLKNOPK'); @endif"
                   type="button" value="Продать">
        </div>
        <div class="float_inner">
            <input class="float_button red_button clear_right_button" onclick="location.href='{{URL::route('clean_cart')}}';"
                   type="button" value="Очистить заявку">
        </div>
    </div>
    <a class="go_top_block" href="javascript:window.scrollTo(0,0);" style="display: none">
        Вверх
    </a>
</div>