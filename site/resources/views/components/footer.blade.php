<footer class="l-footer noprint">

    <div class="l-footer_inner">
        <div class="l-footer_cont">
            <div class="l-footer_cont-inner" style="min-height: 1px;">
                @if(!empty($info_footer->about_contact))
                    <div class="contact_icon_block_container">
                        <div class="contact_icon_block contact_icon_block_phone"></div>
                        <div class="contact_icon_block_content"><a href="tel:{!!str_replace([' ','-','(',')'], '', $info_company->about_contact)!!}" class="tel">{!!$info_footer->about_contact!!}</a></div>
                    </div>
                @endif
                @if(!empty($info_footer->company_phone_footer))
                    <div class="contact_icon_block_container">
                        <div class="contact_icon_block contact_icon_block_whatsapp"></div>
                        <div class="contact_icon_block_content"><a href="tel:{!!str_replace([' ','-','(',')'], '', $info_company->company_phone_footer)!!}" class="tel">{!!$info_footer->company_phone_footer!!}</a></div>
                    </div>
                @endif
                @if(!empty($info_footer->footer_email))
                    <div class="contact_icon_block_container">
                        <div class="contact_icon_block contact_icon_block_email"></div>
                        <div class="contact_icon_block_content"><a href="mailto:{!!$info_footer->footer_email!!}">{!!$info_footer->footer_email!!}</a></div>
                    </div>

                @endif
                @if(!empty($info_footer->footer_address))
                    <div class="contact_icon_block_container">
                        <div class="contact_icon_block contact_icon_block_address"></div>
                        <div class="contact_icon_block_content"><a href="{{route('contacts')}}">{!!$info_footer->footer_address!!}</a></div>
                    </div>
                @endif
                <div class="clear"></div>
{{--                @if(!empty($info_footer->timeblade_footer)){!!$info_footer->timeblade_footer!!}<br>@endif--}}
            </div>
        </div>
        <div class="l-footer_copy">
            @if(!empty($info_company->copyright))
                {!!$info_company->copyright!!}
            @endif
        </div>
    </div>
</footer>

<script src="/js/main.js?3"></script>
<script src="/js/phone_mask.js"></script>

<script type="text/javascript" src="/js/libs/highslide/highslide.packed.js"></script>
<script type="text/javascript" src="/js/libs/highslide/highslide-init.js"></script>
<script type="text/javascript" src="/js/paginator3000/paginator3000.js"></script>