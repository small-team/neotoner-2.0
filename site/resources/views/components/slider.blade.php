<div class="content-body-centerside-slider slider">
@if(!empty($banners))
    @foreach($banners as $banners_item)

        <a href="@if(!empty($banners_item->link)){{$banners_item->link }}@else # @endif" class="content-body-centerside-slider-item">
            <img src="{{$banners_item->image['sizes']['poster']['link']}}" height="202" width="500" alt="" />
            <div class="content-body-centerside-slider-item-content">
                {{$banners_item->description}}
                <strong>{{$banners_item->title}}</strong>
            </div>
        </a>
    @endforeach
@endif
</div>