<div class="search noprint">
    <div class="container">
        <div class="search-form row">
            <form action="{{URL::route('search')}}" method="post">
            {!! csrf_field() !!}
                <button class="search-form__btn btn" type="submit">Поиск</button>
                <div class="search-form-input">
                    <input class="search-form__input" data-value="Поиск по артикулу и модели картриджа" type="text" name="key" />
                </div>
            </form>
        </div>
    </div>
</div>