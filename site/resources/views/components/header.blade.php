<div class="l-header-inner">

    <div class="l-header_logo">
        <a href="/">
            <img class="logo" alt="Neotoner – скупка картриджей в Москве" src="/img/logo_h.png">
            <div>Скупка картриджей</div>
        </a>
    </div>


        <div class="l-header_contacts">
            @if(!empty($info_company->company_phone))
                <a href="tel:{!!str_replace([' ','-','(',')'], '', $info_company->company_phone)!!}" class="tel">{!!$info_company->company_phone!!}</a>
            @endif

            @if(!empty($info_company->timeblade))
                <div class="timerabota">{!!$info_company->timeblade!!}</div>
            @endif
        </div>

    <div class="header_contacts_cart_container">
        <div class="full_cart" style="@if(empty($cart_items))display: none;@endif">
            <a href="{{URL::route('cart')}}"><img src="{{asset('img/full_cart.png')}}"></a>
        </div>

        <div class="empty_cart" style="@if(!empty($cart_items))display: none;@endif">
            <img src="{{asset('img/empty_cart.png')}}">
        </div>

        <div class="l-header_search noprint">

            <div class="searchtop">
                <form action="{{URL::route('search')}}" method="get">
                    <input type="text" name="key"  id="search_key" value="@if(!empty($key_search)){{$key_search}}@endif" placeholder="Поиск" autocomplete="off" />
                    <input type="submit" value="" id="search_button"/>
                </form>
            </div>
            <div class="searchtop_multi" style="display: none">
                <div class="title">Массовый поиск</div>
                <div class="search_button" onclick="multiSearchClick()"></div>
            </div>
            <div id="multi_handler"></div>
        </div>
        <div class="multi_search_popup" style="display: none">
            <textarea id="multi_search_input" placeholder="Введите позиции в столбик"></textarea>
            <input type="button" value="Найти" class="search_button_multi" onclick="multiSearchClick()"/>
        </div>
    </div>

    <div class="clear"></div>
</div>