<nav>
    <ul class="navi noprint">
    @if(!empty($menu_item))
        @foreach($menu_item as $item)
            @if($item->show_head)
                @if($item->object_type=='user_pages')
                    <li><a href="{{URL::route('user_pages',$item->user_slug)}}">{{$item->title}}</a></li>
                @elseif($item->object_type=='fixed_pages')
                @if($item->fixed_slug=='request')
                              <li ><a href="#" style="pointer-events: none;" >Покупка картриджей</a>
                                    <ul>
                                     @foreach($brands as $item_brand)
                                         <li><a href="{{URL::route('price_brand',$item_brand->_slug)}}" class="topblock-menu__sublink">{{$item_brand->title}}</a></li>
                                     @endforeach
                                     </ul>
                              </li>
                @endif
                    <li><a href="{{URL::route($item->fixed_slug)}}">{{$item->title}}</a></li>
                @endif
            @endif
        @endforeach
    @endif
    </ul>
</nav>
