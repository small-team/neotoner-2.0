<div class="content-body-leftside noprint">
    <div class="content-module content-body-leftside-brands">
        <ul>
        @if(!empty($brands))
            @foreach($brands as $item_brand)
            <li><a href="{{URL::route('price_brand',$item_brand->_slug)}}" class="content-body-leftside-brands__link"><span>{{$item_brand->title}}</span></a></li>
            @endforeach
        @endif
        </ul>
    </div>
</div>