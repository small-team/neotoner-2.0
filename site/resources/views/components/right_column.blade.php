<aside class="l-blocks_right">
        @if(!empty($info_company->best_deals))
            <div class="l-block">
                <div class="h1_l-block_title red-highlighted-head">Внимание!</div>

                <div class="l-block_content">
                    <div class="l-block_content-inner">
                        {!!$info_company->best_deals!!}
{{--                        <br />--}}
{{--                        <a id="main_load_price" target="_blank" href="{{URL::route('exel_download','')}}" @if(!env('TEST_SITE', false)) onclick="yaCounter28909060.reachGoal('DOWNLOADALLPRICE');" @endif>--}}
{{--                            <img width="16px" height="16px" src="/img/prlist.png" /> Скачать прайс-лист </a>--}}

                    </div>
                </div>
            </div>
                        @endif

        @if (count($new_news))
            <div class="l-block">
                <div class="h1_l-block_title l-block_title">Новости </div>

                <div class="l-block_content">
                    <div class="l-block_content-inner">
                        @foreach($new_news as $news_item)
                            <p>
                                <i>{{$news_item->created_at}}</i><br/>
                                <a href="{{URL::route('newsitem',$news_item->_slug)}}">{{$news_item->title}}</a>
                            </p>
                        @endforeach
                    </div>
                    <div class="right"><a href="{{URL::route('news')}}">Все новости</a></div>

                </div>
            </div>
        @endif
    </aside>