@extends('layouts.default')
@section('template_content')
 @include('components.right_column')
<aside class="content-center content-center_noright">
    <div class="content__breadcrumbs">
    </div>
    <div class="content-article">

        @if(isset($content_page_404))
        {!!$content_page_404!!}
        @endif
    </div>
</aside>
@stop
