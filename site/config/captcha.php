<?php

return [

    'default'   => [
        'length'    => 4,
        'width'     => 100,
        'height'    => 38,
        'quality'   => 100,
    ],

    'flat'   => [
        'length'    => 3,
        'width'     => 73,
        'height'    => 32,
        'quality'   => 50,
        'lines'     => -1,
        'bgImage'   => false,
        'bgColor'   => '#ecf2f4',
        'fontColors'=> ['#2c3e50', '#c0392b', '#16a085', '#c0392b', '#8e44ad', '#303f9f', '#f57c00', '#795548'],
        'contrast'  => 100
    ],

    'mini'   => [
        'length'    => 3,
        'width'     => 60,
        'height'    => 36,
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -3,
    ]

];
