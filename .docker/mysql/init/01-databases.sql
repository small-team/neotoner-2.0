# create databases
CREATE DATABASE IF NOT EXISTS `tonerbaza_site`;
CREATE DATABASE IF NOT EXISTS `tonerbaza_base`;

# create root user and grant rights
CREATE USER 'root'@'localhost' IDENTIFIED BY 'root';
GRANT ALL ON *.* TO 'root'@'%';